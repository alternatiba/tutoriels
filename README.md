# Tutoriels

Quelques tutoriels utiles pour les outils numériques utilisés par Alternatiba / ANV-COP21

Pour accéder aux tutoriels : <https://tuto.alternatiba.eu>

## Contribuer

`git clone https://framagit.org/alternatiba/tutoriels`

Les tutoriels sont en Markdown, dans le dossier `docs`.

### Dépendances

Le site utilise [MkDocs](https://www.mkdocs.org) et le thème [Material](https://squidfunk.github.io/mkdocs-material/).

Installer Python

- `apt install python3`

Installer MkDocs et Material :

- `pip install mkdocs`
- `pip install mkdocs-material`

Installer les plugins :

- `pip install mkdocs-exclude`
- `pip install mkdocs-git-revision-date-localized-plugin`

#### Cas de l'installation dans un environnement virtuel Python

Voir <https://packaging.python.org/en/latest/guides/installing-using-pip-and-virtual-environments/>

Créer l'environnement virtuel (à faire la première fois seulement) :

- `python3 -m venv chemin/vers/le/dossier`

Activer l'environnement virtuel :

- `source ./bin/activate`

Installer les dépendances avec pip au sein de l'environnement virtuel :

- `python3 -m pip install mkdocs mkdocs-material mkdocs-exclude mkdocs-git-revision-date-localized-plugin`

### Serveur local

`mkdocs serve` (ou `python3 -m mkdocs serve` avec l'environnement virtuel)

Le site sera accessible depuis `http://localhost:8000`

### Build local

`mkdocs build`

Le site sera dans le dossier `site`.

## Déploiement

Le build et le déploiement se font automatiquement avec GitLab CI.

Le site est accessible à <https://tuto.alternatiba.eu>
