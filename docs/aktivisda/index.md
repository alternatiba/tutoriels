---
title: Aktivisda
author: Alternatiba / ANV-COP21
date: Mai 2024
theme: beige
icon: material/palette
---

![Aktivisda](media/aktivisda-logo.png){ align=left }

**Aktivisda est un site web conçu pour créer facilement des visuels aux couleurs d'Alternatiba / ANV-COP21 à partir de modèles pré-enregistrés.**

L'objectif est de simplifier et d'accompagner la communication des groupes locaux Alternatiba et ANVCOP-21. Dans le cadre du [Tour Alternatiba 2024](https://tour.alternatiba.eu), des modèles sont mis à disposition pour les groupes qui accueillent une étape sur le parcours.

👉 Le site est accessible à l'adresse <https://aktivisda.alternatiba.eu>

<div class="grid cards" markdown>

-   ## 🚀 Je me lance !
    - [C'est quoi Aktivisda ?](./presentation)
    - [Mes premiers pas sur Aktivisda](./premiers-pas)

</div>


## 💫 Le mot clef : simplicité

Cet outil vise à **aider les groupes locaux** avec la création de visuels cohérents avec la charte graphique et les lignes de communication d’Alternatiba et d’ANV-COP21 !

Sur Aktivisda, des visuels types appelés **"modèles"** sont mis à disposition avec les éléments graphiques de la charte graphique du Tour Alternatiba 2024 : symboles, polices et couleurs.

En quelques clics, tout un chacun peut utiliser un modèle existant générique et l'adapter à sa propre étape. Il est possible d'utiliser des images déjà présentes sur le serveur, mais également des images de son propre ordinateur.


## 🧩 Un outil collaboratif

Tu penses que ce symbole est manquant dans Aktivisda ? Tu as créé une affiche magnifique pour ton groupe ? Partage-la à l'ensemble du mouvement !

N'importe qui peut proposer des éléments graphiques (symbole, photo, modèle...) pour alimenter la base de données d'Aktivisda et en faire bénéficier les autres groupes. Pour cela, il suffit d'envoyer un courriel à [aktivisda@alternatiba.eu](mailto:aktivisda@alternatiba.eu).


---

Tu es perdu⋅e ? Tu peux écrire à [aktivisda@alternatiba.eu](mailto:aktivisda@alternatiba.eu) ou sur le canal [⁉️ Questions et annonces](https://chat.alternatiba.eu/coordination-du-mouvement/channels/town-square). On est là pour t’aider ! :slight_smile: