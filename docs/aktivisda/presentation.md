---
title: C'est quoi Aktivisda ?
author: Alternatiba
date: Mai 2024
theme: beige
---

## 🎨 En bref

Sur [le site du projet](https://aktivisda.earth/), Aktivisda se présente ainsi :

> *En bref*, Aktivisda est un **site internet statique** et **libre** pour **simplifier** la création de **visuels** (affiches, réseaux sociaux) **par tous·tes les militant·es** d'une **organisation** aux **couleurs de cette organisation**.

C'est un logiciel libre que chaque organisation peut installer et personnaliser. L'instance d'Alternatiba est accessible à l'adresse <https://aktivisda.alternatiba.eu> depuis mai 2024.

Avec des fonctionnalités simples et efficaces, des visuels personnalisés et une philosophie engagée, Aktivisda propose un autre choix face à [Canva](https://canva.com).

👉 Pour en savoir plus sur le logiciel Aktivisda, rendez-vous sur [le site du projet](https://aktivisda.earth/).


## 🤔 Dans quels cas utiliser Aktivisda ?

Aktivisda est parfait pour **créer des visuels avec peu de texte**, par exemple des visuels pour annoncer des événements, rencontres, manifestations. Ces visuels peuvent être imprimés ou bien publiés sur les réseaux sociaux.

Quelques exemples :

* communiquer sur l'actualité du groupe local sur les réseaux sociaux
* annoncer la programmation d'un événement
* publier un post sur la recherche de bénévoles
* inviter largement pour une réunion d'accueil
* etc.

L'objectif est d'avoir une interface accessible et simple d'utilisation : en modifiant le texte, les symboles et les images de modèles, il est facile de personnaliser le contenu tout en gardant une charte graphique cohérente !

!!! tip "Le sais-tu ?"
    [Extinction Rebellion](https://rebellion.global/) utilise largement Aktivisda pour créer ses visuels !

    👉 Tu peux découvrir leur instance Aktivisda ici : <https://extinctionrebellion.aktivisda.earth/>


## 🖌️ Que puis-je faire avec Aktivisda ?

1. **Éditer les visuels**

    La fonctionnalité principale d’Aktivisda est l’édition des visuels. Il est possible de déplacer et de modifier la taille de chaque élément graphique présent dans le visuel, de modifier la couleur et la taille des textes et enfin de modifier les images, en utilisant des photos personnelles ou des symboles déjà présents dans Aktivisda.

    La force d'Aktivisda est de pouvoir créer facilement un visuel en s'appuyant sur un modèle pré-existant.

    !!! warning "Attention"

        L'outil ne permet pas encore d'utiliser la fonction **"retour"**. Ainsi si tu fais une modification et que tu souhaites "revenir en arrière", tu ne peux pas utiliser le raccourci Ctrl+Z.

2. **Accéder à la charte graphique d’Alternatiba et ANV-COP21**

    Afin de créer des visuels, Aktivisda donne accès aux éléments graphiques cohérents avec la charte graphique du Tour Alternatiba 2024 : couleurs, police, symboles... Tu peux utiliser ces éléments dans ton propre logiciel de graphisme ou tes documents.

3. **Exporter et sauvegarder les visuels**

    Il est possible d'exporter un visuel aux formats **png** et **jpg** afin de le sauvegarder sur ton ordinateur et puis le partager sur les réseaux sociaux.

## 💾 Code source

Si tu souhaites voir le code source : <https://framagit.org/aktivisda/aktivisda>

## 🔥 Alors, on y va ?!

Attrape ta peinture et tes pinceaux, et [c'est parti !](../premiers-pas)

---

Tu es perdu⋅e ? Tu peux écrire à [aktivisda@alternatiba.eu](mailto:aktivisda@alternatiba.eu) ou sur le canal [⁉️ Questions et annonces](https://chat.alternatiba.eu/coordination-du-mouvement/channels/town-square). On est là pour t’aider ! :slight_smile: