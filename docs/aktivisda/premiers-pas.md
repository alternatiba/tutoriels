---
title: Se lancer sur Aktivisda
author: Alternatiba / ANV-COP21
date: Mai 2024
theme: beige
---


Accéde à Aktivisda avec ce lien : <https://aktivisda.alternatiba.eu>

!!! warning "Safari"
    Il est possible qu'il y aie des erreurs de rendu avec le navigateur Safari. Nous te conseillons d’utiliser **Firefox** ou **Chrome**.

## Présentation de l'interface

La barre de navigation est composée de plusieurs menus :

![Barre de navigation](media/aktivisda-navbar.png){ loading=lazy .md-img-border }

* **Tous les modèles** : les modèles génériques pré-conçus. Tu peux les modifier en cliquant dessus ;
* **Symboles** : les éléments graphiques qui cmoposent les visuels (symboles, photos, logos...) ;
* **Images de fonds** : il est possible de choisir un motif en arrière-plan. Cette fonctionnalité est peu utilisé pour l'instant ;
* **Couleurs** : pour retrouver l'ensemble des couleurs du Tour Alternatiba 2024 ;
* **Polices** : les polices du Tour Alternatiba 2024 ;
* **Codes QR** : un outil pour créer facilement des codes QR avec un lien personnalisé en intégrant une image au code ;
* **À propos** : des informations générales sur le logiciel.


## Editer un visuel

Pour accéder à l’édition, choisis un modèle pré-existant dans le menu **"Tous les modèles"** ou directement depuis la page d'accueil.

Une fois l'éditeur ouvert, tu peux choisir parmi ces trois onglets :

![Les trois onglets de l'éditeur](media/aktivisda-edit-menus.png){ loading=lazy .md-img-border }

### Document

Dans cette section on peut modifier la taille du document et l’image en arrière-plan.

👉 Pour modifier la **taille** du document on peut appuyer sur le + et - ou choisir entre différents formats prédéfinis :

<video controls width="720" class="md-img-border">
  <source src="../media/aktivisda-modifier-taille.webm" type='video/webm' />
  <source src="../media/aktivisda-modifier-taille.mp4" type='video/mp4' />
  Ton navigateur ne semble pas prendre en charge les vidéos HTML5. Ecris-nous si tu as une difficulté.
</video>

!!! tip "Réseaux sociaux"
    Pour les publications sur les réseaux sociaux, on privilégie le format “Instagram carré” 1080x1080 pixels.

👉 Pour modifier l'**arrière-plan** du document, on utilise la sous-section **image** :

<video controls width="720" class="md-img-border">
  <source src="../media/aktivisda-modifier-arriere-plan.webm" type='video/webm' />
  <source src="../media/aktivisda-modifier-arriere-plan.mp4" type='video/mp4' />
  Ton navigateur ne semble pas prendre en charge les vidéos HTML5. Ecris-nous si tu as une difficulté.
</video>

### Textes

👉 Pour **modifier le texte**, il suffit de double-cliquer sur la zone de texte que tu souhaites modifier et de taper directement le nouveau texte.

Dans la section qui apparaît à gauche de l’écran, tu peux modifier plusieurs paramètres, notamment la taille de la police, la police de caractères, l'alignement, la couleur et la position dans l'affichage :

<video controls width="720" class="md-img-border">
  <source src="../media/aktivisda-modifier-texte.webm" type='video/webm' />
  <source src="../media/aktivisda-modifier-texte.mp4" type='video/mp4' />
  Ton navigateur ne semble pas prendre en charge les vidéos HTML5. Ecris-nous si tu as une difficulté.
</video>

👉 Pour **ajouter un nouveau texte** au visuel, il suffit de cliquer sur le bouton "texte", puis sur "nouveau texte" et de taper ce que tu souhaites écrire :

<video controls width="720" class="md-img-border">
  <source src="../media/aktivisda-ajouter-texte.webm" type='video/webm' />
  <source src="../media/aktivisda-ajouter-texte.mp4" type='video/mp4' />
  Ton navigateur ne semble pas prendre en charge les vidéos HTML5. Ecris-nous si tu as une difficulté.
</video>

👉 Pour **supprimer un texte**, clique sur le texte à supprimer, puis sur l'icône de la poubelle

<video controls width="720" class="md-img-border">
  <source src="../media/aktivisda-supprimer-texte.webm" type='video/webm' />
  <source src="../media/aktivisda-supprimer-texte.mp4" type='video/mp4' />
  Ton navigateur ne semble pas prendre en charge les vidéos HTML5. Ecris-nous si tu as une difficulté.
</video>

!!! tip "Raccourcis clavier"
    Tu peux également utiliser la touche **Suppr** de ton clavier : sélectionne l'élément puis appuie sur la touche.


### Images

Dans la section images on peut modifier les photos et les symboles présents sur le visuel. Cette fonction est utile pour personnaliser le visuel avec tes propres photos ou en choisir d’autres dans la banque d’images d’Aktivisda.

Il est également possible de modifier les couleurs et la taille des images et symboles du visuel.

👉 Pour **modifier les photos** présentes sur le visuel : clique sur la photo que tu souhaites modifier, puis :

clique sur le bouton **“Ouvrir galerie”** si tu veux choisir une autre photo parmi celles qui sont disponible sur Aktivisda :

<video controls width="720" class="md-img-border">
  <source src="../media/aktivisda-charger-photo-galerie.webm" type='video/webm' />
  <source src="../media/aktivisda-charger-photo-galerie.mp4" type='video/mp4' />
  Ton navigateur ne semble pas prendre en charge les vidéos HTML5. Ecris-nous si tu as une difficulté.
</video>

clique sur le bouton **“Charger”** si tu veux utiliser une photo qui est sur ton ordinateur :

<video controls width="720" class="md-img-border">
  <source src="../media/aktivisda-charger-photo-ordinateur.webm" type='video/webm' />
  <source src="../media/aktivisda-charger-photo-ordinateur.mp4" type='video/mp4' />
  Ton navigateur ne semble pas prendre en charge les vidéos HTML5. Ecris-nous si tu as une difficulté.
</video>

!!! warning "Photo chargée depuis ton ordinateur"
    Les éléments que tu charges dans Aktivisda depuis ton ordinateur ne peuvent pas être partagés à d'autres personnes car ils ne sont pas disponibles dans la base de données d'Aktivisda. Tu peux écrire à [aktivisda@alternatiba.eu](mailto:aktivisda@alternatiba.eu) si tu souhaites ajouter des éléments à la base de données (symboles, photos).

👉 Pour **modifier les symboles** présents sur le visuel, procède de la même manière. tu peux également modifier la taille et/ou la couleur du symbole :

<video controls width="720" class="md-img-border">
  <source src="../media/aktivisda-modifier-symbole.webm" type='video/webm' />
  <source src="../media/aktivisda-modifier-symbole.mp4" type='video/mp4' />
  Ton navigateur ne semble pas prendre en charge les vidéos HTML5. Ecris-nous si tu as une difficulté.
</video>

!!! warning "Importance des crédits"
    Chaque fois que tu utilises des photos, vérifie bien d’avoir ajouté les **crédits photos** ! Cela permet de reconnaître l'auteur⋅trice de l'image. Ces crédits sont écrits dans le nom des photos de la banque d’image d’Aktivisda.

## Exporter le visuel

Une fois que tu as terminé d’éditer le visuel, tu peux procéder à son export sur ton ordinateur.

👉 Tu peux **exporter** le visuel sous différents formats en cliquant sur le bouton "Sauvegarder" en haut à droite :

<video controls width="720" class="md-img-border">
  <source src="../media/aktivisda-sauvegarder-visuel.webm" type='video/webm' />
  <source src="../media/aktivisda-sauvegarder-visuel.mp4" type='video/mp4' />
  Ton navigateur ne semble pas prendre en charge les vidéos HTML5. Ecris-nous si tu as une difficulté.
</video>

!!! warning "N'oublie pas de sauvegarder !"
    Attention : une fois que tu as fini d'éditer le visuel et que tu fermes le site Aktivisda, tes modifications seront réinitialisées !

    Tu peux sauvegarder ton travail en cliquant sur le bouton "Sauvegarder" en haut à droite, puis sur "Sauvegarder". Cela te permet de télécharger un fichier au format JSON qui convient tout ton visuel. Tu peux ensuite le charger dans Aktivisda pour reprendre tes modifications.

!!! question "Quel format choisir ?"
    Il est recommandé d'exporter les visuels en **png** et **jpg** parce la version pdf peut rencontrer quelques bugs pour certains éléments.

    Tu verras également la fonctionnalité "partage par lien", mais nous te déconseillons de l'utiliser pour l'instant car le lien généré peut être très très long !


👏 Bravo ! Tu es arrivé·e à la fin de ce tutoriel ! Maintenant tu peux commencer à éditer tes visuels. 

🔎 Tu es curieux⋅se et tu veux en savoir plus sur Aktivisda ? Rendez-vous sur [le site du projet](https://aktivisda.earth/).

---

Tu es perdu⋅e ? Tu peux écrire à [aktivisda@alternatiba.eu](mailto:aktivisda@alternatiba.eu) ou sur le canal [⁉️ Questions et annonces](https://chat.alternatiba.eu/coordination-du-mouvement/channels/town-square). On est là pour t’aider ! :slight_smile:
