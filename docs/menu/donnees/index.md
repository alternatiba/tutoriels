---
title: Outils de données
author: Alternatiba / ANV-COP21
date: Janvier 2025
theme: beige
icon: material/database
---

Retrouve tous les tutoriels des outils de données dans le menu de navigation ou bien ci-dessous :

- [:simple-baserow: Baserow](../../baserow), outil de base de données
- [:fontawesome-solid-map-pin: Transiscope](../../transiscope), carte des alternatives
- [:fontawesome-solid-calendar-day: NOÉ](../../noe), pour organiser des événements

---

Tu es perdu⋅e ? Tu peux écrire à [informatique@alternatiba.eu](mailto:informatique@alternatiba.eu) ou sur le canal [⁉️ Questions et annonces](https://chat.alternatiba.eu/coordination-du-mouvement/channels/town-square). On est là pour t'aider ! :slight_smile:
