---
title: Outils de communication
author: Alternatiba / ANV-COP21
date: Mars 2025
theme: beige
icon: fontawesome/solid/comments
---

Retrouve tous les tutoriels des outils de communication dans le menu de navigation ou bien ci-dessous :

- [:simple-mattermost: Mattermost](../../mattermost), messagerie du mouvement
- [:material-palette: Aktivisda](../../aktivisda), pour la création de visuels
- [:material-email: Mail](../../mail), pour gérer son adresse mail ou sa liste mail Alternatiba

---

Tu es perdu⋅e ? Tu peux écrire à [informatique@alternatiba.eu](mailto:informatique@alternatiba.eu) ou sur le canal [⁉️ Questions et annonces](https://chat.alternatiba.eu/coordination-du-mouvement/channels/town-square). On est là pour t'aider ! :slight_smile:
