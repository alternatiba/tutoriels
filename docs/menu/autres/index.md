---
title: Autres outils
author: Alternatiba / ANV-COP21
date: Janvier 2025
theme: beige
icon: fontawesome/solid/toolbox
---

Retrouve tous les tutoriels des autres outils dans le menu de navigation ou bien ci-dessous :

- [:simple-bitwarden: Bitwarden](../../bitwarden), gestionnaire de mots de passe
- [:simple-googledrive: Google Drive](../../google-drive), pour partager des documents
- [:material-poll: Framavox](../../framavox), outil de prise de décision

:toolbox: Retrouve également tous les outils et liens utiles dans **[la caisse à outils du mouvement](https://outils.alternatiba.eu)** !

---

Tu es perdu⋅e ? Tu peux écrire à [informatique@alternatiba.eu](mailto:informatique@alternatiba.eu) ou sur le canal [⁉️ Questions et annonces](https://chat.alternatiba.eu/coordination-du-mouvement/channels/town-square). On est là pour t'aider ! :slight_smile:
