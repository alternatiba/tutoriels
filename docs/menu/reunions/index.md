---
title: Outils de réunions
author: Alternatiba / ANV-COP21
date: Janvier 2025
theme: beige
icon: fontawesome/solid/users
---

Retrouve tous les tutoriels des outils de réunion dans le menu de navigation ou bien ci-dessous :

- [:material-video: Zoom](../../zoom), pour faire des réunions en audio/vidéo
- [:simple-bigbluebutton: BigBlueButton](../../bbb), pour faire des réunions en audio et vidéo
- [:simple-mumble: Mumble](../../mumble), pour faire des réunions en audio

---

Tu es perdu⋅e ? Tu peux écrire à [informatique@alternatiba.eu](mailto:informatique@alternatiba.eu) ou sur le canal [⁉️ Questions et annonces](https://chat.alternatiba.eu/coordination-du-mouvement/channels/town-square). On est là pour t'aider ! :slight_smile:
