---
title: NOÉ
author: Alternatiba / ANV-COP21
date: Janvier 2025
theme: beige
icon: fontawesome/solid/calendar-day
---

[NOÉ](https://get.noe-app.io/) est un outil open-source pour simplifier la gestion de grands évènements en centralisant les inscriptions, le planning de bénévolat et la programmation événementielle. Il a été créé à la base pour le Camp Climat 2019 !

Retrouve [le tuto organisateur·ice ici](./orga), [le tuto participant·e ici](./user.md) et [les tutos vidéo NOÉ là](https://www.youtube.com/playlist?list=PLxTDjZn9usXIewHmWius7tiWdcm6zdcAY) !

---

Tu es perdu⋅e ? Tu peux écrire à [informatique@alternatiba.eu](mailto:informatique@alternatiba.eu) ou sur le canal [⁉️ Questions et annonces](https://chat.alternatiba.eu/coordination-du-mouvement/channels/town-square). On est là pour t'aider ! :slight_smile:
