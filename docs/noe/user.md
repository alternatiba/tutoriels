---
title: Utiliser NOÉ - Participant·e
author: Alternatiba / ANV-COP21
date: Janvier 2025
theme: beige
---

## Contexte

[NOÉ](https://get.noe-app.io/) est un outil open-source pour simplifier la gestion de grands évènements en centralisant les inscriptions, le planning de bénévolat et la programmation événementielle. Il a été créé à la base pour le Camp Climat 2019 !

Tutos NOÉ (vidéo) : <https://www.youtube.com/playlist?list=PLxTDjZn9usXIewHmWius7tiWdcm6zdcAY>

Exemple de lien pour le Camp Climat 2025 : <https://noe-app.io/camp-climat-2025>

!!! tip "Tuto participant·e"

    Ce tutoriel concerne la vue de participant·e, pour la vue d'organisateur·ice c'est [par ici](./orga.md) !

## Fonctionnalités

Parcours des fonctionnalités en suivant le menu de gauche :

??? info "Accueil : page d'accueil participant·e _(à partir de l'étape de pré-inscription)_"

    ![Accueil](./media/user-00-home.png)

    Cette page est personnalisée par l'orga et permet de s'informer sur l'évènement ainsi que de connecter ou de se créer un compte en tant que participant·e.

??? info "Mon inscription : dates, régime alimentaire... _(à partir de l'étape de pré-inscription)_"

    ![Mon inscription](./media/user-01-register.png)

---

??? info "S'inscrire aux activités : pour s'inscrire aux activités et créneaux de bénévolat _(à partir de l'étape d'inscription des encadrant·es)_"

    ![S'inscrire aux activités](./media/user-02-activities-low.png)

    - On peut s'inscrire et se désinscrire librement
    - Si on se désinscrit à la dernière minute, il y a un message "Êtes vous sûr·e ?" pour décourager les changements tardifs
    - On ne peut pas s'inscrire à plusieurs créneaux au même moment
    - Le bouton d'inscription grisé si on ne peut pas s'inscrire (pas là ou déjà inscrit·e à une autre activité en même temps)
    - On peut avoir été inscrit·e d'office à certaines activités

??? info "Mon planning : résumé de ses activités et créneaux de bénévolat _(à partir de l'étape d'inscription des encadrant·es)_"

    ![Mon planning (liste)](./media/user-03-planning-list.png)

    ![Mon planning (agenda)](./media/user-03-planning-agenda.png)

---

??? info "Jauge de bénévolat : permet de voir si on a pris la bonne quantité de créneaux de bénévolat"

    🔴 Rouge : pas assez 😕
    
    ![S'inscrire aux activités (rouge)](./media/user-02-activities-low.png)
    
    🟢 Vert : idéal 🤩
    
    ![S'inscrire aux activités (vert)](./media/user-02-activities-ok.png)
    
    🔵 Bleu : trop 😵
    
    ![S'inscrire aux activités (bleu)](./media/user-02-activities-high.png)

---

Tu es perdu⋅e ? Tu peux écrire à [informatique@alternatiba.eu](mailto:informatique@alternatiba.eu) ou sur le canal [⁉️ Questions et annonces](https://chat.alternatiba.eu/coordination-du-mouvement/channels/town-square). On est là pour t'aider ! :slight_smile:
