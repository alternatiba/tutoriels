---
title: Utiliser NOÉ - Organisateur·ice
author: Alternatiba / ANV-COP21
date: Janvier 2025
theme: beige
---

## Contexte

[NOÉ](https://get.noe-app.io/) est un outil open-source pour simplifier la gestion de grands évènements en centralisant les inscriptions, le planning de bénévolat et la programmation événementielle. Il a été créé à la base pour le Camp Climat 2019 !

Tutos NOÉ (vidéo) : <https://www.youtube.com/playlist?list=PLxTDjZn9usXIewHmWius7tiWdcm6zdcAY>

Exemple de lien pour le Camp Climat 2025 : <https://orga.noe-app.io/camp-climat-2025>

!!! tip "Tuto participant·e"

    Ce tutoriel concerne la vue d'organisateur·ice, pour la vue de participant·e c'est [par ici](./user.md) !

## Pour bien commencer

Il faut être invité·e par une personne de l'orga pour avoir accès à l'événement en mode orga.

Dans l'exemple du Camp Climat, c'est un événement auto-géré donc les participant·es devront s'inscrire à la fois aux activités et aux créneaux de bénévolat.

NOÉ est un outil central pour l'organisation d'un grand événement :

- **Programmation** → gestion des activités et sessions (à utiliser dès le début)
- **Mobilisation** → gestion des participant·es et encadrant·es
- **Communication** → gestion de la vue participant·es
- **Logistique** → gestion des espaces

## Fonctionnalités

Parcours des fonctionnalités en suivant le menu de gauche :

??? info "Cockpit: tableau de bord avec les informations principales dont un mémo d'équipe"

    ![Cockpit](./media/orga-01-cockpit.png)

??? info "Configuration : configuration générale de tout l’événement"

    ![Configuration](./media/orga-02-config.png)

    Étapes de l’événement :

    1. Fermé → l'orga crée les activités, sessions, etc.
    2. Pré-inscriptions → les participant·es payent (on ne voit pas le programme)
    3. Inscription des encadrant·es → les encadrant·es choisissent leurs activités
    4. Inscription pour tous·tes → les participant·es choisissent leurs activités

??? info "Participant·es (QUI) : les personnes qui participent à des sessions ou qui font du bénévolat"

    ![Participant·es](./media/orga-03-participants.png)

    On peut télécharger le planning individuel pour chaque personne avec le bouton PDF au début de chaque ligne

    ![Participant·e](./media/orga-03-participant.png)

---

??? info "Catégories : toutes les catégories d'activités"

    ![Catégories](./media/orga-04-categories.png)

??? info "Espaces (OÙ) : toutes les salles et espaces où se déroulent les activités"

    ![Espaces](./media/orga-05-spaces.png)

    ![Espace](./media/orga-05-space.png)

??? info "Encadrant·es (QUI) : les personnes qui encadrent / sont responsables des activités"

    ![Encadrant·es](./media/orga-06-respos.png)

    ![Encadrant·e](./media/orga-06-respo.png)

    Informations à renseigner :

    - Prénom, nom, téléphone
    - Résumé, notes privées pour l'orga
    - Disponibilités (facultatif)
    - Participant·e associé·e : inviter par mail ou associer avec un compte existant

??? info "Activités (QUOI) : toutes les activités (formation, concert, créneau de bénévolat...)"

    ![Activités](./media/orga-07-activities.png)

    Dans la liste, cliquer sur ⚙ en haut à droite pour changer l'affichage des colonnes

    ![Activité](./media/orga-07-activity.png)

    - Informations principales :
        - Nom de l'activité
        - Catégorie (obligatoire, on peut en créer à la volée)
    - Informations de bénévolat :
        - Coefficient de bénévolat
            - 0 pour les activités classiques (pas les créneaux de bénévolat)
            - 1 pour le bénévolat classique
            - Plus que 1 pour le bénévolat pénible (1.5 ou 2 par exemple)
            - Moins que 1 pour le bénévolat pépère (0.5 par exemple)
        - Nombre max de bénévoles
    - Informations secondaires :
        - Notes privées pour l'orga
        - Résumé court (quelques mots, vu par les personnes qui veulent s'inscrire)
        - Description détaillée (plus développé que le résumé court)

---

??? info "Sessions (QUOI + OÙ + QUAND) : activités + espaces + horaires"

    ![Sessions](./media/orga-08-sessions.png)

    ![Session](./media/orga-08-session.png)

    - Le nombre max de participant·es est pré-rempli avec l'activité mais peut être écrasé
    - On peut voir la liste des participant·es inscrit·es à cette session
    - La jauge d'une session prend le minimum entre la jauge de l'espace et de l'activité
    - On peut dupliquer ou supprimer une session en haut à droite

??? info "Planning des sessions : vue de toutes les sessions en mode calendrier"

    ![Planning des sessions](./media/orga-09-planning.png)

    - On peut changer la vue en haut : 1 jour, 3 jours, tout l'évènement...
    - On peut rechercher, filtrer par catégorie et grouper par espace en haut
    - On peut déplacer des sessions ou changer leur durée avec du cliquer-glisser

---

!!! info "Espace participant·e : lien vers la vue participane·es pour s'inscrire à des sessions"
!!! info "Mes événements : liste des différents événements dont on participe à l'orga"

## Conseils

- NOÉ est un logiciel complexe qui comprend **plein de fonctionnalités**, notamment des fonctionnalités qui ne sont pas présentées ici : tu peux fouiller et en découvrir plein d'autres
- Si tu as besoin d'**aide** ou que tu penses avoir **tout cassé**, envoie vite un message à [informatique@alternatiba.eu](mailto:informatique@alternatiba.eu)
- Si tu tombes sur **un bug** ou même **une fonctionnalité qui te manque** et qui n'existe pas encore, tu peux envoyer un message à [informatique@alternatiba.eu](mailto:informatique@alternatiba.eu) et on pourra faire remonter à l'équipe de développement (garde bien en tête que le développement prend du temps, donc il faut anticiper)
- Toujours mettre un **résumé** pour faciliter l'organisation (dans les activités, catégories...)
- **Double-cliquer** pour modifier un élément (session, activité...)
- Utiliser NOÉ **dès le début** pour la programmation (plutôt que sur un tableur dans un premier temps puis reporter plus tard)
- **Activité** ou **session** ?
    - Une session est la combinaison d'une activité, d'un espace et d'un horaire
    - Une activité peut se dérouler sur une ou plusieurs sessions
    - On s'inscrit donc à une session et non pas à une activité
- **Participant·e** ou **encadrant·e** ?
    - Les participant·es ne sont pas forcément associé·es à un·e encadrant·e
    - Tous·tes les encadrant·es doivent être associé·es à un·e participant·e
    - Seul·es les encadrant·es peuvent encadrer une activité
    - On peut créer les encadrant·es en avance pour préparer le planning tôt
- Pour devenir un.e vrai.e **pro de NOÉ** :
    - Centre d'aide : <https://get.noe-app.io/community/help-center>
    - Tutos vidéo : <https://www.youtube.com/playlist?list=PLxTDjZn9usXIewHmWius7tiWdcm6zdcAY>
    - Replay de la formation Expert.es : <https://bbb.paquerette.eu/playback/presentation/2.3/f23cb736b7facb8080bd711c88ad87b9e948dbcb-1713801567644>
    - Replay de l'Atelier NOÉ x Camps Climat : <https://peertube.virtual-assembly.org/videos/watch/809c76c2-b2cb-4a95-82cc-0f185ae60717>

---

Tu es perdu⋅e ? Tu peux écrire à [informatique@alternatiba.eu](mailto:informatique@alternatiba.eu) ou sur le canal [⁉️ Questions et annonces](https://chat.alternatiba.eu/coordination-du-mouvement/channels/town-square). On est là pour t'aider ! :slight_smile:
