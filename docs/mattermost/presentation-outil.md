---
title: C'est quoi Mattermost ?
author: Alternatiba
date: Février 2024
theme: beige
---


## ⚙️ Les chouettes fonctionnalités de Mattermost

Voici une liste non exhaustive des fonctionnalités disponibles sur Mattermost :

- ✉️ Envoyer des messages sur différents types de canaux (salons publics ou privés, de personne à personne)
- 💬 **Fils de discussions** : on peut créer un fil pour répondre sur un sujet précis
- 📂 Envoi de fichiers, d'images et de gifs 🤡
- 👥 Création d'**équipes** pour séparer les canaux de la Coordination de ceux de mon groupe local
- 🔎 **Recherche** dans l'historique des messages
- ❗️ Organiser les fils en **catégories** que l'on peut rendre muettes ou mettre en favoris
- ✨ **Réactions** aux messages avec plusieurs emojis 
- 🔥 **Emojis personnalisés** *(ça c'est vraiment super ! 😍)*
- 🔌 Disponible en application **bureau**, sur **navigateur web** et en **applications mobiles** pour Android et Iphone
- ...

Beaucoup de fonctionnalités sont similaires à celles proposées par Rocket.Chat. On t'attend pour tester tout ça !

!!! info "Personnalisation et évolution"
    De nombreuses extensions peuvent être installées sur une instance Mattermost. Et comme nous sommes souverain⋅es sur notre instance, il est possible que des nouveautés arrivent de temps à autres !

## 👨‍👩‍👦 Une équipe globale et des équipes locales

L'instance Mattermost d'Alternatiba / ANV-COP21 fonctionne avec différentes **"équipes"** :

- **Une équipe "Coordination du mouvement"** regroupe l'ensemble des militant⋅es du mouvement : tout le monde peut accéder à différents canaux publics ainsi qu'aux groupes de travail ;
- **Chaque groupe local peut créer sa propre équipe** dans laquelle il peut organiser des propres canaux et membres en autonomie et indépendemment des autres équipes.

Il est très facile de naviguer d'une équipe à une autre, cela se fait en deux clics sur la même interface.

!!! tip "Des messages privés qui traversent les équipes"
    Tu peux envoyer un message privé à toutes les personnes inscrites sur l'instance Mattermost, **même si vous n'êtes pas dans une même équipe**. C'est pratique pour échanger directement d'un groupe local à un autre !

!!! note "Exemple"
    - L'équipe **Alternatiba Picsouville** regroupe tous les canaux du groupe local de Picsouville
    - Je peux passer dans l'équipe **Coordination du mouvement** pour lire les annonces générales et participer à l'organisation du Tour Alternatiba 2024
    - Je peux envoyer un message privé à Raymond du groupe de Mickeyville, même si je ne suis pas dans l'équipe **ANV-COP21 Mickeyville**


## 💬 Les canaux généraux

Voici quelques canaux généraux de l'équipe **"Coordination du mouvement"** :

* **📢 Général - infos importantes**, le fil principal pour les annonces générales qui concernent tout le mouvement
* **⁉ Questions et annonces**, pour poser tes questions quand tu ne sais pas où les poser
* **🌊 Propagation réseaux sociaux**, pour propager et faire grandir les publications sur les réseaux sociaux
* **💪 Le coin militant**, pour partager les actus militantes, les infos de luttes et des articles de fond
* **🆘 Aide Mattermost**, pour demander de l'aide technique sur l'utilisation de Mattermost

Et quelques canaux thématiques ouverts à toustes :

* Tour Alternatiba 2024
* Alternatives Territoriales
* Alternatibases
* Camps Climat
* Outils numériques
* ...

Tu peux retrouver ici [**une cartographie**](https://docs.google.com/drawings/d/17b8Z2_gM5Wb5JhBCzcMIJQ1I5OdKoCuSJu6DY5apWiE) de la plupart des canaux généraux. C'est une base, l'objectif est que ça vive, alors sens-toi libre de créer les canaux qui te plaisent !


## 📜 Charte d'utilisation de Mattermost 

Nous t’invitons à lire [**la charte d’utilisation de Mattermost**](https://docs.google.com/document/d/162m8BTQhNuHaOe2gmTWcGmideIF812gQ9zCUyzPMGMs/) qui fixe quelques règles permettant un bon usage collectif de l’outil.


## 🔥 Alors, on y va ?!

Et tu sais quoi ? On t'accompagne pour te lancer : c'est par [lààà](../premiers-pas) !

---

Tu es perdu⋅e ? Tu peux écrire à [mattermost@alternatiba.eu](mailto:mattermost@alternatiba.eu) ou sur le canal [🆘 Aide Mattermost](https://chat.alternatiba.eu/coordination-du-mouvement/channels/aide-mattermost). On est là pour t’aider ! :slight_smile: