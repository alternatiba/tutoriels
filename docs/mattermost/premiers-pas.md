---
title: Se lancer sur Mattermost
author: Alternatiba / ANV-COP21
date: Février 2024
theme: beige
---

## ✍️ S'inscrire sur Mattermost et rejoindre l'équipe globale

!!! warning "Pour l'inscription, utilise plutôt un *navigateur web*"

    Une fois que ton compte est créé, nous te conseillons d'utiliser [*l'application* (ordi ou mobile)](#applications-bureau-et-mobile)

!!! tip "Inscription et connection"

    **Pour t'inscrire** suis le tuto ci-dessous<br/>
    **Pour te connecter** utilise l'URL de serveur [chat.alternatiba.eu](chat.alternatiba.eu)

1. Ouvre **[ce lien](https://chat.alternatiba.eu/signup_user_complete/?id=4e3aqiofr7y47dgzmz1qj36rty)** dans un navigateur web.

2. Choisis **"Voir dans le navigateur"**.

    ![](media/mattermost-choix-navigateur.png){ loading=lazy .md-img-border}

3. Entre tes informations :

    ![](media/mattermost-inscription.png){ loading=lazy .md-img-border}

    !!! question "Quel nom choisir ?"

        A cette étape, tu peux choisir le **nom d'utilisateur** que tu veux. Exemple : **raymond.grenoble**
        
        Plus tard, tu devras choisir un **nom d'affichage** qui s'affichera quand tu écriras des messages. On t'en reparle [plus bas](#parametrer-son-pseudo) !

4. Jette un œil à ta boîte de messagerie et clique sur le lien de validation reçu par courriel :

    ![](media/mattermost-mail-verification.png){ loading=lazy .md-img-border}

    Et voilà, tu es inscrit⋅e ! :tada:

5. Connecte-toi... Hop ! Tu as rejoint le Mattermost d'Alternatiba / ANV-COP21, et plus particulièrement l'équipe **"Coordination du mouvement"** !

## 📱 Applications bureau et mobile

Tu peux utiliser Mattermost depuis un navigateur web en te rendant sur **[https://chat.alternatiba.eu](https://chat.alternatiba.eu)**, mais tu peux aussi installer une application (pour ordinateur ou pour ordiphone) si tu le souhaites.

1. Si tu n'as pas encore l'application, télécharge-la et passe ensuite directement au point 3.

    * Télécharger l'application bureau : [https://mattermost.com/download/](https://mattermost.com/download/)
    * Télécharge l'application mobile : cherche "Mattermost" dans ton catalogue d'applications

2. Si jamais tu utilises déjà Mattermost avec d'autres collectifs (XR ou sur une équipe Framateam par exemple), tu peux **ajouter un nouveau serveur** dans l'application pour pouvoir naviguer de l'un à l'autre très facilement depuis la même application. Pour ajouter le nouveau serveur, clique sur la flèche en haut à gauche près du nom du serveur que tu utilises déjà :

    ![](media/mattermost-nouveau-serveur.png){ loading=lazy .md-img-border}

    Puis clique sur **"Ajouter un serveur"**.

3. Entre les paramètres suivants dans l'application pour ajouter le serveur Alternatiba / ANV-COP21 :

    * URL du serveur : **<https://chat.alternatiba.eu>**
    * Nom d'affichage : **Alternatiba / ANV-COP21**

    ![](media/mattermost-config-serveur.png){ loading=lazy .md-img-border}  
  
## 🔎 Tour d'horizon de Mattermost

Afin de te présenter concrètement les fonctionnalités de base de Mattermost, faisons un petit tour de l'interface ainsi que des différents modules qu'elle contient !

![](media/mattermost-interface.png){ loading=lazy .md-img-border}

On trouve les différentes parties suivantes :

* 🔴 **L'équipe à laquelle tu es connecté·e** : en cliquant sur le nom de l'équipe, tu peux naviguer pour en rejoindre une autre ;

* ⚪ **Les différentes équipes de l'instance** à laquelle tu es connecté·e (ici, tu peux passer d'une équipe à l'autre facilement ;

* 🟡 **Les différents canaux (privés ou publics) de l'équipe** à laquelle tu es connecté·e ainsi que **tes messages persos** ;

    !!! question "Quels canaux rejoindre ?"
        En arrivant pour la première fois, tu es automatiquement ajouté⋅e aux canaux généraux les plus importants. Tu peux découvrir tous les autres espaces de discussion publics et les rejoindre en cliquant sur **Changer de canal**.

    ??? tip "Organiser les canaux par catégories"
        Tu peux ranger tes canaux par **Catégories**, que tu peux ensuite organiser indépendamment les unes des autres. Tu peux par exemple les mettre en sourdine, les trier ou les classer dans l'ordre que tu souhaites pour hiérarchiser l'information qui t'intéresse.

    ??? tip "Des messages privés qui traversent les équipes"
        Tu peux envoyer un message privé à toutes les personnes inscrites sur l'instance Mattermost, **même si vous n'êtes pas dans une même équipe**. C'est pratique pour échanger directement d'un groupe local à un autre !

* 🔵 Un menu pour **parcourir et rejoindre tous les canaux publics** ;

* 🟢 La **fenêtre de discussion principale** dans laquelle s'affiche les messages du canal courant. Tu peux faire défiler la fenêtre vers le haut pour voir les messages plus anciens. En haut s'affiche le nom du canal, une brêve description et les membres. Tu peux cliquer sur le nom du canal ou sur le ℹ️ pour voir plus d'options ;

* 🟣 Le **fil de discussion** (thread) en cours. Cela permet de répondre à un message posté dans le canal sur un sujet spécifique ;

* 🟤 Le **module de rédaction et de mise en forme des messages** se trouve en bas de la fenêtre ;

* 🟠 Une **barre d'outils** dans laquelle tu peux notamment :

    * faire une recherche dans les messages
    * parcourir les messages sur lesquels tu as été identifié·e
    * modifier les paramètres d'affichage de l'interface
    * modifier les paramètres de ton compte

##  🔀 Naviguer dans Mattermost

:point_right: Pour passer d'un **canal** à l'autre, clique sur le nom du canal dans la liste des canaux à gauche. 🟡

Tu peux organiser les canaux dans l'ordre que tu veux. Tu peux créer des catégories en cliquant sur l'icône ➕.

:point_right: Tes **messages privés** sont accessibles tout en bas de la liste des canaux. 🟡

:point_right: Pour répondre à un message dans un **fil**, clique sur le message dans le canal : la fenêtre "fil de discussion" s'ouvre à droite. 🟣

:point_right: Pour accéder aux canaux d'une autre **équipe**, clique sur l'icône de l'équipe dans la barre complètement à gauche. ⚪

**NB :** si tu n'as rejoint qu'une seule équipe, la barre n'apparaît pas. Tu peux rejoindre une autre équipe en cliquant sur le nom de ton équipe actuelle 🔴, puis sur **"Rejoindre une équipe"**.

??? tip "Naviguer plus vite que l'éclair !"
    Tu peux aussi naviguer facilement d'un canal à l'autre avec la fonction de recherche et le raccourci `Ctrl`+`K`. Plus d'infos sur [cette page](../maitriser). 😉

## 😎 Paramétrer son pseudo

Par défaut, c'est ton nom d'utilisateur qui est affiché lorsque tu envoies un message. Afin de faciliter la lecture de messages et visualiser rapidement avec qui on échange, nous t'invitons donc à modifier ton nom d'affichage. Pour cela :

1. clique sur ton image de profil (tout en haut à droite),

2. clique sur **Profil** dans le menu déroulant

    ![](media/mattermost-profil.gif){ loading=lazy .md-img-border}

3. modifie ensuite le champ **Pseudo**, c'est ce champ qui sera affiché aux autres utilisateur·ices

    👉 Pour plus de clarté, nous t'invitons à indiquer ton **prénom** et ta **ville** entre parenthèses. Par exemple : **Camille (Picsouville)**.
    Tu peux laisser le champ "Nom complet" vide.

4. n’oublie pas d’enregistrer !

    ![](media/mattermost-change-pseudo.gif){ loading=lazy .md-img-border}

## 💬 Envoyer des messages, répondre dans un fil

Lorsque tu es sur un canal (canal public, canal privé, message personnel), tu peux **poster un message** sur le fil principal afin que toutes les personnes qui sont également sur ce canal puisse le voir. Pour cela, il suffit de rédiger le message dans la zone de texte en bas. 🟤

Souvent, on souhaite répondre à un message spécifique sans nécessairement créer un nouveau message dans le fil principal. Pour cela, tu peux **répondre à un message** :

1. **Clique sur le message auquel tu souhaites répondre**

2. Cela va automatiquement **créer un fil de discussion à partir du message du canal principal** qui va apparaître sur la droite de ta fenêtre 🟣

3. Ecris désormais dans cette nouvelle conversation !

:point_right: Cela permet de **hiérarchiser le niveau de messages dans le fil principal** car on distingue les messages qui sont des réponses et ceux qui sont des messages directement postés dans le canal.

??? tip "Réagir, marquer, transférer, marquer non-lu"
    Grâce au menu qui apparaît lorsque tu passes la souris sur un message, tu peux y **réagir** directement avec des emojis (ça permet par exemple d'avoir un avis général sans polluer le canal avec des « je suis d'accord »!)

    Tu peux également le **marquer** pour le retrouver facilement depuis la barre d'outils, le **transférer** sur d'autres canaux ou encore le **marquer comme non-lu** pour y revenir plus tard.

    ![](media/mattermost-message.png){ loading=lazy .md-img-border}

## 👨‍👩‍👦 Et pour mon groupe local ?

Par défaut, tu es connecté⋅e à l'équipe **"Coordination du mouvement"** qui regroupe tous les membres du mouvement Alternatiba / ANV-COP21. Il est possible de créer une **équipe** pour chaque groupe local et d'avoir ainsi un espace dédié aux canaux de chaque groupe.

:point_right: Si tu veux créer ou gérer l'équipe de ton groupe local, c'est [**par ici**](../gerer-une-equipe) !

## 📜 Charte d'utilisation de Mattermost

Nous t’invitons à lire [la charte d’utilisation de Mattermost](https://docs.google.com/document/d/162m8BTQhNuHaOe2gmTWcGmideIF812gQ9zCUyzPMGMs/) qui fixe quelques règles permettant un bon usage collectif de l’outil.

---

Tu es perdu⋅e ? Tu peux écrire à [mattermost@alternatiba.eu](mailto:mattermost@alternatiba.eu) ou sur le canal [🆘 Aide Mattermost](https://chat.alternatiba.eu/coordination-du-mouvement/channels/aide-mattermost). On est là pour t’aider ! :slight_smile:
