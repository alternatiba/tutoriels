---
title: Maîtriser Mattermost
author: Alternatiba
date: Janvier 2024
theme: beige
---

!!! warning "Peinture fraîche"
    Ce tuto est encore en cours de rédaction... Une question ? Une suggestion d'amélioration ? Tu peux écrire à [mattermost@alternatiba.eu](mailto:mattermost@alternatiba.eu) 🙂

Voici quelques fonctionnalités intéressantes pour maîtriser pleinement Mattermost ! 😎

## Organiser ses canaux par catégories

Une fonctionnalité pratique de Mattermost est la possibilité de **ranger ses canaux par catégories** afin que certains canaux apparaissent au même endroit.

Pour créer une catégorie : 

1. Clique sur les 3 petits points à droite de **Canaux**

    ![](media/mattermost-creer-categorie.png){ loading=lazy .md-img-border}
    
2. Clique sur **Créer une catégorie**. Elle apparaît, vide, en haute de la liste de tes canaux.

3. Tu peux maintenant cliquer/déplacer les canaux pour les glisser dans les catégories que tu souhaites

    ![](media/mattermost-categorie.png){ loading=lazy .md-img-border}

!!! question "Visibilité des catégories"
    Les catégories sont visibles par toi seul⋅e. Tu peux donc organiser tes canaux comme tu le souhaites !

??? tip "Sourdine groupée"
    Tu peux mettre en sourdine toute une catégorie si tu ne souhaites plus avoir de notifications pour ces canaux.

    ![](media/mattermost-categorie-sourdine.png){ loading=lazy .md-img-border}


## Gérer les notifications

Par défaut, Mattermost envoie un mail de notification à chaque fois que tu reçois un message. Tu peux paramétrer de manière globale quelles notifications tu veux recevoir dans `Paramètres` > `Notifications`.

...à étoffer...

Eviter de passer à côté d'une information
!!! tip "Tu peux filtrer les canaux pour ne voir que ceux sur lesquels tu as des messages non-lus !"


## Option Fils de discussion repliable

Lorsqu'une personne répond à un message du fil principal d'un canal, cela crée automatiquement un fil de discussion sous ce message. Cela permet de hiérarchiser les niveaux d'information dans la discussion principale.

L'option « Fils de discussion repliable » (accessible dans Paramètres > Affichage) permet de définir comment les fils de discussion sont affichés dans ton interface. 

- Lorsque l'option est **activée** :

    - Les réponses aux messages ne sont pas affichées dans le fil principal : seul un compteur s'affiche sous le message pour indiquer le nombre de réponses ainsi que les avatars des personnes qui ont répondu 
    - Un bouton `Fils de discussion` est présent au-dessus de la liste de vos canaux. Il liste tous les fils de discussion dans lesquels tu as interagi. Tu peux filter uniquement ceux où tu as des messages non-lus.
    
- Lorsque l'option est **désactivée** : 

    - Les réponses s'affichent dans le fil principal du canal.
    - Les canaux avec des messages non-lus (fil principal ou fils de discussion) sont affichés dans une catégorie non-lus.

## 🖋️ Rédiger de beaux messages 

🎨 **Police**. Pour faciliter la lisibilité des messages, tu peux les **mettre en forme** dans la fenêtre de rédaction ! Cela permet de **faciliter leur lecture** par les autres utilisateur·rices en hiérarchisant l'information !

Pour écrire un bout de texte **en gras, en italique ou le barrer**, il faut l'entourer du symbole correspondant (sans espace) : 

Style | Symoble 
 :---: | :---:
Gras | **
Italique | *
Barré | ~~

!!! info "Tu peux aussi mettre en forme en sélectionnant un bout de texte et en cliquand sur le raccourci que tu souhaites sous le message."

🌐 **Ajouter un lien**. Pour faciliter la lisibilité des messages, tu peux cacher les liens derrière un texte puis éviter d'afficher un URL complet, souvent illisible. Pour cela,  il faut mettre le texte à afficher entre crochet (sans espace) suivi de l'URL entre parenthèses (sans espace). 

!!! tip "Méthode alternative 1 : copie l'url, écris le texte à afficher dans ton message, sélectionne-le puis fais `Ctrl` `V`."

!!! tip "Méthode alternative 2 : écris le texte à afficher, sélectionne-le puis `Ctrl` `K`. Copie l'URL à la place de « url »."

↩️ **Sauts de ligne**. Tu peux sauter des lignes grâce à `MAJ` `Entrée`.

🅰️ **Faire des titres**. Tu peux faire différents niveaux de titre en commençant les lignes par des #, ##, ## ou ####.

1️⃣ **Listes à puces et listes numérotées**. Tu peux ajouter des listes à puces en commençant un ligne par un tirer du 6. Tu peux ajouter un deuxième niveau d'indentation en ajoutant un espace devant le tiret. De la même manière, tu peux faire des listes numérotées jusqu'à deux niveaux d'indentation en remplaçant les tirets par des « 1. ».

➡️ **Notifier un·e utilisateur·rice**. Tu peux notifier un·e utilisateur·rice grâce à `@`. La liste des utilisateur·rices de l'équipe apparaît et tu peux sélectionner la personne que tu souhaites tagguer. Elle recevra une notification spécifique.

!!! info "Tu peux aussi notifier tou·tes les utilisateur·rices d'un canal grâce à `@all`. À ne faire qu'en cas de nécessité pour ne pas spammer tout le monde, notamment sur les canaux généraux."

➡️ **Lien vers un canal**. Tu peux créer facilement un lien vers un canal grâce à `~`. La liste des canaux de l'équipe apparaît et tu peux sélectionner celui que tu souhaites utiliser.

🔶 **Exemple** de message mis en forme :

![](media/mattermost-rediger-message.png){ loading=lazy .md-img-border}

Et son rendu : 

![](media/mattermost-resultat-message.png){ loading=lazy .md-img-border}

## 💡 Astuces / les trucs à savoir

![](media/mattermost-astuces.png){ loading=lazy .md-img-border}

🔎 **Fonction de recherche**. Une fonction recherche est disponible, grâce à la barre qui se trouve au milieu en haut de l'interface. La recherche s'effectue sur tous les canaux de toute les équipes dans lesquelles tu es. 

📜 **Navigation dans l'historique**. Tu peux **naviguer d'avant en arrière dans l'historique des canaux que tu as consulter** !

En cliquant sur le bouton "Retour" en haut à gauche qui me permet de voir la vue que j'avais avant. Par exemple : je suis sur le fil #alternatibases et je clique sur le fil #funky finance. Je clique sur le bouton "retour" et PAF : me revoilà sur le fil #alternatibases (alternatibases meilleur fil).
 
!!! tip "Tu peux aussi cliquer sur `Alt` `Gauche` ou `Alt` `Droite`  pour naviguer dans l'historique."

⌨️ **Quelques raccourcis clavier utiles**. Tu peux accéder à la liste des raccourcis clavier en cliquand sur le point d'interrogation à droite de la barre de recherche puis sur `Raccourcis clavier`. En voici quelques-uns :

- `Ctrl` `K` ouvre la fenêtre de parcours des canaux de l'équipe dans laquelle tu te trouves
- `Ctrl` `Maj` `K` ouvre la liste de tes messages personnels
- `Ctrl` `Maj` `M` ouvre la liste de tes notifications récentes (également accessible via le `@` en haut à droite)



---

Tu es perdu⋅e ? Tu peux écrire à [mattermost@alternatiba.eu](mailto:mattermost@alternatiba.eu) ou sur le canal [🆘 Aide Mattermost](https://chat.alternatiba.eu/coordination-du-mouvement/channels/aide-mattermost). On est là pour t’aider ! :slight_smile: