---
title: Créer et gérer l'équipe de son groupe local
author: Alternatiba / ANV-COP21
date: Janvier 2024
theme: beige
---

!!! warning "Peinture fraîche"
    Ce tuto est encore en cours de rédaction... Une question ? Une suggestion d'amélioration ? Tu peux écrire à [mattermost@alternatiba.eu](mailto:mattermost@alternatiba.eu) 🙂

## 🔥 Créer son équipe

!!! info "Pour éviter la multiplication d'instance et les erreurs, merci de ne créer qu'une seule équipe par groupe local !"

Pour créer une équipe pour son groupe local, c'est très simple ! Il suffit de : 

- Crée ton compte sur [le Mattermost Alternatiba / ANV-COP21](https://chat.alternatiba.eu).
- Connecte-toi.
- Dans le menu à gauche, clique sur le + sous les équipes auxquelles tu appartiens déjà.

    ![](media/mattermost-creer-equipe.png){ loading=lazy .md-img-border}

- Clique sur `Créer une équipe`.
- Choisis un nom d'équipe (le nom de ton groupe local).
- Modifie l'URL si besoin.
- Valide.

Tu as créé l'équipe de ton groupe local ! Elle apparaît maintenant dans le menu de gauche, sous l'icône de l'équipe de Coordination du mouvement ! 

![](media/mattermost-nouvelle-equipe.png){ loading=lazy .md-img-border}
    
- Double-clique dessus pour t'y déplacer !

!!! info "Tu peux te déplacer d'une équipe à une autre en double-cliquant sur les icônes du menu de gauche."

## 📊 Administrer son équipe 

### Première connexion et configuration générale

Lors de sa création, une équipe contient deux canaux par défaut : 

- le canal `Centre-ville` (avec une URL en /town-square) que l'on ne peut pas supprimer mais que l'on peut renommer
- le canal `Hors-sujet` que l'on peut supprimer

Tu peux maintenant paramétrer la configuration générale de ton équipe. Pour cela, 

- Clique sur la flèche à côté du nom de l'équipe, puis sur `Paramètres d'équipe`.

    ![](media/mattermost-parametres-equipe.png){ loading=lazy .md-img-border}
    
- Configure les différents paramètres : 
    - le **nom de l'équipe**
    - la **description de l'équipe**
    - une **icône** 
    - la possibilité ou non de limiter l'accès à l'équipe qu'à un certain nom de domaine (déconseillé)
    - la possibilité ou non d'autoriser tou·tes les membres de l'instance à rejoindre l'équipe sans passer par une invitation
    - la génération d'un code d'invitation
    
    ![](media/mattermost-parametres-generaux.png){ loading=lazy .md-img-border}

### Gestion de l'équipe 

!!! info "Dans la version gratuite de Mattermost, les droits associés aux différents rôles (Membre, Admin de canal et Admin d'équipe) sont les mêmes pour toutes les équipe de l'instance."

En tant qu'administrateur·rice de votre équipe, tu peux : 

- **Gérer les paramètres de tous les canaux** (privés ou publics) de l'équipe. Pour gérer les droits d'un canal de l'équipe :
    - Cliquer sur `Add channels`
    - Cliquer sur le nom du canal pour le rejoindre
    - Cliquer sur la flèche à droite du nom du canal pour accéder aux différentes options de gestion 
        
    !!! info "Tu peux notamment ajouter, supprimer et modifier les rôles des membres du canal et changer les infos du canal"
        
- **Gérer les droits des membres de l'équipe** :
    - Clique sur la flèche à côté du nom de l'équipe, puis sur `Gérer les membres`.
    - Tu peux alors nommer des membres Administrateur·rice d'équipe ou les rétrogarder Membre.
    
!!! info "Les administrateur·rices de l'instance sont également administrateur·rices de toutes les équipes."

---

Tu es perdu⋅e ? Tu peux écrire à [mattermost@alternatiba.eu](mailto:mattermost@alternatiba.eu) ou sur le canal [🆘 Aide Mattermost](https://chat.alternatiba.eu/coordination-du-mouvement/channels/aide-mattermost). On est là pour t’aider ! :slight_smile:
