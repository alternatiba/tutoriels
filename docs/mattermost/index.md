---
title: Mattermost
author: Alternatiba / ANV-COP21
date: Février 2024
theme: beige
icon: simple/mattermost
---

![Mattermost](media/mattermost.png){ align=left }

[Mattermost](https://mattermost.com/) est le logiciel de messagerie instantanée qui remplace Rocket.Chat en tant qu'**outil de communication interne du mouvement Alternatiba / ANV-COP21** à partir de février 2024.

Avec son système d'équipes, c'est un outil qui peut être utilisé à la fois au sein d'un groupe local, et à la fois pour l'ensemble du réseau.

!!! tip "Inscription et connection"

    **Pour t'inscrire** suis [ce tuto](./premiers-pas/#sinscrire-sur-mattermost-et-rejoindre-lequipe-globale)<br/>
    **Pour te connecter** utilise l'URL de serveur [chat.alternatiba.eu](chat.alternatiba.eu)

!!! info "🚀 Je me lance"

    - [C'est quoi Mattermost ?](./presentation-outil)
    - [Mes premiers pas sur Mattermost](./premiers-pas)
    - [Maîtriser Mattermost comme un⋅e pro](./maitriser)
    - [Créer et gérer l'équipe de mon groupe local](./gerer-une-equipe)

## 👥 Pourquoi un outil commun ?

Permettre à tous les groupes locaux qui le souhaitent d'avoir **un outil de discussion interne commun** est un enjeu fort pour le mouvement Alternatiba / ANV-COP21. Cela nous permet de :

- **échanger facilement entre groupes locaux et militant·es du réseau** sur des sujets spécifiques (mais aussi, bien sûr, de tout et de rien) ;
- **faciliter la communication** entre la Team, l'Équipe de Gestion Quotidienne et les groupes locaux et militant⋅es ;
- **communiquer les actualités du mouvement à tout le réseau**, y compris aux personnes ne faisant pas partie d'un groupe local Alternatiba / ANV-COP21 ;
- **recruter de nouvelleaux militant·es, bénévoles et activistes** sur les différents projets et événements que nous organisons grâce à un canal de communication direct ;
- et enfin, de s'**organiser collectivement efficacement pour les projets comme le Tour Alternatiba 2024 ou le Camp Climat 2025** !

!!! info "En une phrase"
    L'objectif de la mise en place de cet outil est similaire à celui que nous avions sur Rocket.Chat : mettre à disposition de tout le mouvement un outil de communication commun pour favoriser les liens internes.

## 🌱 Un logiciel libre et un hébergement éthique

**Mattermost est un logiciel libre** (c'est-à-dire qu'on peut librement consulter son code, le télécharger, le modifier et le partager). C'est un choix qui va dans le sens des valeurs du mouvement, et c'est la condition nécessaire pour maîtriser le fonctionnement du logiciel.

Nous hébergeons notre instance chez [Le Cloud Girofle](https://girofle.cloud), qui est membre du collectif [CHATONS](https://chatons.org) (Collectif des Hébergeurs Alternatifs Transparents Ouverts Neutres et Solidaires). Ce qui nous plaît chez cet hébergeur :

- le respect de notre **vie privée** et la **souveraineté** sur nos données ;
- la valorisation d'un **écosystème numérique émancipateur et alternatif** au modèle propriétaire des géants du web ;
- le **lien humain** avec les gens qui hébergent nos données ;
- la **cohérence** avec les **valeurs** d'Alternatiba / ANV-COP21 (sensibilité et engagement écologistes forts) ;
- la **gouvernance horizontale** de l'association.

??? question "Pourquoi quitter Rocket.Chat ?"
    Même si Rocket.Chat est un logiciel libre, son développement est géré par une entreprise qui a pris une direction de moins en moins communautaire en mettant davantage d'énergie pour le développement de fonctionnalités payantes pour les entreprises.

    La version gratuite communautaire du logiciel s'est retrouvée dégradée et notre hébergeur IndieHosters a pris la décision d'arrêter d'héberger des instances Rocket.Chat. Ca a été l'occasion de changer d'outil !

??? question "Mattermost, c'est pas l'outil d'XR ?"
    Effectivement, il se trouve que le mouvement [Extinction Rebellion](https://rebellion.global/) a fait le choix de Mattermost il y a quelques années. Nous utilisons donc le même logiciel, mais XR a sa propre instance Mattermost, distincte de celle d'Alternatiba / ANV-COP21.

    Mattermost est aussi le logiciel qui se cache derrière [Framateam](https://framateam.org), l'instance gérée par [Framasoft](https://framasoft.org).

    Comme c'est le même logiciel qui est utilisé, il est facile de passer d'un serveur à un autre quand on utilise l'application de bureau ou l'application mobile. Pratique pour les militant⋅es qui sont dans plusieurs collectifs !

??? question "Mon groupe local est déjà sur Framateam !"
    Effectivement [Framateam](https://framateam.org) repose sur le même logiciel Mattermost, mais il s'agit d'une autre *instance* bien distincte de celle d'Alternatiba / ANV-COP21. Ca vaut le coup d'envisager de migrer les militant⋅es de ton groupe sur l'instance du mouvement pour faciliter la communication ! Tu peux nous écrire pour qu'on en parle ensemble : [mattermost@alternatiba.eu](mailto::mattermost@alternatiba.eu)

??? question "Rocket.Chat n'est plus accessible ?"
    Si, notre instance Rocket.Chat reste accessible en lecture seule pendant quelques temps à l'adresse [https://chat-archives.alternatiba.eu](https://chat-archives.alternatiba.eu).

!!! info "🔥 C'est parti ?"

    Pour nous rejoindre, c'est super simple ! Il suffit de suivre ces quelques liens pour te lancer :

    - [C'est quoi Mattermost ?](./presentation-outil)
    - [Mes premiers pas sur Mattermost](./premiers-pas)
    - [Maîtriser Mattermost comme un⋅e pro](./maitriser)
    - [Créer et gérer l'équipe de mon groupe local](./gerer-une-equipe)

---

Tu es perdu⋅e ? Tu peux écrire à [mattermost@alternatiba.eu](mailto:mattermost@alternatiba.eu) ou sur le canal [🆘 Aide Mattermost](https://chat.alternatiba.eu/coordination-du-mouvement/channels/aide-mattermost). On est là pour t’aider ! :slight_smile:
