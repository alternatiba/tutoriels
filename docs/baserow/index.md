---
title: Baserow
author: Alternatiba / ANV-COP21
date: Août 2023
theme: beige
icon: simple/baserow
---

## 🤔 C'est quoi Baserow ?

**Baserow est un outil libre de gestion de base de données** ! L’équipe d’animation d’Alternatiba / ANV-COP21 utilise <a href="https://baserow.io" target="_blank">Baserow</a> pour le suivi et l’animation du réseau. Il nous permet de maintenir nos données hors de Google Sheets complexes à maintenir et hébergés chez Google !

Toutes les informations sont par [ici](/baserow/se-lancer-sur-baserow) !

## 📌 Mise à jour des informations

Baserow nous permet de générer des formulaires qui nous permettent de **suivre la dynamique du réseau** !

On s'en sert notamment pour envoyer automatiquement le mail de [mise à jour mensuelle](/baserow/mise-a-jour-mensuelle) des informations des groupes et des militant⋅es et on prévoir d'en faire de même pour le [suivi des mobilisations](/baserow/cest-quoi-baserow), des informations des Alternatibases ou encore des événements du réseau !

## 📊 Vous lancer sur Baserow ?

🆕 Nous mettons également à disposition notre instance Baserow aux groupes locaux qui le souhaitent ainsi qu'un modèle générique de structure du base de données pour vous lancer ! Et nous vous accompagnons si vous avez des questions :)

Si cela vous intéresse, vous pouvez :

- [Vous lancer sur Baserow](/baserow/se-lancer-sur-baserow)
- [Mettre en place votre base de données](/baserow/creation-dune-bdd)
- [Utiliser Baserow](/baserow/utilisation-de-baserow)

Si tu es perdu⋅e, tu peux écrire à [baserow@alternatiba.eu](mailto:baserow@alternatiba.eu). On est là pour t’aider ! :slight_smile:
