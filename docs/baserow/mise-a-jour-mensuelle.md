---
title: Mise à jour mensuelle des informations
author: Alternatiba / ANV-COP21
date: Août 2023
theme: beige
---

## 📫 Mail mensuel de mise à jour

Chaque mois, les référent⋅es de groupe local reçoivent un courriel les invitant à vérifier **les informations de leur groupe** ainsi que **les coordonnées des personnes ayant des rôles particuliers** (référent⋅e ANV-COP21, référent⋅e finance, référent⋅e communication...).

Voilà à quoi ressemble le mail :

![](media/baserow-mail-maj.png){ loading=lazy .md-img-border }



## 📍 Mettre à jour les infos du groupe local

Si les informations indiquées dans le courriel ne sont plus à jour (par exemple, l'adresse du site web a changé)

1. Clique sur le lien dans le courriel, sous les infos du groupe local

2. Tu es redirigé⋅e vers un formulaire pré-rempli avec les informations actuelles

    ![](media/baserow-form-maj-gl.png){ loading=lazy .md-img-border }

3. Il te suffit de modifier les informations qui ont changé

4. Tu peux aussi ajouter des infos et actus sur ton groupe local dans le champ dédié à la fin du formulaire

5. Clique sur "Valider !" en bas du formulaire, et voilà !


!!! warning "Attention"

    Ne modifie pas le **nom du groupe** dans la première case !

    Si tu souhaites modifier d'autres informations, écris un message à [baserow@alternatiba.eu](mailto:baserow@alternatiba.eu)



## 😎 Mettre à jour les infos d'un⋅e militant⋅e

Les informations de chaque personne ayant un rôle dans le groupe sont indiquées dans le message.


!!! warning "Attention"

    Pour **ajouter une nouvelle personne** qui n'est pas listée dans le courriel, utilise le lien "Ajouter un⋅e nouveau⋅lle militant⋅e" (cf. [cette section](#ajouter-une-militante))


Si des informations ne sont plus à jour (par exemple, le numéro de téléphone a changé)

1. Clique sur le lien dans le courriel, sous les infos de la personne concernée

2. Tu es redirigé⋅e vers un formulaire pré-rempli avec les informations actuelles

3. Il te suffit de modifier les informations qui ont changé

4. Dans le champ "Mise à jour des rôles" à la fin, tu peux indiquer des changements de rôles et toute autre info utile

5. Clique sur "Valider !" en bas du formulaire, et voilà !

!!! tip "Changement de rôle"

	Si la personne n'a plus le rôle indiqué (elle a changé de rôle, elle a quitté le groupe...) indique-le en laissant le champ "rôle" vide. (cf. [cette section](#retirer-un-role-dune-militante))


## ➕ Ajouter un⋅e militant⋅e

Si une personne dans le groupe a pris des responsabilités (référent⋅e ANV, référent⋅e finances...)

1. Clique sur le lien dans le courriel, tout en bas du message

2. Tu obtiens un formulaire vierge pour ajouter une nouvelle personne

3. Remplis le formulaire, tout simplement

4. Indique bien le ou les rôles de la personne à la fin

5. Clique sur "Valider !" en bas du formulaire, et voilà !

!!! warning "Attention"

    Ne clique pas sur le lien "Ajouter un⋅e militant⋅e" si la personne est déjà listée plus haut dans le message : clique plutôt sur le lien de mise à jour des informations de la personne ! (cf. [cette section](#mettre-a-jour-les-infos-dune-militante))


## ➖ Retirer un rôle d'un⋅e militant⋅e

Si une personne n'a plus le rôle qu'elle avait apparavant, ou si elle a quitté le groupe

1. Clique sur le lien dans le courriel, sous les infos de la personne concernée

2. Tu es redirigé⋅e vers un formulaire pré-rempli avec les informations actuelles

3. Dans le champ "Mise à jour des rôles" à la fin, laisse la case vide

4. Clique sur "Valider !" en bas du formulaire, et voilà !



## 💡 Exemples de cas concrets

!!! question "Le groupe a une deuxième page Instagram"
    Clique sur le lien de [mise à jour des infos du groupe](#mettre-a-jour-les-infos-du-groupe-local), et ajoute l'URL de la page Instagram dans le champ "Instagram 2"

!!! question "Raymond a changé de numéro de portable"
	Clique sur le lien de [mise à jour des infos de Raymond](#mettre-a-jour-les-infos-dune-militante), et modifie le numéro de téléphone dans le formulaire

!!! question "Anne a un nouveau rôle"
	Clique sur le lien de [mise à jour des infos d'Anne](#mettre-a-jour-les-infos-dune-militante), et ajoute le rôle dans le champ en bas du formulaire

!!! question "Raymond n'est plus référent Alternatiba, c'est désormais Gertrude"
	- Clique sur le lien de [mise à jour des infos de Raymond](#mettre-a-jour-les-infos-dune-militante), et supprime le rôle "Réf Alternatiba" dans le champ en bas du formulaire
	- Clique sur le lien d'[ajout d'une nouvelle personne](#ajouter-une-militante) et remplis les informations de Gertrude en indiquant à la fin le rôle "Réf Alternatiba"

!!! question "Je souhaite être retiré⋅e de la base de données"
	Envoie un message à [baserow@alternatiba.eu](mailto:baserow@alternatiba.eu) :slight_smile:

!!! question "Le groupe change de nom"
	Envoie un message à [baserow@alternatiba.eu](mailto:baserow@alternatiba.eu) :slight_smile:
