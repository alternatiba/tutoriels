---
title: Mise en place de ma première base de données
author: Alternatiba / ANV-COP21
date: Décembre 2023
theme: beige
---

Vous avez créé votre compte et avez accès à votre projet ! **C'est parti !**

## L'interface d'accueil de Baserow 

Une fois sur la page d'accueil, vous avez accès à différentes informations et boutons, parmi lesquels :

- une page principal, comprenant :
    - un récapitulatif de vos projets et base de données
    - des liens vers des ressources utiles (tutos Baserow, documentation Baserow, forum Baserow, etc.)
- un menu à gauche de la page comprenant
    - un accès vers les paramètres de votre compte (sous votre nom, en haut à gauche)
    - le projet courant
    - un bouton pour envoyer des invitations à rejoindre le projet 
    - un bouton pour voir les membres du projet
    - un bouton pour créer une base de données

![](media/baserow-page-accueil.png){ loading=lazy .md-img-border }


## Créer une base de données

Si vous vous connectez pour la première fois, vous devez **créer votre première base de données** !

1. Cliquer sur `Nouveau`.
2. Vous avez ensuite le choix entre 
    1. `Base de données` : Créer une base de données vierge et créer vous-mêmes toutes les tables et les champs !
    2. `À partir d'un modèle` Créer votre base de données à partir d'un modèle pré-construit par Baserow. Elle est ensuite complètement personnalisable selon vos besoins! *À venir : nous avons créé un modèle générique de base de données qui peut vous orienter et vous aider à commencer !*
3. Normalement, la base de données s'ouvre alors automatiquement mais si ce n'est pas le cas, vous pouvez l'ouvrir en double-cliquant dessus dans le menu à gauche

## Un peu de vocabulaire pour commencer 

Avant de rentrer dans les actions concrètes, voici une rapide présentation de quelques mots de vocabulaire pour faciliter la compréhension et l'exploration de la base de données :)

- 🏙️ **Projet**  : ensemble de base de données (indépendantes). Un compte utilisateur permet d’accéder à tout un projet.
- 🏠 **Base de données**  : ensemble de tableaux (tables) qui peuvent être liés les uns avec les autres. 
- 📊 **Table** : un tableau
    - Ex. : Militant·es, Actions, Groupes de travail, etc.
- 🚪 **Entrée** : une ligne d'une table
- 🔑 **Clé** : la première colonne d'une table, elle permet d’identifier les entrées lorsque l'on va les lier entre elles d'une table à l'autre. _NB pour les connaisseur·ses : ce n'est pas la clé primaire qui, elle, est cachée._
- 👁️ **Vue** : une table sur laquelle on a appliqué des filtres. On peut partager une vue en lecture seule et la protéger par un mot depasse.
- 🩻 **Filtre**  : sous-ensemble d’entrées d’une table, déterminé par des conditions. S’applique pour tout le monde instantanément
- 🌾 **Champ** : une colonne. On peut définir plusieurs types (texte, URL, date, choix multiple, formule, etc.) pour les données qu'on y mettra.
    - Ex. : date de création du groupe ; téléphone d’une militante ; site internet
- 🔢 **Valeur**  : la donnée d’une entrée correspondant à un champ → une case


![](media/baserow-overview-table.png){ loading=lazy .md-img-border }


Vous allez maintenant pouvoir commencer à **manipuler les tables pour y ajouter vos données** !
