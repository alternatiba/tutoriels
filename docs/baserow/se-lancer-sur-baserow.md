---  
title: Se lancer sur Baserow  
author: Alternatiba / ANV-COP21  
date: Décembre 2023  
theme: beige  
---

L'équipe d'animation globale met à disposition une <a href="https://bdd.alternatiba.eu" target="_blank">instance Baserow</a> pour les groupes locaux qui souhaitent s'en servir :-)

## Comment ça fonctionne ?

Une instance Baserow peut contenir plusieurs **projets qui peuvent eux-mêmes contenir plusieurs bases de données**. Les accès sont gérés par des comptes individuels et les droits d'accès sont liés à un projet.

Nous proposons donc aux groupes qui le souhaitent d'**avoir leur propre projet avec des comptes administrateurs dédiés pour le gérer en autonomie** (inviter des nouveaux membres, révoquer des membres, créer des bases de données, des formulaires, etc.).

Il vous est donc possible d'avoir votre propre base de données, indépendante de celles des autres groupes ou de l'équipe globale. Vous n'aurez accès qu'à votre base de données et personne n'aura accès à vos données.

!!! question "Personne d'autre que nous n'aura accès à nos données ?"
    Notre instance est hébergée chez <a href="https://www.gresille.org/" target="_blank">Grésille</a>, un hébergeur associatif militant grenoblois, et installée sur une machine chiffrée. Seul·es les adminsys de Grésille et les administrateur·rices de l'instance (commission informatique d'Alternatiba / ANV-COP21) ont les accès à l'ensemble des données. Mais nous nous engageons à ne pas aller les consulter.

## Ça a l'air chouette, comment je me lance ?

Les étapes pour se lancer :

1. Contacter un·e admin de l'instance Baserow à l'adresse baserow@alternatiba.eu (ou directement la commission informatique).
    - On vous créera ensuite un projet et une invitation à vous créer un compte avec les droits admin sur ce projet.
2. Créer son compte grâce au lien d'invitation.
3. Se connecter à <a href="https://bdd.alternatiba.eu" target="_blank">Baserow</a>.
4. Commencer à mettre en place votre base de données !

!!! question "J'aimerais tester avant d'en parler dans mon groupe, c'est possible ?"
    Oui, tout à fait ! On peut te créer un projet « bac à sable » pour que vous puissiez tester ce qu'il est possible de faire, prendre en main l'outil, expérimenter !

!!! question "Je ne suis pas très à l'aise avec les outils numériques mais ça m'intéresse. Est-ce que c'est pour moi ?"
    Baserow est un outil intuitif et très simple d'utilisation et nous sommes à disposition pour faire de l'accompagnement, donc oui !

📫 Pour toute question, tu peux contacter la Baserow-team à l’adresse baserow@alternatiba.eu !
