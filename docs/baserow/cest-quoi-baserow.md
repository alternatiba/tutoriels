---
title: C'est quoi Baserow ?
author: Alternatiba / ANV-COP21
date: Août 2023
theme: beige
---

<a href="https://baserow.io" target="_blank">Baserow</a>, c'est un logiciel libre de gestion de base de données no-code.
L'équipe d'animation d'Alternatiba / ANV-COP21 l'utilise notamment pour le suivi et l'animation des groupes locaux du réseau.

## Pourquoi c'est pratique ?

L'idée d'une **base de données** est de pouvoir lier des informations les unes avec les autres. Par exemple, on associe facilement un⋅e militant⋅e à un groupe local, un groupe local à une mobilisation etc. Ainsi, Baserow permet de s'affranchir de multiples tableaux.

C'est également un logiciel **"no-code"**, c'est-à-dire qu'on peut manipuler facilement les données sans avoir à entrer des commandes ou à faire du code informatique.

## Pourquoi c'est sympa ?

C'est un **logiciel libre**, c'est-à-dire que le code source est accessible, utilisable et modifiable par n'importe qui. Cela permet d'avoir une bonne confiance au logiciel puisque ce n'est pas une "boîte noire".

L'instance [bdd.alternatiba.eu](https://bdd.alternatiba.eu) est hébergée par Alternatiba / ANV-COP21 chez un **hébergeur de confiance**, on maîtrise complètement les données stockées, elles ne sont pas sur des serveurs de Google ou Amazon.

## Et n8n ?

<a href="https://n8n.io" target="_blank">n8n</a> est un autre logiciel libre, également auto-hébergé, qui permet d'**automatiser des tâches**. Couplé à Baserow, cela en fait un outil puissant qui permet de générer des formulaires pré-remplis, d'envoyer des courriels, et tout ça automatiquement !

C'est notamment pour cela que les liens URL qui apparaissent dans les mails de mise à jour sont des liens qui commencent par *"n8n.alternatiba.eu"*
