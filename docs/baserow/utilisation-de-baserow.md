---
title: Utilisation de Baserow
author: Alternatiba / ANV-COP21
date: Décembre 2023
theme: beige
---

## Les opérations

### Créer une table

Les objets de base d'une base de données qui contiennent les données sont les **tables**, qui correspondent aux tableaux ! Pour créer une nouvelle table : 

1. Cliquer sur `+ Ajouter une table` qui apparaît sous la liste des tables de la base de données courantes dans le menu de gauche.
2. Choisir une méthode de création, soit vide, soit à partir d'une méthode existante si vous avez un fichier à importer.
3. Choisir le nom de la table.
4. Cliquer sur `Ajouter la table`.

👉 Vous voici maintenant devant une **table que vous pouvez maintenant personnaliser** ! 

### Créer un champ

Vous pouvez ajouter des nouveaux champs (colonne) à une table : 

1. Choisir où ajouter le champ : 

    - scroller à droite de la dernière colonne et cliquer sur `+` pour ajouter un champ tout à droite
    - cliquer sur la flèche à droite du nom d'un champ existant pour ajouter un champ à droite ou à gauche de celui-ci 

2. Choisir le nom du champ.
3. Choisir le type de champ.
4. Remplir les paramètres du champ, le cas échéant.
5. Cliquer sur `Créer`.

![](media/baserow-ajouter-champ.png){ loading=lazy .md-img-border }

👉 Le choix d'**un type de champs permet de forcer le format d'un type de données**. Il y en a différents, par exemple : 

- `Texte` / `Nombre` / `Date` : force à entrer le type de valeur choisi.
- `Téléphone` / `Courriel` / `URL` : force la valeur à respecter le format type (et rend la valeur cliquable).
- `Booléen` : oui / non.
- `Liste déroulante` et `Sélection multiple` : permet de choisir une valeur (si liste déroulante) ou plusieurs valeurs (si sélection multiple) parmi une liste de valeurs pré-définies. 
- `Lien vers table` : permet de choisir la valeur parmi les entrées d'une autre table (et de créer un lien cliquable pour en voir les infos)
    - _dans ma table `Groupe de travail`, je souhaite pouvoir associer à chaque groupe son référent (qui est une entrée de la table `Militant·es`): je crée dans la table `Groupe de travail` un champ nommé `Référent·es` qui est un lien vers la table `Militant·es`. Ainsi, lorsque je remplirai ce champ, je choisirai parmi la liste des clés de la table `Militant·es`._
- `Champ rapporté` : permet de choisir la valeur parmi les champs d'une autre table.
    - _dans l'exemple précédent, je souhaite avoir dans la table `Groupe de travail` un champ contenant l'adresse mail de son référent. Je crée un champ rapporté à partir du champ `Référent·es` pour pouvoir récupérer le mail associé._
- `Formule` : il est également posible d'utiliser des fonctions implémentées par Baserow afin de créer des champs personnalisés !

!!! tip "Pour scroller à droite de la base de données et voir tous les champs, vous pouvez utiliser `Maj`+ molette."

### Ajouter/supprimer une entrée

- Pour ajouter une entrée dans une table, il suffit de cliquer sur le bouton `+`sous la dernière entrée ou de faire un clic droit sur une entrée puis `Insérer au dessus`ou `Insérer en dessous` pour ajouter l'entrée. Vous pouvez également dupliquer la ligne sur laquelle vous avez cliquer.

- Pour supprimer une entrée, faites un clic droit puis `Insérer la ligne`.

!!! tip "Les données supprimées sont accessibles à la restauration pendant 3 jours dans la corbeille."

### Manipuler les valeurs

- Pour ajouter/modifier/supprimer des valeurs, il suffit de double-cliquer sur la valeur choisie et d'entrer la nouvelle valeur.
- On peut également sélectionner la case et appuyer sur `Suppr.`pour supprimer directement.
- Il est possible de cliquer/glisser pour sélectionner plusieurs valeurs afin de les supprimer ou les copier/coller.

!!! tip "`Ctrl`+ `Z` fonctionne en cas de **fausse manip**."

!!! tip "Il y a un c**hamp de recherche** en haut à droite de l'interface. La recherche s'applique à tous les champs visible dans la vue."

### Créer un filtre

👉 Il est possible de **n’afficher que certaines entrées d’une table** (pour récupérer les infos qu'on cherche : par exemple, uniquement les membres d'un GT en particulier ou les militant·es dont on n'a pas les adresses mail). Pour cela, on utilise les **filtres** !

On peut **filtrer une table grâce à une ou plusieurs conditions** (les conditions portent sur les champs de la table).

1. Se placer sur la table souhaitée.
2. Cliquer sur `Filtre` puis `Ajouter un filtre`.
3. Choisir le champ souhaité.
4. Définir la condition.
5. Ajouter un nouveau filtre si on le souhaite.
    - Plusieurs conditions peuvent être combinées avec ET (les deux conditions doivent être vérifiées) ou avec OU (au moins l’une des conditions est vérifiée).

![](media/baserow-filtre.png){ loading=lazy .md-img-border }

La vue affichée se met** automatiquement à jour en fonction du filtre choisi**.

### Créer une vue 

👉 **La vue par défaut de chaque table est partagée par tout le monde** : lorsque l’on y applique un filtre, ça modifie la vue de tou·tes les personnes qui sont en même temps sur cette vue. Par ailleurs, vous aurez peut-être le besoin d'**accéder régulièrement à certaines données filtrées** d'une table. 

Pour cela, **on peut « stocker » les filtres dans des vues** !

1. Cliquer sur la vue en cours. 
    - vous accédez à la liste des vues existantes sur cette table 
2. Cliquer sur Tableau, en bas du menu.
3. Entrer le nom de la vue.
4. Cliquer sur `Ajouter tableau`.

![](media/baserow-vue.png){ loading=lazy .md-img-border }

**Une fois la vue créée, vous pouvez maintenant y ajouter des filtres qui y seront sauvegardées**. Autrement dit, lorsque vous reviendrez plus tard sur cette vue, les filtres choisis seront toujours sélectionnés et actifs. 

!!! tip "Les filtres, tris, ordres de champs et masquages de champs que vous effectuez sur une vue ne sont visibles que sur celle-ci (vous n'embêtez pas vos camarades qui travaillent sur d'autres vues, mais celleux qui sont sur la même vue que vous seront affecté·es par vos modifications)."

!!! warning "Quand un champ est créé dans une vue, il apparaît automatiquement dans toutes les autres vues. Il faut donc masquer les nouveaux champs créés dans les autres vues si elles ne sont pas souhaitables."

### Partager une vue 

Il est possible de **partager une vue** (seulement en lecture seule) grâce à un lien. Pour cela, : 

- cliquer sur `Partager la vue`.
- cliquer sur `Créer un lien de partage privé vers la vue`.

Le lien est automatiquement généré. **Vous pouvez maintenant le copier pour le partager.**

![](media/baserow-partager-vue.png){ loading=lazy .md-img-border }

!!! tip "vous pouvez définir un mot de passe pour en restreindre l'accès (attention à la gestion du mot de passe).

!!! tip "Si vous pensez que le lien et/ou le mot de passe sont corrompus (ont fuité), vous pouvez réinitialiser l'un ou l'autre quand vous voulez.

!!! warning "Lorsqu'un nouveau champ est ajouté, il apparaîtra dans toutes les vues partagées car tous les champs non masqués apparaissent par défaut (et les nouveaux champs apparaissent par défaut dans toutes les vues). Donc, faites attention à bien masquer les champs que vous ne voulez pas pas partager avant de partager la vue."

### Créer un formulaire

Baserow permet **la création de formulaire associé à une table** ! Le formulaire permet à un·e utilisateur·rice de remplir les champs choisis d'une table.

1. Choisir la table à remplir.
2. Cliquer sur la vue en cours (pour Baserow, un formulaire est un type de vue de la table, c'est donc par ce menu que vous pourrez accéder de nouveau aux formulaires associés à la table).
3. Cliquer sur `Formulaire` en bas du menu.
4. Choisir un nom puis valider.
5. Personnaliser le formulaire (titre, description, image, champs à remplir)
6. Générer ensuite le lien, comme pour les vues. Vous pouvez également protéger le formulaire par un mot de passe.

Les soumissions du formulaire génèrent et remplissent automatiquement de nouvelles lignes de la table avec les champs remplis par l'utilisateur·rice et vide pour ceux qui n'apparaissent pas dans le formulaire.
