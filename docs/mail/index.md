---
title: Gérer ses mails
author: Alternatiba / ANV-COP21
date: Mars 2025
theme: beige
icon: material/email
---

!!! warning "Changement de serveur mail"

    **Depuis le 5 mars 2025, nous avons migré nos mails depuis notre ancien hébergeur OVH vers un hébergeur associatif (Grésille). La configuration des mails doit être mise à jour, et voici l'URL du nouveau webmail à utiliser : [https://webmail.gresille.org](https://webmail.gresille.org). Tu trouveras toutes les infos dans ces tutos mis à jour.**

Tous les tutoriels pour bien gérer ses boîtes mails et listes mails Alternatiba / ANV-COP21 :

- [Configurer et utiliser sa boîte mail Grésille](./config.md)
- [Configurer Thunderbird (logiciel de messagerie)](./thunderbird.md)
- [Utiliser le webmail Roundcube](./roundcube.md)
- [Mettre en place une signature email](./signature.md)
- [Créer une adresse e-mail](./creer-adresse.md)
- [Gérer une liste mail avec SYMPA](./listes.md)
- [Migrer ses contacts de OVH vers Grésille](./migration-contacts-mail.md)

---

Une question sur ces tutos ? Une remarque ? Fais signe à [informatique@alternatiba.eu](mailto:informatique@alternatiba.eu) si besoin ! On est là pour t'aider ! 🤓
