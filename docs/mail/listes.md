---
title: Gérer une liste mail avec SYMPA
author: Alternatiba / ANV-COP21
date: Mars 2025
theme: beige
---

!!! warning "Changement de serveur mail"

    **Depuis le 5 mars 2025, nous avons migré nos mails depuis notre ancien hébergeur OVH vers un hébergeur associatif (Grésille). La configuration des mails doit être mise à jour, et voici l'URL du nouveau webmail à utiliser : [https://webmail.gresille.org](https://webmail.gresille.org). Tu trouveras toutes les infos dans ces tutos mis à jour.**

## 📙 Contexte

Ce tutoriel est dédié aux personnes qui sont gestionnaires d'une liste mail **@listes.alternatiba.eu** ou **@listes.anv-cop21.org** :

- listes de contact des différents espaces du mouvement
- listes de communication interne des groupes de travail
- listes de communication de groupe local <ville.cercle@listes.alternatiba.eu> ou <ville.cercle@listes.anv-cop21.org> (exemple : <picsouville.benevoles@listes.alternatiba.eu>)

!!! info "Un hébergement associatif et un logiciel libre"

    La gestion des listes mail se fait grâce à [SYMPA](https://www.sympa.community/), un logiciel libre hébergé par [Grésille](https://www.gresille.org/) qui héberge également nos mails.

## ✉️ Accéder à l'nterface de gestion des listes

Tu peux y accéder à ces adresses :

- **<https://listes.alternatiba.eu>** (pour les listes mail en listes.alternatiba.eu)
- **<https://listes.anv-cop21.org>** (pour les listes mail en listes.anv-cop21.org)

### Création de son compte

**Les droits (propriétaires et modérateurs) sont associés à une adresse mail**. Il est donc nécessaire de se créer un compte avec l'adresse mail qui est abonnée et/ou propriétaire de la liste. Pour cela, il faut cliquer sur `Connexion` en haut à droite puis sur `Première connexion`.

![Connexion à l'instance SYMPA](./media/sympa-connexion.png){ loading=lazy : style="width:600px"}

Après avoir entré ton adresse de messagerie, tu reçois un message pour créer ton mot de passe. Tu peux ensuite te connecter !

Une fois ton compté créé, tu peux visualiser l'ensemble des listes visibles sur l'instance et à l'ensemble des listes sur lesquelles tu as des droits.

### Les différents niveaux de droits

Le logiciel SYMPA permets différents niveaux de droits :

- les **propriétaires**, qui peuvent paramétrer la liste, ajouter et supprimer des membres
- les **modérateurs**, qui peuvent modérer les mails qui sont envoyés sur la liste
- les **abonnés**, qui reçoivent les mails

## 🛠️ Configurer ma liste

![Interface du logiciel SYMPA](./media/sympa-interface.png){ loading=lazy : style="width:600px"}

En tant que propriétaire, tu peux :

- ajouter / modifier / supprimer des abonnés
- ajouter / modifier / supprimer des modérateurs
- ajouter / modifier / supprimer des propriétaires
- configurer la liste

Pour cela, clique sur `Admin` dans le menu à gauche. Tu accèdes alors à l'ensemble des paramètres que tu peux configurer. La plupart des opérations principales sont dans les menus `Configurer la liste` et `Les utilisateurs`.

![Interface propriétaire du logiciel SYMPA](./media/sympa-admin.png){ loading=lazy : style="width:600px"}

Parmi **les paramètres qu'il est possible de configurer** (liste non exhaustive, n'hésite pas à parcourir toi-même) :

- qui peut écrire à la liste
- la visibilité de la liste (sur la page d'accueil de l'instance)
- le texte affiché entre crochet dans l'objet des mails
- les politiques d'abonnement et de désabonnement
- la politique de réponse aux mails

!!! info "Tuto à venir"

    On n'a pas encore rédigé ce tuto. On arrive bientôt. Si tu as besoin d'aide, tu peux écrire à [listmaster@alternatiba.eu](mailto:listmaster@alternatiba.eu).

---

Une question sur ce tuto ? Une remarque ? Fais signe à [informatique@alternatiba.eu](mailto:informatique@alternatiba.eu) si besoin ! On est là pour t'aider ! 🤓
