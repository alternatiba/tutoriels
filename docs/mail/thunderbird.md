---
title: Configurer Thunderbird
author: Alternatiba / ANV-COP21
date: Mars 2025
theme: beige
---

!!! warning "Changement de serveur mail"

    **Depuis le 5 mars 2025, nous avons migré nos mails depuis notre ancien hébergeur OVH vers un hébergeur associatif (Grésille). La configuration des mails doit être mise à jour, et voici l'URL du nouveau webmail à utiliser : [https://webmail.gresille.org](https://webmail.gresille.org). Tu trouveras toutes les infos dans ces tutos mis à jour.**

!!! info "Premiers pas"

    Ce tuto est spécifique au logiciel Thunderbird. **Dans un premier temps, aller voir le tuto [Configurer sa boîte mail Grésille](./config.md).**

## 🪄 Ajouter son compte dans Thunderbird (ou des comptes supplémentaires)

1. Télécharge, installe et lance [Thunderbird](https://www.thunderbird.net)

2. Clique sur **Fichier** \> **Nouveau** \> **Compte courrier existant**

3. Entre tes informations de connexion et clique sur **Continuer**

    !!! info "Nom complet"

        Le "nom complet" est le nom d'affichage que les gens liront en recevant tes mails.

    ![Connection à Thunderbird](./media/thunderbird_login.png)

    !!! tip "Retenir le mot de passe"

        Nous conseillons de cocher "Retenir le mot de passe" **à condition que tu aies mis un mot de passe sur ta session d'ordinateur** (Préférences \> Vie privée et sécurité \> Utiliser un mot de passe principal).

4. Normalement, la configuration est automatiquement détectée. Si ce n'est pas le cas, cliquer sur "Configuration manuelle" puis entre les informations suivantes :


      - Serveur entrant
        - Protocole : IMAP
        - Nom d'hôte : imap.gresille.org
        - Port : 143
        - Sécurité de connexion : STARTTLS
        - Méthode d'authentification : Mot de passe normal
        - Nom d'utilisateur : ton adresse mail complète (ex : raymond.deubaze@alternatiba.eu)

      - Serveur sortant
        - Protocole : SMTP
        - Nom d'hôte : mail.gresille.org
        - Port : 587
        - Sécurité de connexion : STARTTLS
        - Méthode d'authentification : Mot de passe normal
        - Nom d'utilisateur : ton adresse mail complète (ex : raymond.deubaze@alternatiba.eu)

    !!! warning "Nom d'utilisateur"

        Le nom d'utilisateur est l'adresse mail **complète**, avec le "@alternatiba.eu"

    ![Paramètres serveur de Thunderbird](./media/thunderbird_server-parameter.png)

   1. Clique sur **Terminé** ou sur **Re-tester** et voilà \! Ton adresse mail apparaît sur le panneau de gauche et tes messages dans le grand panneau de droite :

    ![Adresse mail à gauche](./media/thunderbird_home-left.png)

    ![Message à droite](./media/thunderbird_home-right.png)

## 🚨 Houston, on a un problème (FAQ)

### Bug de certificat

Si tu tombes sur ce message, clique sur "Confirmer l'exception de sécurité" en gardant cochée la case "Conserver cette exception de façon permanente".

![Message d'erreur Thunderbird : exception de sécurité](./media/thunderbird_bug-certificat.jpeg){ loading=lazy : style="width:500px"}

### Bug d'enregistrement des mails (brouillon, envoyés...)

Il se peut que le logiciel n'enregistre pas correctement les mails dans les brouillons ou les mails envoyés.

1. Clique droit sur le compte de messagerie, puis "Paramètres"

    ![Paramètres Thunderbird](./media/thunderbird_settings.png){ loading=lazy : style="width:475px"}

2. Dans le menu "Copies et dossiers" :

      - Pour les messages envoyés (en haut de l'écran), sélectionner "Autre dossier" et aller chercher le dossier **INBOX \> Sent**.
      Si ça ne fonctionne pas, choisir un autre dossier. Vous pouvez créer vous même un dossier "Envoyés" si il n'existe pas.

        ![Paramètres Thunderbird > Copies et dossiers > Messages envoyés](./media/thunderbird_settings-sent.png){ loading=lazy : style="width:450px"}

      - Pour les brouillons (en bas de l'écran), sélectionner "Autre dossier" et aller chercher le dossier "Brouillon".

        ![Paramètres Thunderbird > Copies et dossiers > Brouillons](./media/thunderbird_settings-draft.png){ loading=lazy : style="width:450px"}

### Bug "Échec de la connexion"

Si tu obtiens ce message d'erreur, c'est certainement que le mot de passe de la boîte mail a été modifié :

![Message d'erreur Thunderbird : échec de connection](./media/thunderbird_echec-connection.png)

Pour résoudre le problème :

1. Clique sur **"Saisir un nouveau mot de passe"**
2. Récupère le nouveau mot de passe (dans Bitwarden, ou en demandant aux personnes concernées)
3. Entre le nouveau mot de passe et valide

Si ça ne fonctionne toujours pas, tu peux supprimer le compte et l'ajouter à nouveau :

1. Clique droit sur l'adresse mail dans le panneau de gauche \> **Paramètres**
2. En bas de la liste de gauche, cliquer sur **Gestion des comptes** \> **Supprimer le compte**
3. Ajoute à nouveau le compte en suivant [ce tuto](#ajouter-son-compte-dans-thunderbird-ou-des-comptes-supplementaires)

### Bug "Le serveur de courrier entrant n'existe pas"

Si vous subissez cette erreur, c'est certainement que vous avez changé de version de Windows, et que Windows a pété un boulon lors de la migration \! En deux mots : le profil Thunderbird a été migré, mais le logiciel ne prend pas toutes les infos en compte.

![Message d'erreur Thunderbird : le serveur de courrier entrant n'existe pas](./media/thunderbird_serveur-inexistant.png)

Voici comment la résoudre :

1. Dans l'explorateur de fichiers, entre **%APPDATA%** dans la barre en haut.<br/>
   Tu te retrouves dans le dossier : **C:\\Users\\Raymond\\AppData** (remplace "Raymond" par ton nom d'utilisateur)
2. Va dans le dossier **Roaming \> Thunderbird \> Profile**<br/>
   Dans ce dossier, il y a des dossiers sous la forme **xxxxxxx.default**. Ce sont les "profils Thunderbird" qui contiennent les données de configuration et les mails téléchargés sur l'ordinateur.
3. S'il y a plusieurs dossiers, cherche celui qui est le plus gros. C'est certainement le profil qui a été migré depuis l'ancienne version de Windows. Entre dans ce dossier.
4. Les configurations de serveur entrants sont dans le dossier **IMAPs**<br/>
   Renomme le dossier IMAPs en IMAPs\_old (pour garder une sauvegarde) puis crée un dossier IMAPs vide.
5. Redémarre Thunderbird, ça devrait aller mieux \! 😊

## 💡 Astuces Thunderbird

Si vous décidez d'utiliser Thunderbird, on vous a préparé plein d'astuces !

### Mettre en place une signature

Voir ce tuto : [Mettre en place une signature email](./signature.md).

### Afficher les mails par conversation

Pour avoir une vue un peu comme dans Gmail : installer l'extension [Thunderbird Conversations](https://addons.thunderbird.net/fr/thunderbird/addon/gmail-conversation-view/) \!

Voici [un tuto sympathique](https://www.arobase.org/thunderbird/regrouper-les-messages-avec-thunderbird-conversations.htm).

### Créer des filtres automatiques

Dans Thunderbird, il est possible de filtrer automatiquement les messages reçus dans la boîte de réception.

!!! info "Remarque"

    Le filtre ne se lance que lorsque Thunderbird est en fonctionnement, il ne faut pas s'attendre à ce que les messages soient filtrés lorsque l'ordinateur est éteint. Pour filtrer les messages dès la réception sur le serveur (marquer comme SPAM par exemple), il faut le faire dans l'interface d'administration de OVH.

Lorsqu'un message arrive dans la boîte de réception, on peut automatiquement :

- le déplacer dans un dossier
- le marquer comme "lu"
- le supprimer
- le transférer à une autre adresse
- ...

Pour cela :

1. Va dans **Outils** \> **Filtres de messages** : une fenêtre listant les filtres s'ouvre.
2. Sélectionne dans **"Filtres pour"** le compte de messagerie pour lequel tu souhaites exécuter le filtre
3. Crée un nouveau filtre en cliquant sur **"Nouveau"** : une fenêtre pour créer le filtre s'ouvre.
4. Nomme le filtre avec un nom explicite pour t'y retrouver, par exemple : "Partenariat"
5. Tu peux choisir précisément les conditions à appliquer :
      - Sur quel partie du message la condition s'applique-t-elle ?
      - Ast-ce que le filtre s'applique si TOUTES les conditions sont respectées ? une seule de toutes ces conditions ?
      - Quelle action effectuer quand les conditions sont respectées ?

    !!! example "Exemple"

        **Quand** une de ces conditions est respectée :

          - le sujet du message contient **\[partenaire1\]**
          - le sujet du message contient **\[partenaire2\]**

        **Alors** déplacer le message dans le dossier **Partenaires**

6. Valide la création du filtre. Il s'ajoute à la liste des filtres déjà créés.

7. Tu peux exécuter directement le filtre sur les messages actuellement présents dans ta boîte de réception en cliquant sur **"Exécuter"**

Les prochains messages reçus passeront dans les filtres à l'avenir !

!!! example "Exemple"

    Les prochains messages contenant **\[partenaire1\]** ou  **\[partenaire2\]** dans le sujet seront automatiquement déplacés dans le dossier **"Partenaires"**.

### Envoyer un message d'absence en réponse de mail

Afin d'envoyer un mail de réponse d'absence, tu peux le faire à partir du [webmail Grésille](https://webmail.gresille.org : `Paramètres` > `Filtres` > `Créer`.

### Programmer un envoi de mail pour plus tard

Pour envoyer un message programmé, il existe l'extension [Send Later](https://addons.thunderbird.net/fr/thunderbird/addon/send-later-3/) sur Thunderbird. Habile Bill \! 😉

1. Installer l'extension [Send Later](https://addons.thunderbird.net/fr/thunderbird/addon/send-later-3/)
2. Écrire son message
3. Cliquer sur "Envoyer plus tard" en haut à droite (ou "Send later")
4. Configurer le jour et l'heure de l'envoi
5. Cliquer sur le bouton suivant (ou utiliser les options préconfigurées)
6. ✨ Attendre que la magie opère ✨
7. Ne pas hésiter à tester en s'envoyant des mails à soi-même

!!! warning "Note importante"

    Le message partira de ton ordinateur, il faut donc que ton PC soit allumé et Thunderbird ouvert pour que l'envoi programmé fonctionne.

### Envoyer une série de mail avec des variables

L'extension [Mail Merge](https://addons.thunderbird.net/fr/thunderbird/addon/mail-merge/) permet de réaliser des envois en utilisant des variables.

Cela permet de personnaliser les messages à partir d'informations contenues dans un tableau. Cela est très utile pour que les destinataires se sentent davantage concerné⋅es, et pour envoyer des informations particulières selon les personnes. L'extension permet aussi de répartir les envois dans le temps pour éviter d'être considéré comme spam.

En résumé :

1. [Installer l'extension](#1-installer-lextension)
2. [Créer la base de données](#2-creer-la-base-de-donnees)
3. [Créer le message](#3-creer-le-message)
4. [Envoyer le message](#4-envoyer-le-message)

---

En détail :

#### 1. Installer l'extension

1. Menu **Outils** \> **Modules complémentaires et thèmes**
2. Dans la barre de recherche en haut, chercher "Mail merge"
3. Ajouter l'extension à Thunderbird

#### 2. Créer la base de données

!!! info "Base de données CSV"

    Il s'agit d'un fichier au format CSV qui contient les informations à personnaliser dans le message.

    - chaque ligne correspond à un message que l'on envoie
    - chaque colonne correspond à une information qui sera incluse dans le message
    - la première ligne contient les variables

1. Créer un fichier dans un logiciel de tableur (LibreOffice Calc par exemple)

2. Entrer les informations des destinataires sur lesquelles tu souhaites faire un envoi personnalisé

    - ⚠️ Dans ce fichier, **il faut absolument une colonne qui contient l'adresse mail des destinataires**
    - L'en-tête des colonnes correspond aux **variables** qui seront utilisées plus tard dans le message.
        - Une variable est placée entre accolades
        - Une variable ne contient pas d'espace
        - Une variable ne contient pas de caractères spéciaux

    !!! example "Exemple de fichier"

        ![Mail Merge : exemple de fichier](./media/thunderbird_mail-merge-example-file.png){ loading=lazy : style="width:600px"}

3. Enregistrer le fichier au format **.csv**

    !!! tip "Paramètres lors de l'enregistrement"

        Si ton logiciel demande des précisions lors de l'enregistrement, voici une configuration qui fonctionne bien :

        - Jeu de caractères : **UTF-8**
        - Séparateur de champ : **,** (virgule)
        - Séparateur de texte : **"** (guillemets)

    !!! info "À quoi ça ressemble"

        Si tu ouvres ton fichier avec un éditeur de texte tout simple (Notepad par exemple), il devrait ressembler à une suite de lignes où les données sont séparées par des virgules (et encadrées par des guillemets si le contenu contient des virgules).

#### 3. Créer le message

Dans Thunderbird, rédige ton message en intégrant les variables de la base de données créée précédemment :

1. Dans le champ "Pour", inclus la variable correspondant à l'adresse de messagerie, puis appuie sur la touche "Tabulation" : dans l'exemple ci-dessous, c'est la variable **{{Email}}**
2. Dans le champ "Sujet", tu peux inclure des variables si tu le souhaites
3. Dans le corps du message, tu peux inclure des variables également

!!! example "Exemple de message"

    ![Mail Merge : exemple de message](./media/thunderbird_mail-merge-example-message.png){ loading=lazy : style="width:500px"}

#### 4. Envoyer le message

!!! warning "Faire des tests avant d'envoyer"

    Il est fortement recommandé de faire des tests avant d'envoyer vraiment un message \!<br/>
    L'extension est délicate et on arrive rarement à bien la configurer du premier coup \!

1. Dans la fenêtre de rédaction du message, clique sur "Fichier" puis "Mail Merge" : la fenêtre de configuration de l'envoi s'ouvre

2. Voici les informations à entrer :
    - Source : **CSV**
    - Mode de livraison : **Envoyer maintenant**
    - Fichier : clique sur "Parcourir" pour choisir ton fichier **.csv** créé plus tôt
    - Jeu de caractères : **UTF-8**
    - Séparateur de champ : **,** (virgule)
    - Séparateur de texte : **"** (guillemets)

    ![Mail merge : fenêtre d'envoi](./media/thunderbird_mail-merge-send.png){ loading=lazy : style="width:500px"}

3. Au niveau de "Fichier" clique sur le bouton "Aperçu" (à côté du bouton "Parcourir") pour voir si les données sont prises en compte correctement

    ![Mail merge : aperçu des données](./media/thunderbird_mail-merge-preview-data.png){ loading=lazy : style="width:500px"}

4. Puis ajoute les données d'envoi en lot :

    - **Pause** : c'est le nombre de secondes d'attente entre deux envois. Il peut être intéressant en cas de nombreux envois, de découper par lots ou bien mettre une temporisation pour ne pas être considéré comme du SPAM. Nous recommandons d'entrer une valeur de quelques secondes.
    - **Démarrer** : c'est le numéro de la ligne à laquelle tu souhaites démarrer l'envoi, par exemple pour ne pas prendre le début de la base de données
    - **Arrêter** :  c'est le numéro de la ligne à laquelle tu arrêter l'envoi, par exemple pour ne pas prendre la fin de la base de données

    !!! tip "Envoi à toute la base de données"

        Pour envoyer à toute la base de données, ne rien entrer dans "Démarrer" et "Arrêter".

5. Clique sur "Aperçu" en bas pour voir à quoi ressemblent les messages quand les variables sont remplacées par les données

    ![Mail merge : aperçu d'un mail](./media/thunderbird_mail-merge-preview-mail.png){ loading=lazy : style="width:500px"}

6. Cliquer sur "OK"

    !!! info "Un message d'alerte s'affiche"

        Ce message d'alerte recommande d'utiliser "Envoyer plus tard". Tu peux cliquer sur "Envoyer" à tes risques et périls \!

7. L'envoi se fait :

    ![Mail merge : envoi des mails en cours](./media/thunderbird_mail-merge-sending.png){ loading=lazy : style="width:500px"}

8. Les destinataires reçoivent un message personnalisé \! 🎉

    ![Mail merge : mail envoyé](./media/thunderbird_mail-merge-sent.png){ loading=lazy : style="width:500px"}

---

Une question sur ce tuto ? Une remarque ? Fais signe à [informatique@alternatiba.eu](mailto:informatique@alternatiba.eu) si besoin ! On est là pour t'aider ! 🤓
