---
title: Créer une adresse e-mail
author: Alternatiba / ANV-COP21
date: Janvier 2025
theme: beige
---

!!! warning "Changement de serveur mail"

    **Depuis le 5 mars 2025, nous avons migré nos mails depuis notre ancien hébergeur OVH vers un hébergeur associatif (Grésille). La configuration des mails doit être mise à jour, et voici l'URL du nouveau webmail à utiliser : [https://webmail.gresille.org](https://webmail.gresille.org). Tu trouveras toutes les infos dans ces tutos mis à jour.**

## Contexte

Il est possible de créer des **adresses e-mail individuelles** de la forme [prenom.nom@alternatiba.eu](mailto:prenom.nom@alternatiba.eu) ou [prenom.nom@anv-cop21.org](mailto:prenom.nom@anv-cop21.org) afin de représenter le mouvement lors de nos échanges en ligne.

Ces adresses individuelles sont réservées aux personnes permanentes (de l’EGQ), aux personnes référentes de commissions au niveau national, ainsi qu’aux porte-parole du mouvement. Nous ne pouvons pas fournir d’adresse individuelle pour une utilisation locale.

En revanche, nous pouvons fournir une adresse pour le groupe du type [ville@alternatiba.eu](mailto:ville@alternatiba.eu). Il est également tout à fait possible de créer des adresses génériques telles que [ville.finance@anv-cop21.org](mailto:ville.finance@anv-cop21.org).

!!! warning "Groupes locaux"

    Quand les **groupes locaux** Alternatiba et ANV-COP21 demandent la création d’une adresse mail, il faut mettre Léa C. en copie ([lea.chancelier@alternatiba.eu](mailto:lea.chancelier@alternatiba.eu)). Cela lui permet de mettre à jour les infos sur ce groupe et de garder une vision sur l'activité des groupes locaux.

## Pré-requis

Ce guide est adressé aux personnes gestionnaires des boîtes et listes mail.

Voir [Gérer une liste mail avec SYMPA](./listes.md).

**Pour demander la création d’une adresse e-mail**, envoyer un message à [listmaster@alternatiba.eu](mailto:listmaster@alternatiba.eu).

## Procédure

1. Se connecter à [l’interface Grésille](https://compte.gresille.org) avec le compte administrateur.

2. Dans le menu, choisir `Créer une boîte de messagerie`.

    ![Connexion à Epeire](./media/epeire-connexion.png){ loading=lazy : style="width:300px"}

3. Configurer la boîte :

    ![Epeire](./media/epeire-creation.png){ loading=lazy : style="width:600px"}

4. Valider en cliquant sur `OK`.

5. Il est ensuite possible de configurer d'autres paramètres en vous connectant à [l’interface Grésille](https://compte.gresille.org) avec les identifiants de la boîte créée.

6. Envoyer à la personne ses identifiants de connexion **par deux canaux différents** :

    - L’adresse email par un canal (adresse mail perso, message Mattermost)

        ??? example "Exemple de message à envoyer à une personne à qui on vient de créer une adresse"

            >    **Objet : Bienvenue XXX sur ton adresse mail Alternatiba \!**
            >
            >    Salut,
            >
            >    Je viens de créer ton adresse individuelle [XXX@alternatiba.eu](mailto:XXX@alternatiba.eu) \!
            >
            >    Note bien qu’elle n’est à utiliser que dans le cadre de tes missions au nom du mouvement, pas pour des besoins de groupe local et encore moins pour une utilisation personnelle.
            >
            >    Tu peux t’y connecter depuis un client mail de ton choix (on conseille l’utilisation de Thunderbird) ou depuis l’interface web [https://webmail.gresille.org](https://webmail.gresille.org).
            >
            >    - Ton identifiant est ton adresse e-mail : [XXX@alternatiba.eu](mailto:XXX@alternatiba.eu)
            >    - J’ai inventé un mot de passe, que je t’envoie par Signal/SMS
            >
            >    Enfin, je t’invite à parcourir le tutoriel ["Configurer sa boite mail"](https://tuto.alternatiba.eu/mail/config/), notamment si tu veux utiliser un logiciel de client mail. Nous recommandons le logiciel Thunderbird (voir le tutoriel ["Configurer Thunderbird"](https://tuto.alternatiba.eu/mail/thunderbird/)) \!
            >
            >    N’hésite pas à contacter l'adresse [listmaster@alternatiba.eu](mailto:listmaster@alternatiba.eu) si tu as des questions \!
            >
            >    Bonne journée,
            >
            >    *Signature*

        ??? example "Exemple de message à envoyer à un groupe à qui on vient de créer une adresse"

            >    **Objet : Création de l'adresse mail  Xnom du groupeX**
            >
            >    Salut,
            >
            >    Je viens de créer votre adresse [*XXX@alternatiba.eu*](mailto:XXX@alternatiba.eu) \!
            >
            >    Vous pouvez vous y connecter depuis un client mail de votre choix ou depuis l’interface web [https://webmail.gresille.org](https://webmail.gresille.org).
            >
            >    - L'identifiant est l'adresse e-mail [*XXX@alternatiba.eu*](mailto:XXX@alternatiba.eu)
            >    - J’ai inventé un mot de passe, que je vous envoie par Signal/SMS
            >
            >    Enfin, je vous invite à parcourir le tutoriel ["Configurer sa boite mail"](https://tuto.alternatiba.eu/mail/config/), notamment si vous voulez utiliser un logiciel de client mail. Nous recommandons le logiciel Thunderbird (voir le tutoriel ["Configurer Thunderbird"](https://tuto.alternatiba.eu/mail/thunderbird/)) \!
            >
            >    N’hésitez pas à contacter l'adresse [listmaster@alternatiba.eu](mailto:listmaster@alternatiba.eu) si vous avez des questions \!
            >
            >    Bonne journée,
            >
            >    *Signature*

    - Le mot de passe par un autre canal

        !!! info "Envoi de mots de passe"

            Si possible, utiliser un moyen de communication chiffré pour envoyer le mot de passe (Signal, protonmail to protonmail, Mattermost en activant E2E...). À défaut, envoyer le mot de passe par SMS.

            Supprimer le message contenant le mot de passe une fois que celui-ci a été correctement reçu.

7. Répertorier l’adresse nouvellement créée dans la [Liste des boites mails et listes mails](https://docs.google.com/spreadsheets/d/1lTshug9WV7KyOPgt0leuKwl-j0JfN5CsK-I0SkXRC9E/)

---

Une question sur ce tuto ? Une remarque ? Fais signe à [informatique@alternatiba.eu](mailto:informatique@alternatiba.eu) si besoin ! On est là pour t'aider ! 🤓
