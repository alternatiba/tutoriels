---
title: Configurer et utiliser sa boîte mail Grésille
author: Alternatiba / ANV-COP21
date: Mars 2025
theme: beige
---

!!! warning "Changement de serveur mail"

    **Depuis le 5 mars 2025, nous avons migré nos mails depuis notre ancien hébergeur OVH vers un hébergeur associatif (Grésille). La configuration des mails doit être mise à jour, et voici l'URL du nouveau webmail à utiliser : [https://webmail.gresille.org](https://webmail.gresille.org). Tu trouveras toutes les infos dans ces tutos mis à jour.**

## 📙 Contexte

Ce tutoriel est dédié aux personnes qui ont une adresse mail **@alternatiba.eu** ou **@anv-cop21.org** :

- adresse personnelle de type [raymond.deubaze@alternatiba.eu](mailto:raymond.deubaze@alternatiba.eu)
- adresse de groupe local de type [dunkerque@alternatiba.eu](mailto:dunkerque@alternatiba.eu)
- adresse de commission / GT de type [communication@alternatiba.eu](mailto:communication@alternatiba.eu)

Si tu n'as pas d'adresse @alternatiba.eu, tu peux faire la demande à [listmaster@alternatiba.eu](mailto:listmaster@alternatiba.eu).

!!! warning "Rappel d'usage"

    Ton adresse est dédiée à ta mission au sein d'Alternatiba / ANV-COP21. Il ne faut pas utiliser cette adresse pour un usage personnel.

## ✉️ Comment accéder à ses mails ?

Il y a plein de moyens différents d'accéder à ses mails !

En résumé :

1. **Avec un logiciel de messagerie installé sur son ordinateur** (Thunderbird, Outlook, Mail pour Mac...)<br/>
→ ✅ la solution que l'on recommande !
2. **Avec un navigateur web avec l'application mail par défaut** (Roundcube)<br/>
→ ✅ facile d'accès (il suffit d'avoir un navigateur web)
3. **Depuis une autre application mail en ligne** (Gmail, Microsoft live...)<br/>
→ ❌ on déconseille : les données risquent d'être stockées et utilisées par les GAFAM
4. **Depuis son smartphone** (application Mail, application Gmail...)<br/>
→ ❌ on déconseille car c'est contraire au principe de déconnexion numérique (toujours dans la poche)
5. **Avec une redirection vers sa boîte mail perso**<br/>
→ ❌ on déconseille car c'est contraire au principe de déconnexion numérique (mélange perso / pro)

En détail :

### 1. Avec un logiciel de messagerie

Nous recommandons l'utilisation de la boîte mail avec un logiciel de messagerie. Notre favori est **Thunderbird** ❤️ : c'est un logiciel libre qui dispose de nombreuses fonctionnalités, personnalisable avec des extensions et permettant de gérer plusieurs boîtes mail.

Le principe est le même avec d'autres logiciels de messagerie (Microsoft Outlook, Mail pour MacOS, Courrier pour Windows 10, etc.). N'hésite pas à demander de l'aide pour un logiciel en particulier si besoin à [listmaster@alternatiba.eu](mailto:listmaster@alternatiba.eu).

!!! tip "Plus de détails"

    **Voir le tuto [Configurer Thunderbird](./thunderbird.md)** !


### 2. Avec un navigateur Internet (webmail Roundcube)

Facile ! Il suffit d'utiliser un navigateur Internet et d'aller sur [https://webmail.gresille.org](https://webmail.gresille.org) (à ajouter à tes favoris !).

Il suffit de te connecter en entrant ton adresse mail et ton mot de passe :

![Écran de connection à OVH](./media/roundcube-connexion.png){ loading=lazy : style="width:200px"}

Tu arrives alors sur la ~~magnifique~~ interface du logiciel Roundcube et tu peux accéder à tes mails :

![Écran de connection à OVH](./media/roundcube-interface.png){ loading=lazy : style="width:600px"}

!!! info "Plus de détails"

    Tu peux retrouver des astuces de configuration de Roundcube dans [ce tuto détaillé](./roundcube.md).

### 3. Depuis un client mail en ligne

!!! warning "Recommandation"

    Nous déconseillons cette méthode car tous les messages envoyés sur un client mail externe transitent par des serveurs externes et la plupart des clients sont gérés par des GAFAM qui peuvent donc avoir accès au contenu des messages. **Envoyer et recevoir des mails par ces clients détenus par des GAFAM, c'est accepter d'être surveillé·e par eux.** Nous recommandons d'[utiliser un logiciel de messagerie](#1-avec-un-logiciel-de-messagerie).

Il est possible d'accéder aux mails d'une adresse @alternatiba.eu à partir d'un autre client mail en ligne (Gmail, live.com, webmail Free, boîte mail de l'université, etc.). Voici [le tutoriel d'OVH pour configurer son adresse sur Gmail](https://docs.ovh.com/fr/emails/mail-mutualise-guide-configuration-dun-e-mail-mutualise-ovh-sur-linterface-de-gmail/).

La configuration dépend de chaque client mail, n'hésite pas à demander de l'aide pour une application en particulier si besoin à [listmaster@alternatiba.eu](mailto:listmaster@alternatiba.eu).

### 4. Depuis son smartphone

!!! warning "Recommandation"

    Nous déconseillons cette méthode car les notifications ne permettent pas le [droit à la déconnexion](https://fr.wikipedia.org/wiki/Droit_%C3%A0_la_d%C3%A9connexion). Nous recommandons d'[utiliser un logiciel de messagerie](#1-avec-un-logiciel-de-messagerie).

Voici néanmoins les tutoriels proposés par OVH :

- [Configurer une adresse e-mail sur Android via l'application Gmail](https://help.ovhcloud.com/csm/fr-mx-plan-android-gmail-configuration?id=kb_article_view&sysparm_article=KB0052054)
- [Configurer une adresse e-mail sur un iPhone ou un iPad via l'application Mail](https://help.ovhcloud.com/csm/fr-mx-plan-email-iphone-configuration?id=kb_article_view&sysparm_article=KB0052073)

N'hésite pas à demander de l'aide pour un téléphone ou une application en particulier si besoin à [listmaster@alternatiba.eu](mailto:listemaster@alternatiba.eu).

### 5. Avec une redirection vers sa boîte mail perso

!!! warning "Recommandation"

    Nous déconseillons cette méthode car cela est contraire au [droit à la déconnexion](https://fr.wikipedia.org/wiki/Droit_%C3%A0_la_d%C3%A9connexion). Nous recommandons d'[utiliser un logiciel de messagerie](#1-avec-un-logiciel-de-messagerie).

Si tu le souhaites, tu peux recevoir tous les messages @alternatiba.eu sur ton adresse mail personnelle et y répondre en utilisant ton adresse @alternatiba.eu. **Mais attention !** Cela risque de créer une frontière poreuse entre le "perso" et le "pro".

Pour cela, tu peux configurer une redirection en suivant [ce tuto](#mettre-en-place-une-redirection).

## 😉 FAQ et astuces

### Modifier mon mot de passe

Pour modifier ton mot de passe :

1. RDV sur l'interface d'administration de ta boîte mail [https://compte.gresille.org](https://compte.gresille.org)

2. Connecte-toi en entrant ton adresse mail complète et ton mot de passe

3. Choisis le menu `Changer son mot de passe` et modifie ton mot de passe

### J'ai oublié mon mot de passe

Il n'existe pas de fonctionnalité "Mot de passe oublié". Si tu as oublié ton mot de passe, il faut demander à l'équipe informatique... Donc tu peux faire un petit mail à [listmaster@alternatiba.eu](mailto:listmaster@alternatiba.eu) !

### Activer une réponse automatique pour les vacances

Tu pars en vacances quelques jours ? Il peut être utile d'indiquer à tes correspondant·es qui t'écriraient par mail que tu ne leur répondras pas avant une certaine date. C'est possible avec les réponses automatiques.

!!! info "Tuto à venir"

    On n'a pas encore rédigé ce tuto. On arrive bientôt. Si tu as besoin d'aide, tu peux écrire à [listmaster@alternatiba.eu](mailto:listmaster@alternatiba.eu).

### Ajouter de la mise en forme à mes messages

Peut-être que tu as essayé de rédiger un message en utilisant Roundcube et que tu ne parviens pas à mettre des mots en gras, à ajouter des liens etc. Tu es certainement en édition "Texte en clair". Jette un oeil en haut à gauche de l'écran !

![Type d'éditeur : HTML ou texte en clair](./media/roundcube_html.png)

### Envoyer correctement un mail à une liste

Pour la plupart des listes mail Alternatiba / ANV-COP21, il faut être abonné·e à la liste pour pouvoir envoyer un message dessus. Pour vérifier à quelles listes tu es abonné⋅e en te connectant sur [https://listes.alternatiba.eu](https://listes.alternatiba.eu) ou [https://listes.anv-cop21.org](https://listes.anv-cop21.org).

!!! tip "Quelques recommandations"

    - Attention à bien **relire ton message** car il est envoyé instantanément à de nombreuses personnes
    - **Inutile d'ajouter les crochets** en début d'objet, ceux-ci sont ajoutés automatiquement par le logiciel de listes (par exemple : [coordination])
    - Il faut envoyer le mail dans le **champ "À"** et pas dans les champs "CC" ou "CCi"

### Ajouter une signature à mes messages

Voir le tutoriel dédié [Mettre en place une signature email](./signature.md).

### Mettre en place une redirection

Pour mettre en place une redirection :

1. RDV sur l'interface d'administration de ta boîte mail [https://compte.gresille.org](https://compte.gresille.org)

2. Connecte-toi en entrant ton adresse mail complète et ton mot de passe

3. Clique sur le menu `Configurer son courrier`

4. Coche `Redirection activée`

5. Coche `Conserver une copie des courriers sur le serveur` si tu souhaites que tes messages restent sur la boîte

6. Dans `Adresses de redirection`, entre les adresses de redirection avec une virgule entre chaque adresse. Exemple : `adresse1@aaa.com , adresse2@bbb.com , adresse3@ccc.com `

7. N'oublie pas de cliquer sur `Enregistrer`

    ![Mise en place d'une redirection'](./media/epeire-redirection.png){ loading=lazy : style="width:600px"}

### Mes mails ne s'envoient pas

Si tu rencontres ce souci sur Thunderbird, peut-être que tu trouveras une solution dans [le tuto Thunderbird](./thunderbird.md).

### Mes brouillons et messages envoyés ne s'enregistrent pas

Si tu rencontres ce souci sur Thunderbird, peut-être que tu trouveras une solution dans [le tuto Thunderbird](./thunderbird.md).

### Erreur SMTP 535 : échec d'authentification

Lorsque ce problème se produit, la solution consiste à [réinitialiser son mot de passe](#modifier-mon-mot-de-passe).

### Extensions et astuces Thunderbird

L'avantage de Thunderbird est qu'il est utilisé par beaucoup de personnes et du coup tout plein d'extensions gratuites et pratiques pour faciliter la vie ont été créées. Voici une petite liste :

- [Send Later](https://addons.thunderbird.net/en-us/thunderbird/addon/send-later-3/) : Permet d'envoyer de programmer l'envoie des mails. Par exemple, si vous voulez partager un CR et qu'il est trop tard pour l'envoyer, vous pouvez le programmer pour le lendemain matin.
- [Mail Merge](https://addons.thunderbird.net/fr/thunderbird/addon/mail-merge/) : Permet d'envoyer des mails multiples et personnalisés. Si ça vous intéresse, il est recommandé d'en parler avec la commission informatique car l'outil peut paraître un peu technique et qu'il est facile de faire des erreurs en l'utilisant.

Tu retrouveras les détails sur [le tuto Thunderbird](./thunderbird.md) !

---

Une question sur ce tuto ? Une remarque ? Fais signe à [informatique@alternatiba.eu](mailto:informatique@alternatiba.eu) si besoin ! On est là pour t'aider ! 🤓
