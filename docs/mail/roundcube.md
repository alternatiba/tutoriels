---
title: Utiliser le webmail Roundcube
author: Alternatiba / ANV-COP21
date: Mars 2025
theme: beige
---

!!! warning "Changement de serveur mail"

    **Depuis le 5 mars 2025, nous avons migré nos mails depuis notre ancien hébergeur OVH vers un hébergeur associatif (Grésille). La configuration des mails doit être mise à jour, et voici l'URL du nouveau webmail à utiliser : [https://webmail.gresille.org](https://webmail.gresille.org). Tu trouveras toutes les infos dans ces tutos mis à jour.**

!!! info "Premiers pas"

    Ce tuto est spécifique au logiciel Thunderbird. **Dans un premier temps, aller voir le tuto [Configurer sa boîte mail Grésille](./config.md).**

## Utiliser Roundcube

Facile ! Il suffit d'utiliser un navigateur Internet et d'aller sur [https://webmail.gresille.org](https://webmail.gresille.org).

Il suffit de te connecter en entrant ton adresse mail et ton mot de passe :

![Écran de connection à OVH](./media/roundcube-connexion.png){ loading=lazy : style="width:200px"}

Tu arrives alors sur la ~~magnifique~~ interface du logiciel Roundcube et tu peux accéder à tes mails :

![Écran de connection à OVH](./media/roundcube-interface.png){ loading=lazy : style="width:600px"}


## Astuces Roundcube

!!! info "Tuto à venir"

    On n'a pas encore rédigé ce tuto. On arrive bientôt. Si tu as besoin d'aide, tu peux écrire à [listmaster@alternatiba.eu](mailto:listmaster@alternatiba.eu).


---

Une question sur ce tuto ? Une remarque ? Fais signe à [informatique@alternatiba.eu](mailto:informatique@alternatiba.eu) si besoin ! On est là pour t'aider ! 🤓
