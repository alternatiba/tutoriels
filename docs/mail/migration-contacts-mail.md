---
title: Migrer ses contacts OVH vers Grésille
author: Alternatiba / ANV-COP21
date: Mars 2025
theme: beige
---

!!! warning "Changement de serveur mail"

    **Depuis le 5 mars 2025, nous avons migré nos mails depuis notre ancien hébergeur OVH vers un hébergeur associatif (Grésille). La configuration des mails doit être mise à jour, et voici l'URL du nouveau webmail à utiliser : [https://webmail.gresille.org](https://webmail.gresille.org). Tu trouveras toutes les infos dans ces tutos mis à jour.**

!!! warning "Récupérer vos contacts chez OVH"

    **Depuis le 5 mars 2025, les anciennes boîtes mail ne sont plus accessibles. Vous avez reçu toutes les informations pour vous connecter aux nouvelles adresse Grésille qui contiennent tous vos anciens mails. Si vous avez néanmoins besoin de récupérer vos contacts, faites-nous un message à [listmaster@alternatiba.eu](mailto:listmaster@alternatiba.eu) pour qu'on vous donne accès.**

## 📙 Contexte

Après des années d'hébergement des nos mails chez OVH, **nous migrons en 2025 vers un hébergeur associatif ([Grésille](https://www.gresille.org/))**. Nous nous occupons de la migration des mails et de l'arborescence des dossiers vers notre nouvel hébergeur. Nous ne pouvons pas le gérer nous-mêmes mais **voici un tuto (très simple) pour migrer l'ensemble de ses contacts depuis OVH vers Grésille** facilement !

💡 Rappel : pour vous connecter aux deux serveurs, utilisez les identifiants adéquats et les adresses suivantes :

- Roundcube OVH : <https://mail.ovh.net/roundcube>
- Roundcube Grésille : <https://webmail.gresille.org>

## 📩 Exporter les contacts OVH

- Demander à l'équipe informatique tes identifiants pour accéder à ton ancienne boîte OVH
- Connecte-toi sur ta boîte mail OVH sur [Roundcube](https://mail.ovh.net/roundcube/) avec tes anciens identifiants
- Clique sur `Contacts`
- Clique sur `Exporter les contacts`
- Tu obtiens un fichier au format `*.vcf` :

![Interface propriétaire du logiciel SYMPA](./media/contacts-export.png){ loading=lazy : style="width:600px"}

## 📩 Importer les contacts chez Grésille

- Connecte-toi au [webmail Grésille](https://webmail.gresille.org) avec tes nouveaux identifiants
- Clique sur `Contacts`
- Clique sur `Importer` :

![Interface propriétaire du logiciel SYMPA](./media/contacts-import-2.png){ loading=lazy : style="width:600px"}

- Choisis le fichier `*.vcf` que tu as téléchargé depuis OVH
- Dans le menu déroulant `Importer les affectations de groupe`, choisis `Tous (créer les groupes si nécessaire)` pour importer également les groupes de contacts
- Clique sur `Importer` :

![Interface propriétaire du logiciel SYMPA](./media/contacts-import-1.png){ loading=lazy : style="width:600px"}

👉 **L'ensemble de tes contacts est alors importé chez Grésille** et tu peux t'en servir de la même façon que sur OVH.

---

Une question sur ce tuto ? Une remarque ? Fais signe à [informatique@alternatiba.eu](mailto:informatique@alternatiba.eu) si besoin ! On est là pour t'aider ! 🤓
