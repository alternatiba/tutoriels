---
title: Mettre en place une signature email
author: Alternatiba / ANV-COP21
date: Janvier 2025
theme: beige
---

!!! warning "Changement de serveur mail"

    **Depuis le 5 mars 2025, nous avons migré nos mails depuis notre ancien hébergeur OVH vers un hébergeur associatif (Grésille). La configuration des mails doit être mise à jour, et voici l'URL du nouveau webmail à utiliser : [https://webmail.gresille.org](https://webmail.gresille.org). Tu trouveras toutes les infos dans ces tutos mis à jour.**

## 📙 Contexte

Soyons toutes ambassadrices et tous ambassadeurs du mouvement \!

Une manière toute simple de participer, c’est d’**ajouter une signature email**.

## 🚀 Ajouter une signature à mes mails

### Sur Thunderbird

1. Fais un **clic droit** sur le compte mail à modifier dans le panneau de gauche<br/>
   Puis choisis "Paramètres" dans le menu contextuel

    ![Paramètres Thunderbird](./media/thunderbird_settings.png){ loading=lazy : style="width:400px"}

2. Les paramètres du compte s’affichent :

    - Coche la case devant "Texte de signature \- Utiliser HTML"
    - Dans la zone de texte, copie-colle [le code source de la signature](#le-code-source-de-la-signature-a-copier-coller)

    ![Paramètres Thunderbird : mise en place de la signature](./media/thunderbird_signature-setup.png){ loading=lazy : style="width:400px"}

3. Dans le code source de la signature, **modifie les informations personnelles** :

    - Prénom, nom
    - Mission
    - Adresse mail, téléphone

    ![Exemple de code de signature](./media/thunderbird_signature-code.png){ loading=lazy : style="width:400px"}

4. Et voilà \! Une jolie signature sera insérée dans tes nouveaux messages \!

    ![Exemple d'aperçu de signature](./media/thunderbird_signature-example.png){ loading=lazy : style="width:400px"}

### Sur Roundcube

!!! tip "Conseil"

    Tu utilises Roundcube ? Tu peux [configurer ta boîte mail](./ovh.md) pour utiliser Thunderbird, un logiciel qui a une interface plus jolie et plus pratique \!

1. Clique sur "Paramètres" en haut à droite de l’écran :

    ![Paramètres Roundcube](./media/roundcube_settings.png){ loading=lazy : style="width:400px"}

2. Dans l’onglet "Préférences", choisis "Rédaction de courriels" dans le menu de gauche :

    ![Paramètres Roundcube : rédaction de courriels](./media/roundcube_settings-redaction.png){ loading=lazy : style="width:400px"}

3. Choisis "toujours" pour "Rédiger des courriels HTML" et clique sur "**enregistrer**" en bas :

    ![Paramètres Roundcube : rédiger des courriels HTML](./media/roundcube_settings-html.png){ loading=lazy : style="width:400px"}

4. Va maintenant dans l’onglet "Identités", choisis l’adresse qui t’intéresse à gauche, puis choisis l’onglet "Signature" :

    ![Paramètres Roundcube : identités > signature](./media/roundcube_settings-identity.png){ loading=lazy : style="width:400px"}

5. Tu arrives sur le menu de signature, coche la case "Signature HTML" en bas, puis clique sur les deux chevrons dans le menu :

    ![Paramètres Roundcube : signature HTML](./media/roundcube_settings-signature.png){ loading=lazy : style="width:400px"}

6. Dans le pop-up qui s’ouvre, copie-colle [le code source de la signature](#le-code-source-de-la-signature-a-copier-coller) et modifie les coordonnées personnelles :

    - Prénom, nom
    - Mission
    - Adresse mail, téléphone

    ![Exemple de code de signature](./media/roundcube_signature-code.png){ loading=lazy : style="width:400px"}

7. N’oublie pas de valider et voilà \! Une jolie signature sera insérée dans tes nouveaux messages \!

    ![Exemple d'aperçu de signature](./media/roundcube_signature-example.png){ loading=lazy : style="width:400px"}

### Sur Gmail

!!! tip "Conseil"

    Tu utilises Gmail ? Tu peux [configurer ta boîte mail](./ovh.md) pour utiliser Thunderbird, un logiciel libre qui ne lit pas les messages que tu envoies et que tu reçois \!

1. Gmail n’accepte pas le code source de la signature, il faut copier/coller le "rendu" du code source : pour cela, RDV sur [https://htmledit.squarefree.com](https://htmledit.squarefree.com) (c’est une application qui permet de transformer un **code source** HTML en un **rendu**)

2. Dans la zone du haut, copie-colle [le code source de la signature](#le-code-source-de-la-signature-a-copier-coller) : le rendu apparaît dans la zone du bas

3. Dans le code source de la signature, **modifie les informations personnelles** :

    - Prénom, nom
    - Mission
    - Adresse mail, téléphone

    ![Informations personnelles à modifier](./media/gmail_signature-perso.png){ loading=lazy : style="width:400px"}

4. Sélectionne tout le contenu de la zone du bas (le rendu) et copie le :

    ![Sélection de tout le rendu de la signature](./media/gmail_signature-copy.gif){ loading=lazy : style="width:400px"}

    !!! warning "Sélectionne tout"

        Fais attention à sélectionner **tout** le rendu \! L’image aussi \!

5. Dans Gmail, cliquer sur l’engrenage en haut à droite puis sur "Voir tous les paramètres" :

    ![Paramètres Gmail](./media/gmail_settings.png){ loading=lazy : style="width:200px"}

6. Scroller jusqu’au menu "Signature" puis cliquer sur "Créer" :

    ![Paramètres Gmail : créer une signature](./media/gmail_settings-signature.png){ loading=lazy : style="width:400px"}

7. Nommer la signature :

    ![Paramètres Gmail : nommer une signature](./media/gmail_signature-name.png){ loading=lazy : style="width:400px"}

8. Dans la zone d’édition, coller le contenu que tu as copié plus tôt dans [https://htmledit.squarefree.com](https://htmledit.squarefree.com) :

    ![Paramètres Gmail : coller la signature copiée](./media/gmail_signature-paste.png){ loading=lazy : style="width:400px"}

9. Sous la zone d’édition, sélectionner la signature que tu viens de créer :

    ![Paramètres Gmail : sélectionner la nouvelle signature](./media/gmail_signature-select.png){ loading=lazy : style="width:400px"}

    !!! tip "Signer seulement au premier mail"

        Pour "Dans les réponses/transferts" si on sélectionne "Aucun" alors la signature ne sera insérée qu'au premier mail : voir [cette section](#inserer-la-signature-seulement-au-premier-mail).

10. Scroller tout en bas de la page pour enregistrer les modifications.<br/>Et voilà \! Une jolie signature sera insérée dans tes nouveaux messages \!

    ![Exemple d'aperçu de signature](./media/gmail_signature-example.png){ loading=lazy : style="width:400px"}

## 💡 Conseils et astuces

### Insérer du HTML plutôt qu’une image

- Pour que les URLs soient cliquables
- Pour réduire la taille des envois (un image pèse lourd, un code HTML est léger)

### Insérer la signature seulement au premier mail

Il est possible d’activer l’envoi de la signature pour le premier mail échangé uniquement.

**Pour Roundcube :**

1. Aller dans Paramètres > Préférences > Rédaction de courriels > Option de signature
2. Sélectionner "Ajouter la signature automatique" → "Nouveau courriel seulement"
3. Cocher la case "Lors de la réponse, supprimer la signature originale du courriel"

**Pour Gmail :**

1. Aller dans Paramètres \> Général \> Signature
2. Choisir la signature ajoutée pour "Dans les nouveaux mails"
3. Choisir "Aucune" pour "Dans les réponses/transferts"

## 📝 Le code source de la signature à copier-coller

!!! warning "Personnaliser sa signature"

    Il faut bien penser à modifier les lignes qui commencent par `<!-- MODIFIEZ MOI -->` !

???+ note "Camp Climat 2025"

    ```html
    <div style="font-size: 12px; padding-top: 6px;">

    <div>
    <!-- MODIFIEZ MOI --> <p style="margin: 0; font-weight: bold;">Raymond DEUBAZE</p>
    <!-- MODIFIEZ MOI --> <p style="margin: 0;">Membre de l’organisation du Camp Climat 2025</p>
    <!-- MODIFIEZ MOI --> <p style="margin: 0;">raymond.deubaze@alternatiba.eu | 06 06 06 06 06</p>
    </div>

    <div style="margin-top:10px">
    <p style="margin: 0; font-weight:bold"><span style="color: #00083a">Retrouvez-nous cet été pour le Camp Climat 2025 ! </span>
    <a href="https://campclimat.eu/" target="_blank" rel="nofollow" style="color: #00804D;">https://campclimat.eu</a>
    </p>
    </div>

    <div style="margin-top:10px">
    <a href="https://campclimat.eu/" target="_blank" rel="nofollow" style="text-decoration: none;">
    <img style="margin: 0; width:400px" src="https://campclimat.eu/wp-content/uploads/2025/01/signature-camp-climat.png" alt="Camp Climat 2025">
    </a>
    </div>

    </div>
    ```

    ![Aperçu de signature : Camp Climat 2025](./media/signature_camp-climat-2025.png)

??? note "\[Archivé\] Collecte de fin d’année 2024"

    ```html
    <div style="font-family: Gill Sans, Arial, Helvetica, sans-serif; font-size: 12px; padding-top: 6px;">

    <div>
    <!-- MODIFIEZ MOI --> <p style="margin: 0; font-weight: bold;">Raymond DEUBAZE</p>
    <!-- MODIFIEZ MOI --> <p style="margin: 0;">Membre de l’organisation du Tour Alternatiba 2024</p>
    <!-- MODIFIEZ MOI --> <p style="margin: 0;">raymond.deubaze@alternatiba.eu | 06 06 06 06 06</p>
    </div>

    <div style="margin-top:10px">
    <a href="https://don.alternatiba.eu" target="_blank" rel="nofollow" style="text-decoration: none;">
    <img style="margin: 0; width:400px" src="https://alternatiba.eu/wp-content/uploads/2024/11/signature.png" alt="Votre soutien ici permet de multiplier des victoires partout. Je donne.">
    </a>
    </div>

    </div>
    ```

    ![Aperçu de signature : Collecte de fin d’année 2024](./media/signature_collecte-2024.png)

??? note "\[Archivé\] Tour Alternatiba 2024"

    ```html
    <div style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; padding-top: 6px;">

    <div>
    <!-- MODIFIEZ MOI --> <p style="margin: 0; font-weight: bold;">Raymond DEUBAZE</p>
    <!-- MODIFIEZ MOI --> <p style="margin: 0;">Membre de l’organisation du Tour Alternatiba 2024</p>
    <!-- MODIFIEZ MOI --> <p style="margin: 0;">raymond.deubaze@alternatiba.eu | 06 06 06 06 06</p>
    </div>

    <div style="margin-top:10px">
    <p style="margin: 0; font-weight:bold"><span style="color: #00083a">Participe au Tour Alternatiba 2024 ! </span>
    <a href="https://tour.alternatiba.eu" target="_blank" rel="nofollow" style="color: #00804D;">https://tour.alternatiba.eu</a>
    </p>
    </div>

    <div style="display: table; margin-top:10px">
    <a href="https://tour.alternatiba.eu" target="_blank" rel="nofollow" style="text-decoration: none;">
    <div style="background:#00083a; float:left; height: 130px; padding:10px">
    <div style="text-align: center">
    <img style="margin: 0; width:120px" src="https://alternatiba.eu/wp-content/uploads/2023/11/logo_tour_blanc_365x250.png" alt="Participe au Tour Alternatiba 2024 !">
    </div>
    <div style="margin: 10px 0 20px 0;" class="temp_banner_cta_container">
    <span style="background-color: #00804D; color: #fff; font-weight: 600; font-size: 18px; margin: 10px auto; padding: 10px; border-radius: 15px; text-align: center; text-decoration: none; border: none;" class="temp_banner_cta">Je participe !</span>
    </div>
    </div>
    <div style="float:right;">
    <img style="margin: 0;" src="https://alternatiba.eu/wp-content/uploads/2023/12/2018_GRANDDEPART-204x150-1.jpg" alt="Participe au Tour Alternatiba 2024 !">
    </div>
    </a>

    </div>

    </div>
    ```

    ![Aperçu de signature : Tour Alternatiba 2024](./media/signature_tour-alternatiba-2024.png)

??? note "\[Archivé\] Tour Alternatiba 2024 \- Financement participatif"

    ```html
    <div style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; padding-top: 6px;">

    <div>
    <!-- MODIFIEZ MOI --> <p style="margin: 0; font-weight: bold;">Raymond DEUBAZE</p>
    <!-- MODIFIEZ MOI --> <p style="margin: 0;">Membre de l’organisation du Tour Alternatiba 2024</p>
    <!-- MODIFIEZ MOI --> <p style="margin: 0;">raymond.deubaze@alternatiba.eu | 06 06 06 06 06</p>
    </div>

    <div style="margin-top:10px">
    <p style="margin: 0; font-weight:bold"><span style="color: #00083a">Participe au financement du Tour Alternatiba 2024 ! </span>
    <a href="https://tour.alternatiba.eu/don" target="_blank" rel="nofollow" style="color: #00804D;">https://tour.alternatiba.eu/don</a>
    </p>
    </div>

    <div style="display: table; margin-top:10px">
    <a href="https://tour.alternatiba.eu/don" target="_blank" rel="nofollow" style="text-decoration: none;">
    <div style="background:#00083a; float:left; height: 130px; padding:10px">
    <div style="text-align: center">
    <img style="margin: 0; width:120px" src="https://alternatiba.eu/wp-content/uploads/2023/11/logo_tour_blanc_365x250.png" alt="Participe au financement du Tour Alternatiba !">
    </div>
    <div style="margin: 10px 0 20px 0;" class="temp_banner_cta_container">
    <span style="background-color: #00804D; color: #fff; font-weight: 600; font-size: 18px; margin: 10px auto; padding: 10px; border-radius: 15px; text-align: center; text-decoration: none; border: none;" class="temp_banner_cta">Je donne !</span>
    </div>
    </div>
    <div style="float:right;">
    <img style="margin: 0;" src="https://alternatiba.eu/wp-content/uploads/2023/12/2018_GRANDDEPART-204x150-1.jpg" alt="Participe au financement du Tour Alternatiba !">
    </div>
    </a>

    </div>

    </div>
    ```

    ![Aperçu de signature : Tour Alternatiba 2024 - Financement participatif](./media/signature_fp-tour-alternatiba-2024.png)

??? note "\[Archivé\] Alternatiba \- Collecte"

    ```html
    <div style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; padding-top: 6px;">
    <div>
    <!-- MODIFIEZ MOI --> <p style="margin: 0; font-weight: bold;">Raymond DEUBAZE</p>
    <!-- MODIFIEZ MOI --> <p style="margin: 0;">Coordination de la collecte Alternatiba / ANV-COP21</p>
    <!-- MODIFIEZ MOI --> <p style="margin: 0;">raymond.deubaze@alternatiba.eu | 06 06 06 06 06</p>
    </div>
    <div style="margin-top:10px">
    <p style="margin: 0; font-weight:bold"><span style="color: #512BAF">Je fais un don, je soutiens Alternatiba ! </span>
    <a href="https://don.alternatiba.eu/nous-soutenir?utm_source=email&utm_medium=signature&utm_id=Lena" target="_blank" rel="nofollow" style="color: #057f46;">https://don.alternatiba.eu</a>
    </p>
    <a href="https://don.alternatiba.eu/nous-soutenir?utm_source=email&utm_medium=signature&utm_id=Lena" target="_blank" rel="nofollow" style="text-decoration: none;">
    <img style="margin: 0;" src="http://alternatiba.eu/wp-content/uploads/2021/12/chaine-humaine300x109.png" alt="Je fais un don, je soutiens Alternatiba !">
    </a>
    </div>
    </div>
    ```

    ![Aperçu de signature : Alternatiba - Collecte](./media/signature_alternatiba-collecte.png)

    Variante avec hyperlien créateur de mail sur l’adresse mail \+ hyperlien vers profil Telegram :

    ```html
    <div style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; padding-top: 6px;">
    <div>
    <!-- MODIFIEZ MOI --> <p style="margin: 0; font-weight: bold;">Raymond DEUBAZE</p>
    <!-- MODIFIEZ MOI --> <p style="margin: 0;">Coordination de la collecte Alternatiba / ANV-COP21</p>
    <!-- MODIFIEZ MOI --> <a href="mailto:raymond.deubaze@alternatiba.eu ">raymond.deubaze@alternatiba.eu</a> | Telegram :
    <!-- MODIFIEZ MOI --> <a href="https://t.me/RaymondDebazeAlternatiba">@RaymondDebazeAlternatiba</a>
    </p>
    </div>
    <div style="margin-top:10px">
    <p style="margin: 0; font-weight:bold"><span style="color: #512BAF">Je fais un don, je soutiens Alternatiba ! </span>
    <a href="https://don.alternatiba.eu/nous-soutenir?utm_source=email&utm_medium=signature&utm_id=Lena" target="_blank" rel="nofollow" style="color: #057f46;">https://don.alternatiba.eu</a>
    </p>
    <a href="https://don.alternatiba.eu/nous-soutenir?utm_source=email&utm_medium=signature&utm_id=Lena" target="_blank" rel="nofollow" style="text-decoration: none;">
    <img style="margin: 0;" src="http://alternatiba.eu/wp-content/uploads/2021/12/chaine-humaine300x109.png" alt="Je fais un don, je soutiens Alternatiba !">
    </a>
    </div>
    </div>
    ```

    ![Aperçu de signature : Alternatiba - Collecte (variante)](./media/signature_alternatiba-collecte-telegram.png)

??? note "\[Archivé\] ANV-COP21 \- Collecte"

    ```html
    <div style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; padding-top: 6px;">
    <div>
    <!-- MODIFIEZ MOI --> <p style="margin: 0; font-weight: bold;">Raymond DEUBAZE</p>
    <!-- MODIFIEZ MOI --> <p style="margin: 0;">Coordination de la collecte Alternatiba / ANV-COP21</p>
    <!-- MODIFIEZ MOI --> <p style="margin: 0;">raymond.deubaze@alternatiba.eu | 06 06 06 06 06</p>
    </div>
    <div style="margin-top:10px">
    <p style="margin: 0; font-weight:bold"><span style="color: #690F0F">Pour changer le système, pas le climat : je fais un don ! </span>
    <a href="https://don.anv-cop21.org/nous-soutenir?utm_source=email&utm_medium=signature&utm_id=final" target="_blank" rel="nofollow" style="color: #EEC621;">https://don.anv-cop21.org</a>
    </p>
    <a href="https://don.anv-cop21.org/nous-soutenir?utm_source=email&utm_medium=signature&utm_id=final" target="_blank" rel="nofollow" style="text-decoration: none;">
    <img style="margin: 0;" src="http://anv-cop21.org/wp-content/uploads/2021/12/bandeau-anv500x88.jpg" alt="Pour changer le système, pas le climat : je fais un don !">
    </a>
    </div>
    </div>
    ```

    ![Aperçu de signature : ANV-COP21 - Collecte](./media/signature_anv-collecte.png)

??? note "\[Archivé\] Festival Alternatiba 2022"

    ```html
    <div style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; padding-top: 6px;">
    <div>
    <!-- MODIFIEZ MOI --> <p style="margin: 0; font-weight: bold;">Raymond DEUBAZE</p>
    <!-- MODIFIEZ MOI --> <p style="margin: 0;">Coordination de la communication Alternatiba / ANV-COP21</p>
    <!-- MODIFIEZ MOI --> <p style="margin: 0;">raymond.deubaze@alternatiba.eu | 06 06 06 06 06</p>
    </div>
    <div style="margin-top:10px">
    <p style="margin: 0; font-weight:bold"><span style="color: #512BAF">Cet été, participe au plus grand festival climat ! </span>
    <a href="https://festival.alternatiba.eu/"_blank" rel="nofollow" style="color: #057f46;">https://festival.alternatiba.eu</a>
    </p>
    <a href="https://festival.alternatiba.eu/" target="_blank" rel="nofollow" style="text-decoration: none;">
    <img style="margin: 0;" src="https://alternatiba.eu/wp-content/uploads/2022/03/Sans-titre-4.png" alt="Alternatiba">
    </a>
    </div>
    </div>
    ```

    ![Aperçu de signature : Festival Alternatiba 2022](./media/signature_festival-alternatiba-2022.png)

---

Une question sur ce tuto ? Une remarque ? Fais signe à [informatique@alternatiba.eu](mailto:informatique@alternatiba.eu) si besoin ! On est là pour t'aider ! 🤓
