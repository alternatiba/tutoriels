---
title: Tour Alternatiba 2024
author: Alternatiba / ANV-COP21
date: Mai 2024
theme: beige
icon: material/bike
---

Voici le site du Tour Alternatiba 2024 : <https://tour.alternatiba.eu>

Il permettra de regrouper toutes les informations de la centaine d'étapes en un même endroit.

<div class="grid cards" markdown>

-   ## 🚀 Je me lance !
    - [Se lancer sur le site](./premiers-pas)
    - [Espace d'administration du site web](./administration)
    - [Créer une trace GPX avec Openrouteservice](./trace-gpx)

</div>

## ⚙️ Les chouettes fonctionnalités du site du Tour Alternatiba 2024

Voici une liste non exhaustive des fonctionnalités disponibles :

- 🌍 Afficher la carte de toutes les étapes du Tour Alternatiba 2024
- 👤 Se connecter en tant que référent·e d'étape
- 📍 Afficher et modifier des étapes
- 📅 Afficher et modifier des activités
- ...

!!! tip "Évolution"
    De nombreuses fonctionnalités sont encore en cours de développement et seront ajoutées plus tard.

## 💾 Code source

Si tu souhaites voir le code source : <https://framagit.org/alternatiba/tour-alternatiba-2024>

## 🔥 Alors, on y va ?!

Et tu sais quoi ? On t'accompagne pour te lancer : c'est par [lààà](./premiers-pas) !

---

Tu es perdu⋅e ? Tu peux utiliser le [formulaire de contact](https://tour.alternatiba.eu/contact) ou envoyer un message sur le canal Mattermost [🚴 Tour Alternatiba 2024 - Étapes](https://chat.alternatiba.eu/coordination-du-mouvement/channels/tour-alt-ref-etapes). On est là pour t’aider ! 😄
