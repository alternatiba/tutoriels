---
title: Se lancer sur le site
author: Alternatiba / ANV-COP21
date: Mai 2024
theme: beige
---

## 👀 Utiliser le site en tant que public

Voici ce qu'on peut voir en tant que public (sans se connecter) :

![](media/siteweb-accueil.png){ loading=lazy .md-img-border}

On trouve les différentes parties suivantes :

- 🔴 **[La page d'accueil](https://tour.alternatiba.eu/)** : présentation rapide, mini carte intéractive et cartes d'action (don, rejoindre l'organisation...)

- ⚪ **[L'itinéraire](https://tour.alternatiba.eu/itineraire)**  : carte intéractive où on peut voir toutes les étapes du Tour Alternatiba 2024 et où on peut cliquer sur les points pour avoir plus d'informations

    !!! tip "Filtres et vues"

        Tu peux également utiliser des filtres : à gauche sur ordi, en bas à droite sur mobile.

        Ainsi qu'une vue "liste" : en bas à droite sur ordi, en haut sur mobile.

- 🟡 **[L'appel](https://tour.alternatiba.eu/appel)** : appel du Tour Alternatiba 2024 ainsi que la liste de ses signataires

- 🔵 **[En savoir plus](https://tour.alternatiba.eu/en-savoir-plus)** : présentation du Tour Alternatiba 2024, d'Alternatiba et de nos soutiens

- 🟢 **[Page étape](https://tour.alternatiba.eu/etape/Nantes)** : informations générales d'une étape (date, site, contact, description...) et la liste des activités de cette étape

    !!! tip "Vues des activités"

        Tu peux passer de la vue "liste" à une vue "carte" ou "calendrier" pour visualiser les activités.


- 🟣 **[Page activité](https://tour.alternatiba.eu/etape/Nantes/activite/1)** : informations générales d'une activité (date, lieu, site,  description...)

- 🟤 **[Contact](https://tour.alternatiba.eu/contact)** : envoyer un message à l'équipe du Tour Alternatiba ou faire remonter une information à l'équipe du site

<!-- - 🟠 **[Actualités](https://tour.alternatiba.eu/actualites)** : articles à propos du Tour Alternatiba 2024 et liens vers des médias qui en parlent -->


## ✍️ Se connecter sur le site en tant que référent·e étape

C'est le pôle Tracé qui créé les comptes des référent·es étape, on ne peut pas le créer soi-même.

Si ton compte n'a pas été créé, tu peux envoyer un message sur le canal Mattermost [🚴 Tour Alternatiba 2024 - Étapes](https://chat.alternatiba.eu/coordination-du-mouvement/channels/tour-alt-ref-etapes).

!!! tip "Adresse mail de compte"

    Les comptes peuvent utiliser une adresse mail personnelle ou partagée pour un groupe local

1. Va sur le site <https://tour.alternatiba.eu> et clique sur l'icône 👤 en haut à droite

![](media/siteweb-seconnecter.png){loading=lazy .md-img-border}

2. Entre ton adresse mail et ton mot de passe

    !!! tip "Mot de passe oublié"

        Si tu as oublié ton mot de passe, clique sur le lien "Mot de passe oublié ?" en bas.

3. Maintenant que tu es connecté·e, tu peux accéder à plusieurs espaces en cliquant sur ton nom en haut à droite

      1. Ton compte, où tu peux changer tes informations : prénom, nom, adresse mail et mot de passe

      2. L'espace d'administration (ou espace référent) : voir [le tuto sur l'administration](../administration)

        !!! tip "Conseil d'utilisation"

            Nous te conseillons d'utiliser cet espace sur ordinateur, c'est moins pratique sur mobile

         1. Tu peux **mettre à jour les infos de ton étape** 

         2. Tu peux **mettre à jour les infos des différentes activités ton étape**

      3. Se déconnecter du site

---

Tu es perdu⋅e ? Tu peux utiliser le [formulaire de contact](https://tour.alternatiba.eu/contact) ou envoyer un message sur le canal Mattermost [🚴 Tour Alternatiba 2024 - Étapes](https://chat.alternatiba.eu/coordination-du-mouvement/channels/tour-alt-ref-etapes). On est là pour t’aider ! 😄
