---
title: Trace GPX
author: Alternatiba
date: Mai 2024
theme: beige
---

## Openrouteservice pour créer des traces GPX

**[Openrouteservice](https://maps.openrouteservice.org)** est un site **open source**, gratuit et sans compte qui permet de créer sur ordinateur des **fichiers GPX** représentant un tracé à vélo. Ces fichiers peuvent ensuite être importés dans n'importe quelle **application GPS** sur téléphone pour servir de guidage. On utilisera ces traces dans le Tour Alternatiba 2024 pour définir le **tracé entre deux étapes** et pour les **vélorutions**.

## Création du tracé

1. Zoomez et cliquez sur la carte pour placer l’arrivée
![Création / 1](media/gpx-creation-1.png){ loading=lazy .md-img-border}
2. Cliquez sur l’icône d’itinéraire
![Création / 2](media/gpx-creation-2.png){ loading=lazy .md-img-border}
3. Cliquez sur la case du lieu de départ
![Création / 3](media/gpx-creation-3.png){ loading=lazy .md-img-border}
4. Cliquez sur la carte pour placer le départ
![Création / 4](media/gpx-creation-4.png){ loading=lazy .md-img-border}
5. Un tracé vous est proposé
![Création / 5](media/gpx-creation-5.png){ loading=lazy .md-img-border}
6. Cliquez et tirez au milieu du tracé pour le modifier
![Création / 6](media/gpx-creation-6.png){ loading=lazy .md-img-border}
7. Vous pouvez changer l’ordre des étapes et en supprimer
![Création / 7](media/gpx-creation-7.png){ loading=lazy .md-img-border}

## Informations du tracé

1. Cliquez sur “Informations supplémentaires” pour voir le détail du tracé
![Information / 1](media/gpx-info-1.png){ loading=lazy .md-img-border}
2. Cliquez sur l’icône d’œil  pour voir le détail sur la carte
![Information / 2](media/gpx-info-2.png){ loading=lazy .md-img-border}
3. Descendez et cliquez sur l’icône à côté de l’altitude pour voir le détail
![Information / 3](media/gpx-info-3.png){ loading=lazy .md-img-border}

## Téléchargement du tracé

1. Plusieurs options : imprimer en PDF (1), partager le lien (2), télécharger le fichier GPX (3)
![Téléchargement / 1](media/gpx-telechargement-1.png){ loading=lazy .md-img-border}
1. Nommez le fichier, sélectionnez “Standard GPX” et cliquez sur “Télécharger”
![Téléchargement / 2](media/gpx-telechargement-2.png){ loading=lazy .md-img-border}
1. Et voilà à quoi ressemble le fichier GPX !
![Téléchargement / 3](media/gpx-telechargement-3.png){ loading=lazy .md-img-border}

## Mise à jour du tracé

### Par un fichier

1. Cliquez sur le menu
![Mise à jour avec un fichier / 1](media/gpx-maj-fichier-1.png){ loading=lazy .md-img-border}
2. Cliquez sur le bouton d’import
![Mise à jour avec un fichier / 2](media/gpx-maj-fichier-2.png){ loading=lazy .md-img-border}
3. Sélectionnez le fichier d’un tracé existant
![Mise à jour avec un fichier / 3](media/gpx-maj-fichier-3.png){ loading=lazy .md-img-border}
4. Vous pouvez modifier le tracé **puis l’exporter quand vous aurez fini**
![Mise à jour avec un fichier / 4](media/gpx-maj-fichier-4.png){ loading=lazy .md-img-border}

### Par un lien

1. Sauvegardez le lien du tracé, modifiez le tracé **puis sauvegardez le nouveau lien**
![Mise à jour avec un lien / 1](media/gpx-maj-lien-1.png){ loading=lazy .md-img-border}

## Importation et utilisation de la trace GPX

!!! tip "Application mobile"

    Ici on donne un exemple mais la même logique peut s'appliquer avec beaucoup d'applications GPS

1. Installez l’appli [OsmAnd](https://osmand.net/) sur votre smartphone
       1. Via le Google Play Store : <https://play.google.com/store/apps/details?id=net.osmand&hl=fr&gl=US>
       2. Ou bien via [F-Droid](https://f-droid.org/fr) pour plus de fonctionnalités : <https://f-droid.org/fr/packages/net.osmand.plus>
2. Ouvrez le fichier  GPX avec l’appli OsmAnd et cliquez sur le bouton d’itinéraire
![Importation et utilisation / 1](media/gpx-import-1.png){ loading=lazy .md-img-border}
1. Sélectionnez le point pour rejoindre le départ et le type de véhicule puis lancez-vous !
![Importation et utilisation / 2](media/gpx-import-2.png){ loading=lazy .md-img-border}

---

Tu es perdu⋅e ? Tu peux utiliser le [formulaire de contact](https://tour.alternatiba.eu/contact) ou envoyer un message sur le canal Mattermost [🚴 Tour Alternatiba 2024 - Étapes](https://chat.alternatiba.eu/coordination-du-mouvement/channels/tour-alt-ref-etapes). On est là pour t’aider ! 😄
