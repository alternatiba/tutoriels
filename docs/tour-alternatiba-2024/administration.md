---
title: Espace d'administration
author: Alternatiba
date: Mai 2024
theme: beige
---

!!! tip "Conseil d'utilisation"

    Nous te conseillons d'utiliser cet espace sur ordinateur, c'est moins pratique sur mobile.

## 📍 Administrer ses étapes

Dans l'espace d'administration des étapes, on peut voir la liste des étapes dont on est référent·e :

- **Nom** : clique sur le lien pour voir la page de ton étape
- **Dates de début et de fin** de l'étape
- **Vélorution ? / Village des alternatives ? / Formation ANV ?** : permet de voir si les activités de base ont déjà été créées ou pas
- **Volontaires** : clique sur le bouton 🔎 pour consulter la liste des volontaires : nom, prénom, email, téléphone, date d'inscription et message personnalisé

    !!! info "Qui sont les volontaires ?"

        Si tu as sélectionné l'option "Formulaire interne" (voir section suivante) : ce sont les personnes ayant cliqué sur "Je participe" sur l'étape

- **Ajouter une activité** : clique sur le bouton ➕ pour ajouter une activité à une étape
- **Actions** : clique sur le bouton ✏ pour modifier une étape

![](media/siteweb-adminEtapes.png){ loading=lazy .md-img-border}

Pour **éditer une étape**, tu peux cliquer sur le bouton ✏ soit dans l'espace d'administration, soit en bas à droite sur la page étape.
Voici ce que l'on peut modifier pour une étape :

- **Ville** : seul·es les admins peuvent la changer
- **Email de contact** : affiché de manière publique pour que des personnes puissent contacter l'organisation d'une étape
- **Date de début et de fin** : tu peux modifier l'heure mais seul·es les admins peuvent changer les dates
- **Description courte** : elle s’affichera en vue liste et dans les blocs de survol/clic de la carte
- **Description longue** : elle s'affichera sur la page étape et peut contenir autant de détail qu'on le souhaite avec de la mise en forme (gras, italique, liens, images, vidéos...)
- **Site web** : lien vers un site externe si besoin, par exemple vers le site du groupe local
- **Bouton d'action pour aider à l'organisation d'une étape** : en bas de la page de l'étape se trouve une carte pour les personnes intéressées à rejoindre l'organisation de l'étape, tu peux choisir comment elles peuvent vous contacter :
    - par mail
    - via un formulaire intégré au site
    - via un formulaire externe de votre choix
- **Photo principale** : cette image apparaîtra en haut de la page de l'étape en tant que bannière
- **Autres photos** : d'autres images secondaires, affichées en bas de la page étape dans un carousel (on conseille plutôt d'utiliser les images directement intégrées à la description longue)
- **Partenaires** : logos de partenaires de l'étape (s'il y en a)
- **Référent·es associé·es** : liste des personnes qui sont référent·es de cette étape, seulement modifiable par les admins

![](media/siteweb-editerEtape.png){ loading=lazy .md-img-border}

## 📅 Administrer ses activités

Dans l'espace d'administration des activités, on peut voir la liste des activités des étapes dont on est référent·e :

- **Nom** : clique sur le lien pour voir la page de ton activité
- **Catégorie** : catégorie de l'activité
- **Étape** : étape associée à cette activité
- **Dates de début et de fin**
- **Dates de création et de modification**
- **Participant·es** : clique sur le bouton 🔎 pour consulter la liste des participant·es
    - Tu peux voir leurs informations : nom, prénom, email, téléphone, date d'inscription
    - Tu peux supprimer des participant·es
    - Tu peux exporter la liste dans un fichier tableur
    - Tu peux envoyer un mail à tou·tes les participant·es
- **Actions** : clique sur le bouton ✏ pour modifier une activité ou 🗑 pour supprimer une activité

![](media/siteweb-adminActivites.png){ loading=lazy .md-img-border}

Pour **éditer une activité**, on peut cliquer sur le bouton ✏ soit dans l'espace d'administration, soit en bas à droite sur la page activité.
Voici ce que l'on peut modifier pour une activité :

- **Étape** : seul·es les admins peuvent la changer
- **Nom** : nom de l'activité
- **Catégorie** : catégorie de l'activité
    - 🚲 **Vélorution** (requis)
    - 🏘 **Village des alternatives** (requis)
    - ✊ Transformation
    - 🎤 Concert, Spectacle
    - 🎓 Formation, Atelier, Table-ronde
    - 🖐 **Formation à la désobéissance civile non-violente** (requis)
    - ✨ Conférence du Tour Alternatiba
    - 👀 Autre
- **Adresse** : lieu où se déroule l'activité
- **Date de début et de fin** : on peut modifier la date et l'heure
- **Résumé** : elle s’affichera dans la liste des activités sur la page étape associée
- **Description longue** : elle s'affichera sur la page activité et peut contenir autant de détail qu'on le souhaite avec de la mise en forme (gras, italique, liens, images, vidéos...)
- **Site web** : lien vers un site externe si besoin, par exemple vers un évènement Mobilizon ou Facebook
- **Inscription** : activer ou désactiver la possibilité de s'inscrire à une activité
- **Informations pratiques** : informations diverses, par exemple indications pour accéder au lieu ou matériel nécessaire
- **Photo principale** : cette image apparaîtra en haut de la page de l'activité en tant que bannière
- **Autres photos** : d'autres images secondaires, affichées en bas de la page activité dans un carousel (on conseille plutôt d'utiliser les images directement intégrées à la description longue)

![](media/siteweb-editerActivite.png){ loading=lazy .md-img-border}

---

Tu es perdu⋅e ? Tu peux utiliser le [formulaire de contact](https://tour.alternatiba.eu/contact) ou envoyer un message sur le canal Mattermost [🚴 Tour Alternatiba 2024 - Étapes](https://chat.alternatiba.eu/coordination-du-mouvement/channels/tour-alt-ref-etapes). On est là pour t’aider ! 😄
