---
title: Transiscope
author: Alternatiba / ANV-COP21
date: Août 2023
theme: beige
icon: fontawesome/solid/map-pin
---

## 🤔 C'est quoi Transiscope ?

**[Transiscope](https://transiscope.gogocarto.fr/)** est une cartographie numérique des acteurs et actrices de la transision à travers tout le territoire.

![](media/transiscope-carte.png){ loading=lazy .md-img-border }

À compléter.

Si tu es perdu⋅e, tu peux écrire à [baserow@alternatiba.eu](mailto:baserow@alternatiba.eu). On est là pour t’aider ! :slight_smile:
