---
title: Utiliser l'API Transiscope
author: Alternatiba / ANV-COP21
date: Août 2023
theme: beige
---

En plus de la visualisation cartographique, **Transiscope** est également doté d'une fonctionnalité permettant de récupérer les points de la carte sous un format qui permet d'exploiter facilement les données pour s'en servir. Cette fonctionnalité fait appel à une API et permet de sélectionner différents critères pour définir quels points on souhaite récupérer (zone géographique, catégories, nombre max, etc.).

## Récupérer les données de Transiscope

Pour commencer, il faut **récupérer les données brutes de Transiscope**.

![](media/transiscope-api.png){ loading=lazy .md-img-border }

1. Se rendre sur [https://transiscope.gogocarto.fr/api](https://transiscope.gogocarto.fr/api)

2. Choisir une ou plusieurs catégories, ou ne pas restreindre.

3. Sur la carte, zoomer sur la sone géographique souhaitée puis dessiner un rectangle pour 

4. Enlever le nombre limite de résultats.

5. Cliquer sur `Tester`.

6. Sur page suivante, faire un clic droit puis `Enregistrer sous` pour stocker le fichier sur votre ordinateur.

## Convertir le fichier .json en fichier .csv

Ce format n'est pas facilement exploitable par un·e humain·e. Afin de pouvoir travailler dessus, on a envie de le convertir dans un format qui s'y prête bien pour pouvoir l'afficher sous forme de tableur. On va donc le **convertir au format .csv**.

![](media/transiscope-json-to-csv.png){ loading=lazy .md-img-border }

1. Se rendre sur [https://www.convertcsv.com/json-to-csv.htm](https://www.convertcsv.com/json-to-csv.htm).

2. Dans `Step 1`, choisir `Choose file`.

3. Cliquer sur `Parcourir` pour sélectionner le fichier element.json.

4. Cliquer sur `Convert JSON To CSV`.

5. Cliquer sur `Download Result`.

➡️ Le fichier que vous obtenez peut maintenant s'ouvrir dans n'importe quelle application de tableur (Excel, Libre Office Calc, Google Sheets, etc. et même dans [Baserow](https://tuto.alternatiba.eu/baserow)) et vous pouvez commencer à travailler dessus.

!!! info "Nettoyage des données"
    Le fichier que vous venez de récupérer contient beaucoup de données. Vous pouvez masquer ou supprimer un certain nombre de colonne qui ne sont pas utiles et ne garder que celles pertinentes (nom, adresse, contact, etc.).

---

Si tu es perdu⋅e, tu peux écrire à [informatique@alternatiba.eu](mailto:informatique@alternatiba.eu). On est là pour t’aider ! :slight_smile:
