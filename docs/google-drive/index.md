---
title: Google Drive
author: Alternatiba / ANV-COP21
date: Janvier 2025
theme: beige
icon: simple/googledrive
---

Voici les tutoriels liés à Google Drive :

- [Transfert de propriété des documents Google Drive à Alternatiba / ANV-COP21](./transfert-propriete.md)
- [Gérer les accès par Google Groups](./google-groups.md)