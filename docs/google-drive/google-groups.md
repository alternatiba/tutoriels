---
title: Gérer les accès par Google Groups
author: Alternatiba / ANV-COP21
date: Janvier 2025
theme: beige
---

## 📙 Contexte

Au sein d'Alternatiba / ANV-COP21, les accès aux dossiers et fichiers partagés sur Google Drive sont gérés par le biais de groupes Google (Google Groups).

En effet, au lieu d'attribuer des accès Drive à un compte individuel, on peut attribuer des accès à un groupe Google. Ainsi, tous les membres du groupe bénéficient des accès. Cela facilite la gestion des accès, notamment car il est possible d'avoir plusieurs personnes administratrices d'un groupe Google.

Le principe général :

- des personnes sont ajoutées à un groupe Google
- un dossier ou fichier est partagé à un groupe Google
- les membres du Google Group obtiennent l'accès au dossier / fichier

Les groupes existants ainsi que les dossiers avec un accès particulier sont dans [Liste des Google Groups](https://docs.google.com/spreadsheets/d/1XwuRCxCqWDd4ldEvbe9szNAeABQJ_v-m9dNrOtqUHZg).

## 👥 Les rôles

Pour les groupes Google d'Alternatiba / ANV-COP21, on utilise deux rôles :

* **membre** : les personnes qui ont accès au dossier / fichier
* **gestionnaire** : les personnes qui ont accès au dossier / fichier et qui gèrent les accès

Le compte Google **[1000alternatiba@gmail.com](mailto:1000alternatiba@gmail.com)** a le rôle de **propriétaire**, en cas de besoin. Voir avec l'équipe info.

## ➕ Ajouter ou supprimer des membres d'un groupe Google

1. Connecte-toi à [https://groups.google.com](https://groups.google.com)
2. Clique sur le groupe que tu souhaites modifier
3. Dans la colonne de gauche, clique sur "Membres" : la liste s'affiche

    ![Liste des membres du Google Group](./media/google-group_membres.png){ loading=lazy : style="width:200px"}

### Pour ajouter des membres

1. Clique sur "Ajouter des membres" en haut

    ![Ajouter des membres du Google Group](./media/google-group_ajouter-membres.png){ loading=lazy : style="width:400px"}

2. **⚠️ N'entre pas directement les nouveaux membres** : coche le bouton "Ajouter les membres directement"

    ![Ajouter les membres directement](./media/google-group_ajouter-directement.png){ loading=lazy : style="width:400px"}

3. Entre dans la case "Membres du groupe" les adresses courriel associées aux comptes Google des personnes que tu souhaites ajouter au groupe

    !!! tip "Gestionnaires du groupe"

        Si tu souhaites ajouter d'autres personnes gestionnaires du groupe (qui peuvent gérer les membres), inscris leurs adresses dans la case "Gestionnaires du groupe".

    !!! tip "Adresse du compte Google"

        Souvent, l'adresse du compte Google est une adresse Gmail (mais pas nécessairement) \!

        Pour les membres de la Team, il s'agit des adresses dans la colonne "Email Google Drive (compte Google)" dans [le Tableur Team](https://docs.google.com/spreadsheets/d/1q5-NvCgfL8Wmid6xQwPTX7kfSLLCKG7c0Ckk77Z1P2I/edit#gid=0) (fichier à accès restreint).

4. Tu peux aussi ajouter un message de bienvenue

    !!! example "Exemple de message de bienvenue"

        > Bienvenue dans le Google Group de XXX \!
        >
        > Ce groupe est utilisé pour la gestion des accès aux fichiers et dossiers sur Google Drive. Vous n'avez rien à faire, l'accès vous est automatiquement accordé.
        >
        > Pour toute question liée aux outils numériques, envoyer un message à [informatique@alternatiba.eu](mailto:informatique@alternatiba.eu)

5. Voilà à quoi le formulaire peut ressembler à ce stade :

    ![Formulaire complet d'ajout de membres](./media/google-group_ajouter-membre-complet.png){ loading=lazy : style="width:400px"}

6. Enfin, clique sur le bouton "Ajouter des membres"

7. Dans la liste des membres, modifie les paramètres :

    * Abonnement : Aucun e-mail
    * Publier : Non autorisé

    ![Paramètres Abonnement / Publier](./media/google-group_abonnement-publier.png){ loading=lazy : style="width:400px"}

Et voilà \! 🎉

### Pour retirer des membres

1. Sélectionne tous les membres que tu souhaites retirer du groupe : clique sur le petit bonhomme pour cocher la case

    ![Bonhomme non sélectionné](./media/google-group_bonhomme.png){ loading=lazy : style="width:50px"} → ![Bonhomme sélectionné](./media/google-group_bonhomme-selectionne.png){ loading=lazy : style="width:50px"}

2. Clique sur le petit sens interdit en haut à droite et confirme la suppression

    ![Supprimer le membre](./media/google-group_supprimer-membre.png){ loading=lazy : style="width:300px"}

## 🤗 Partager un fichier / dossier

Pour partager un fichier ou un dossier Google Drive à un groupe de personnes :

1. Ajoute toutes les personnes que tu souhaites à un groupe Google

2. Dans les paramètres de partage du fichier / dossier, ajoute **l'adresse du groupe Google** à la liste des personnes autorisées :

    ![Partager un fichier / dossier](./media/google-group_partage-doc.png){ loading=lazy : style="width:400px"}

    !!! tip "Adresse du Google Group"

        L'adresse du groupe Google est visible dans le menu "À propos" à gauche. Elle se termine par **@googlegroups.com**.

3. Tiens à jour le fichier [Liste des Google Groups](https://docs.google.com/spreadsheets/d/1XwuRCxCqWDd4ldEvbe9szNAeABQJ_v-m9dNrOtqUHZg/edit#gid=880975488) à la deuxième page (liste des accès particuliers)

---

!!! success "Fin du tutoriel"

    Le tutoriel de base est terminé, tu peux poursuivre ta lecture pour plus d'informations avancées sur les Google Groups !

## 📝 Usage au sein du mouvement

!!! info ""

    - La règle par défaut est **« Team et EGQ ont accès à tous les documents du Drive »** afin de limiter le nombre de Google Groups (simplification de la gestion et clarté des accès / transparence)
    - Si besoin d'**un espace avec des droits particuliers, on doit avoir un Google Group associé** (et non pas des accès individuels)
        - *Exemples de cas : dossier ou fichier restreint à une commission ou un GT, accès au Drive d'une commission à des membres ni dans l'EGQ ou la Team, restriction des droits d'édition d'un doc...*
    - Les Google Groups doivent être **créés par la commission informatique** pour que [1000alternatiba@gmail.com](mailto:1000alternatiba@gmail.com) en soit propriétaire
    - Les éléments avec accès restreints doivent comporter **\[accès restreint\]** dans le nom
    - La liste des membres est tenue à jour par les administrateur·rices des groupes

Les dossiers avec un accès particulier sont listés dans [Liste des Google Groups](https://docs.google.com/spreadsheets/d/1XwuRCxCqWDd4ldEvbe9szNAeABQJ_v-m9dNrOtqUHZg/edit#gid=880975488).

## 🔎 Comment ça marche ?

### Ce qui est possible

- Donner des droits de lecteur, de commentateur ou d'éditeur d'un dossier à tout un Google Group (par défaut ça donnera les droits à tous les dossiers et fichiers du dessous)
- On peut enlever à la main les droits d'un dossier à un Google Group, alors même qu'on lui avait donné auparavant les droits d'un dossier parent
- On peut ajouter un autre Google Group pour un sous-dossier
- On peut donner accès "Éditeur" à un Google Group et décocher "Les éditeurs peuvent modifier les autorisations et partager des documents" dans les paramètres avancés (seul le propriétaire peut faire ça)

### Ce qui n'est pas possible

- S'il y a un lien partagé sur un dossier, les personnes ayant accès au dossier auront accès à tous les fichiers/dossiers du dessous (il n'est pas possible de modifier précisément l'accès par lien partagé pour des sous-dossiers)
- Un Google Group ne peut pas être membre d'un autre Google Group
- Un Google Group ne peut pas être propriétaire d'un fichier ou d'un dossier sur Google Drive
- Une personne avec le rôle de gestionnaire ou de propriétaire est également membre du Google Group (un⋅e admin d'un Google Group a nécessairement accès au contenu d'un dossier partagé au groupe)

### Arborescence possible pour la Team

On peut donc imaginer la chose suivante :

```
Team Alternatiba/ANV-COP21 ··················· → 🟢 groupe "Team"
│
└───Animation du réseau ······················ → 🟢 groupe "Team"
│   │
│   └───Suivi du réseau ······················ → 🟢 groupe "Team"
│   │   │
│   │   └───Suivi du réseau ANV ·············· → 🟢 groupe "Team"
│   │       │
│   │       └───Tableau de suivi du réseau ANV → 🔴 groupe "Référent·es ANV"
│   |
│   └───Ressources pour les groupes ·········· → 🟢 groupe "Team" + 🔗 lien partagé
│       │
│       └───Documents de ressources ·········· → 🟢 groupe "Team" + 🔗 lien partagé
│
└───Animation générale ······················· → 🟢 groupe "Team"
│   │
│   └───Binôme d'animation générale ·········· → 🟣 groupe "Binôme anim-gé"
│   
└───Régulation des conflits ·················· → 🟡 groupe "Régulation des conflits"
│   │
│   └───Régulations ·························· → 🟡 groupe "Régulation des conflits"
│   
└───RH ······································· → 🟢 groupe "Team"
    │
    └───Documents internes RH ················ → 🔵 groupe "RH"
```

## ✨ Créer un groupe Google

Pour cela, prends soin de lire les conventions ci-dessous et note bien le nouveau groupe dans [Liste des Google Groups](https://docs.google.com/spreadsheets/d/1XwuRCxCqWDd4ldEvbe9szNAeABQJ_v-m9dNrOtqUHZg/edit#gid=0).

Bien qu'il soit possible de créer un groupe soi-même, **il vaut mieux demander à [informatique@alternatiba.eu](mailto:informatique@alternatiba.eu)** afin de faciliter la gestion et notamment pour inclure le compte Google administrateur en tant que propriétaire du groupe.

### Paramétrage des groupes

Voici le paramétrage pour tous les groupes Google avec en gras les paramètres à modifier :

* L'email du groupe commence toujours par "alternatiba-" (ex : [alternatiba-team@googlegroups.com](mailto:alternatiba-team@googlegroups.com))
* Qui peut voir le groupe → membres
* Qui peut rejoindre le groupe → invité⋅es uniquement
* Afficher les conversations → **propriétaire** (on n'utilise pas la fonctionnalité de liste mail)
* Publier → **propriétaire** (on n'utilise pas la fonctionnalité de liste mail)
* Afficher les membres → membres
* Qui peut gérer les membres → gestionnaires
* Qui peut modifier les rôles personnalisés → **propriétaires**
* Contacter les propriétaires du groupe → **membres**
* Afficher l'adresse e-mail des membres → gestionnaires
* Autoriser les publications par email → **non**
* Autoriser les publications par le web → **non**
* Historique de la conversation → **désactivé**

### Message de bienvenue

Quand une personne est invitée, voici des exemples de messages de bienvenue à envoyer :

> Bienvenue dans le Google Group de XXX \!
>
> Ce groupe est utilisé pour la gestion des accès aux fichiers et dossiers sur Google Drive. Vous n'avez rien à faire, l'accès vous est automatiquement accordé.
> 
> Pour toute question liée aux outils numériques, envoyer un message à [informatique@alternatiba.eu](mailto:informatique@alternatiba.eu)
  
---

> Bienvenue dans le Google Group de XXX \!
> 
> Ce groupe sera utilisé sous peu pour la gestion des accès aux fichiers et dossiers Google Drive. Vous n'avez rien à faire pour le moment.
> 
> Pour toute question liée aux outils numériques, envoyer un message à [informatique@alternatiba.eu](mailto:informatique@alternatiba.eu)

### Ajout des nouveaux groupes dans la liste

Quand un nouveau Google Group est créé, bien penser à **le rajouter dans [Liste des Google Groups](https://docs.google.com/spreadsheets/d/1XwuRCxCqWDd4ldEvbe9szNAeABQJ_v-m9dNrOtqUHZg)** !

---

Une question sur ce tuto ? Une remarque ? Fais signe à [informatique@alternatiba.eu](mailto:informatique@alternatiba.eu) si besoin ! On est là pour t'aider ! 🤓
