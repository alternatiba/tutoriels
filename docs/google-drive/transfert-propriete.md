---
title: Transfert de propriété des documents Google Drive à Alternatiba / ANV-COP21
author: Alternatiba / ANV-COP21
date: Janvier 2025
theme: beige
---

## Contexte

**👉 Objectif** : transférer la propriété de la totalité des éléments dont on est en propritaire sur le Google Drive Team Alternatiba/ANV-COP21 au compte [1000alternatiba@gmail.com](mailto:1000alternatiba@gmail.com)

**⏰ Durée** : entre 3 et 10 minutes

**🤔 Pourquoi transférer la propriété des docs ?**<br/>
Quand une personne crée un document avec son compte Google perso, elle est propriétaire de ce doc. Ainsi, afin de faciliter le travail des personnes qui gèrent les outils numériques du mouvement et pour assurer la pérennité des données (notamment quand des personnes quittent le mouvement), il est important de *transférer la propriété* des docs à Alternatiba / ANV-COP21. Et en plus, ça libère de l’espace dans ton Drive perso \! 😎

## 1. Se connecter

- Va sur [Google Drive](https://drive.google.com) et **connecte-toi avec ton compte Google**

## 2. Faire la recherche

Ensuite, il faut rechercher l**es documents dont tu es propriétaire**.

Pour cela, deux choix : [lien automatique](#lien-automatique) ou [recherche manuelle](#recherche-manuelle).

### Lien automatique

**Clique sur [ce lien](https://drive.google.com/drive/search?q=owner%3Ame+parent%3A1xM237YY-7PUYfgR-YJV68Q3PaIzgrQR3)** qui fait la recherche adéquate automatiquement.
 
Ensuite [rendez-vous directement ici](#3-selectionner-les-docs) une fois que c’est fait \!

### Recherche manuelle

Sinon, tu peux faire la recherche manuellement :

* Clique sur le bouton « **Recherche avancée** » à droite de la barre recherche en haut de la page.

    ![Bouton de recherche avancée](./media/drive_advanced-search.png){ loading=lazy : style="width:700px"}

* Sélectionne :
    * **Type** : tous
    * **Propriétaire** : moi
    * **Emplacement** : TEAM ALTERNATIBA/ANV-COP21 (ou un sous-dossier)

* Clique sur « **Rechercher** »

    ![Filtres de recherche](./media/drive_search-filters.png){ loading=lazy : style="width:600px"}

## 3. Sélectionner les docs

- Une fois la recherche effectuée, **prends un peu de temps** pour vérifier cette liste et assure-toi qu'il n'y a pas d'erreurs (il s'agit bien de documents qui sont dans le Drive d'Alternatiba)

- Descendre tout en bas de la page de manière à ce que la liste se charge entièrement (sinon, seulement le début est affiché et sélectionné)
   
- Sélectionne tous les fichiers dont tu es propriétaire (tu peux faire CTRL \+ A)

!!! warning "Attention"

    * Fais attention à bien sélectionner uniquement les documents dont tu es propriétaire
    * Ignore les « Raccourcis »

!!! tip "Conseils"

    - Maintiens MAJ appuyé pour sélectionner plusieurs docs à la suite
    - Maintiens CTRL appuyé pour sélectionner ou désélectionner les docs un à un

## 4. Transférer la propriété

- Fais **clic droit \> Partager** sur l’un des éléments sélectionnés

!!! warning "Transférer beaucoup de fichiers"

    Google Drive peut parfois être capricieux et ne pas vouloir partager la propriété de certains documents dans des blocs trop gros (dans ce cas, le bouton « Partager » est grisé). Il faut aller sélectionner moins de documents à la fois et faire les transfert par blocs.

- Parcours les adresses mail jusqu’à trouver **le compte Alternatiba / ANV-COP21 (relié à l’adresse [1000alternatiba@gmail.com](mailto:1000alternatiba@gmail.com)**).

- Clique sur **la flèche à droite** du rôle actuel du compte Alternatiba

    ![](./media/drive_share.png){ loading=lazy : style="width:400px"}

- Choisis « **Transférer la propriété** » puis « **Envoyer l’invitation** »

    ![](./media/drive_share-settings.png){ loading=lazy : style="height:300px"} ![](./media/drive_send-invite.png){ loading=lazy : style="height:300px"}

Et voilà, merci \! [1000alternatiba@gmail.com](mailto:1000alternatiba@gmail.com) n’a plus qu’à accepter le transfert de propriété 🙏 🎉

---

Une question sur ce tuto ? Une remarque ? Fais signe à [informatique@alternatiba.eu](mailto:informatique@alternatiba.eu) si besoin ! On est là pour t'aider ! 🤓