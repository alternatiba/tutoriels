---
title: Big Blue Button - utilisateurice
author: Alternatiba / ANV-COP21
date: Janvier 2024
theme: beige
---

## Contexte

Ce tutoriel est à destination des utilisateurices de BigBlueButton.

## Rejoindre une réunion

1. Cliquer sur un lien de réunion (il ressemble à : [https://visio.globenet.org/rooms/aaa-bbb-ccc-ddd/join](https://visio.globenet.org/rooms/aaa-bbb-ccc-ddd/join))

2. Renseigner son prénom ou pseudo (et idéalement la ville entre parenthèses) puis cliquer sur "Rejoindre la réunion"

    ![Rejoindre une réunion : renseigner son nom](./media/bbb_join-name.png){ loading=lazy : style="width:400px"}

3. Cliquer sur "Parler et écouter", choisir les périphériques et cliquer sur "Rejoindre la réunion en audio"<br/>(penser à accepter la demande d'autorisation du navigateur)

    ![Rejoindre une réunion : parler et écouter](./media/bbb_join-audio.png){ loading=lazy : style="width:400px"}

4. Et voilà, on est arrivé !

## Fonctionnalités

Avec BigBlueButton on peut notamment :

- Discuter en audio et/ou vidéo
- Partager son écran, une présentation, une vidéo
- Collaborer sur un tableau blanc
- Discuter à l'écrit
- Prendre des notes partagées
- "Lever" la main (pour demander un tour de parole)
- Créer des sondages
- Enregistrer la réunion
- Créer des sous-salles (pour des temps d'échanges en petits groupes)
- Et bien plus encore !

**Pour plus de détails : [https://ma.formation-logiciel-libre.com/bbb](https://ma.formation-logiciel-libre.com/bbb)**

## Conseils

La discussion et les notes ne sont pas sauvegardées après une réunion, si vous voulez les conserver il faudra les télécharger (bouton en haut à droite du panneau).

---

Une question sur ce tuto ? Une remarque ? Fais signe à [informatique@alternatiba.eu](mailto:informatique@alternatiba.eu) si besoin ! On est là pour t'aider ! 🤓