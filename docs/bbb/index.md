---
title: Big Blue Button
author: Alternatiba / ANV-COP21
date: Janvier 2025
theme: beige
icon: simple/bigbluebutton
---

## C'est quoi ?

"BigBlueButton" (ou BBB) est un système de visioconférence opensource et hébergé par [Globenet](https://www.globenet.org)

Il permet de faire des réunions en audio et/ou vidéo depuis son navigateur web (rien besoin d'installer !)

Voici quelques liens utiles :

- **[Mini tuto Formation Logiciel Libre](https://ma.formation-logiciel-libre.com/bbb)** (concis, clair et avec des images)
- [Wiki du collectif CHATONS](https://wiki.chatons.org/doku.php/services/visio-conference/big_blue_button) (avec plein de ressources)
- [Tutoriels vidéo officiels](https://bigbluebutton.org/tutorials)
- [Manuel par l'Université de Strasbourg](https://documentation.unistra.fr/DNUM/Services_communication/guide_bigbluebutton.pdf) (long mais complet, à consulter seulement si besoin)

**C'est [par ici](./user.md) pour le tutoriel utilisateurice !**

---

Une question sur ce tuto ? Une remarque ? Fais signe à [informatique@alternatiba.eu](mailto:informatique@alternatiba.eu) si besoin ! On est là pour t'aider ! 🤓