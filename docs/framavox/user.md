---
title: Framavox - utilisateurice
author: Alternatiba / ANV-COP21
date: Janvier 2025
theme: beige
---

## 🐣 Les débuts dans Framavox

Pour bien débuter dans Framavox, il faut simplement maîtriser 2 types d'écrans et réaliser un paramétrage pour ne pas recevoir trop de mails.

!!! question "Framavox et Loomio"

    Un peu partout, tu verras parfois "Framavox" et parfois "Loomio". Tu peux considérer que c'est pareil ([Framavox](https://framavox.org) est basé sur le logiciel libre [Loomio](https://www.loomio.com/)).

### 1ère étape : S'inscrire en 3 clics

1. Vous allez **recevoir un e-mail** *"Charles Adrien vous invite à rejoindre "Team Alternatiba" sur loomio"*

2. Cliquez sur le bouton **"Accepter l'invitation"** : un écran d'enregistrement apparaît

3. Choisissez soit de vous **créer un compte** soit d'utiliser un autre service (Facebook, Google...) selon de votre préférence

4. Une fois le compte créé, cliquez sur **"Ask to join group"** pour demander à rejoindre le groupe

5. Dès que la demande est acceptée, vous **recevez un second mail** vous prevenant avec un lien

6. Cliquez dessus et vous arrivez à [l'écran principal](#3eme-etape-lecran-principal)

Avant de le regarder en détail, on va juste passer quelques secondes à [régler les paramètres](#2eme-etape-regler-les-parametres), comme ça c'est fait !

### 2ème étape : Régler les paramètres

1. Aller dans **"Paramètres e-mail"** dans la barre de gauche (il peut être caché dans le menu ☰ en haut à gauche)

2. Choisir les paramètres suivants :

    - **"Résumé quotidien"** pour avoir un mail par jour avec tout ce qui s'est dit
    - **"Mentions"** pour recevoir un mail quand on site notre nom dans une discussion

3. Puis cliquez sur modifier à droite de "Team Alternatiba" pour régler le degré d'activité :

    - **"Activité importante"** pour recevoir un mail quand une discussion ou une prise de décision sur une discussion commence

Et voilà pour le paramétrage ! Pour revenir à l'écran principal, cliquez sur "Team Alternatiba" dans la barre de gauche.

### 3ème étape : L'écran principal

![Interface Framavox / Loomio](./media/framavox_interface.png){ loading=lazy : style="width:800px"}

L'écran ci-dessus est l'écran principal. Il est constitué de 3 parties :

- **La barre latérale gauche**, que l'on peut réduire avec l'icone ☰ tout en haut

- **Le centre**, on y voit toutes les discussions par ordre d'activité récente (la plus récente apparait en haut)

    - Si un vote est en cours, on voit l'état du vote (ici un vote est en cours avec 50% d'abstention et 50% de vote pour)
    - Lorsqu'un sujet est en gras, c'est qu'il y a des modifications depuis notre dernier passage

- **La barre de droite**, avec une succession de petits carrés :
    - Plan cadeau si vous souhaitez faire un don à loomio
    - Les membres, permet d'avoir accès à la liste des membres et éventuellement d'inviter de nouveaux membres
    - Propositions précédentes : reprend les derniers résultats des propositions précédentes

Cet écran sert essentiellement à choisir une discussion et à créer une nouvelle discussion ou groupe (voir la [partie avancée](#partie-avancee)).

### 4ème étape : L'écran de discussion

L'écran ci-dessous est celui qui est représente une discussion :

![Discussion Framavox / Loomio](./media/framavox_discussion.png){ loading=lazy : style="width:800px"}

Il est composé de trois parties :

- **Zone du haut gauche** (ici "Comment utiliser loomio") : c'est la zone de départ, celle dans laquelle on va mettre le texte sur lequel on souhaite discuter ou sur lequel une décision est à prendre

- **Zone gauche** : c'est la zone d'activité, chacun·e peut ajouter un commentaire (dans la case "Say something")

    - On verra dans cette zone toute l'activité, c'est à dire tous les commentaires, les prises de décisions et votes de chacun
    - Ils s'affichent par ordre chronologique, le premier en haut, le dernier en bas
    - Quand on arrive, il suffit de reprendre depuis le dernier endroit lu (une ligne orange s'affiche) et de parcourir cette zone

- **Zone droite** : c'est la zone de prise de décision

    - Lorsqu'une prise de décision est active, on se prononce en cliquant sur les icônes de main puis on explique pourquoi
    - La zone droite reprend tous les votes et explications de chacun pour avoir en un coup d'œil l'état de la prise de décision
    - Si d'autres prises de décisions ont été faites, elles apparaissent en dessous de la prise de décision active

### 5ème étape : En pratique comment ça se passe ?

Si vous avez choisi les paramétrage conseillé, une fois l'inscription faite (étapes 1 et 2), vous recevrez 4 types de mails :

- **Nouvelle discussion** - Lorsque quelqu'un créé une nouvelle discussion, le texte initial est envoyé par mail avec un lien pour accéder directement à la discussion. Libre à chacun ensuite de réagir directement dans la zone d'activité.
- **Nouvelle décision** - Lorsqu'une personne lance une prise de décision sur une discussion, vous recevez un mail de démarrage de la prise de décision avec un lien pour accéder directement à la discussion, le détail de la prise de décision et en particulier la date limite de réponse, et des liens permettant directement de se prononcer.
- **Fermeture dans 24h** - Lorsqu'une décision arrive à son terme, un mail de rappel est envoyé à tout le monde 24h exactement avant la clôture.
- **Hier dans loomio** - Tous les jours au matin, vous avez un mail qui reprend discussion par discussion tout ce qu'il s'est dit. Le mail est assez ergonomique et pour chaque discussion des liens sont créés pour pouvoir répondre directement ou pour voter si un vote est en cours.

!!! success ""

    L'utilisation de base consiste donc simplement à lire ces mails, à réagir si on le souhaite en allant dans la discussion appropriée, en y posant un commentaire et/ou en prenant par à la décision.

## 🚀 Partie avancée

Dans cette partie, on détaille toutes les fonctionalités utiles de loomio.

### La barre de gauche

![Barre de gauche](./media/framavox_barre-gauche.png){ align=left loading=lazy : .w-200 .big }

![Barre de gauche](./media/framavox_barre-gauche.png){ loading=lazy : .w-200 .small }

- **Récent**<br/>
  Permet de voir uniquement les discussions récentes

- **Non lu**<br/>
  Permet de voir uniquement les discussions pour lesquels il y a de nouveaux éléments que je n'ai pas lu

- **Désactivé**<br/>
  Permet de voir les discussions que j'ai désactivé<br/>
  Il est possible de désactiver une discussion en cliquant sur l'icône mute

    ![Désactiver une discussion](./media/framavox_desactiver.png){ loading=lazy : style="width:400px"}


- **Team Alternatiba**<br/>
  Permet d'arriver à la page principale de notre groupe

- **Éditer le profil**<br/>
  Permet de modifier vos paramètres perso et de rajouter votre photo par exemple

- **Paramètres e-mail**<br/>
  Permet de changer les paramètres e-mail (vu précédemment)

<br/>

### Comment créer une discussion

En cliquant via l'écran principal sur le bouton "Démarrer une nouvelle discussion" :

![Bouton pour démarrer une nouvelle discussion](./media/framavox_nouvelle-discussion.png){ loading=lazy : style="width:800px" }

![Formulaire pour créer une nouvelle discussion](./media/framavox_nouvelle-discussion-form.png){ align=left loading=lazy : .w-400 .big }

Apparaît alors un panneau permettant de créer une discussion avec 3 cases principales :

- **Groupe**<br/>Si vous avez plusieurs groupes, choisissez dans quel groupe vous souhaitez que la discussion apparaisse

- **Titre**<br/>Le titre qui apparaîtra sur l'écran principal

- **Contexte**<br/>L'ensemble du texte qui apparaîtra dans la zone haut-gauche de la discussion

![Formulaire pour créer une nouvelle discussion](./media/framavox_nouvelle-discussion-form.png){ loading=lazy : .w-400 .small }

Vous pouvez également joindre un fichier et mettre en forme le contexte. Pour cela, les règles de formatage sont accessibles en cliquant sur "Aide pour le formatage"
Une fois le texte rédigé, cliquez sur "Démarrer une discussion" et le tour est joué \!

### Comment créer une prise de décision

Les prises de décisions sont un élément important qui permet de faire avancer une discussion ou d'acter quelque chose. Il ne faut pas hésitez à en prendre, comme on peut faire des prises de températures en réunion. Sur une même discussion, plusieurs prises de décisions peuvent être faites.

Pour créer une prise de décision, il ne faut pas qu'une prise de décision soit en cours.

Il y a alors la possibilité dans la partie droite de cliquer sur le bouton **"Créer une proposition"**.

![Démarrer une nouvelle décision](./media/framavox_demarrer-proposition.png){ align=left loading=lazy : .w-400 .big }

![Démarrer une nouvelle décision](./media/framavox_demarrer-proposition.png){ loading=lazy : .w-400 .small }

Une nouvelle fenêtre s'ouvre et il faut indiquer :

- **Titre** – L'énoncé exacte de la proposition qui est soumise.

- **Détails** – Si besoins, des informations complémentaires permettant d'aider à chacun à se positionner.

- **Ferme** – Indique le jour et l'heure de fin de proposition. Une fois la date passée, la proposition est close et il n'est plus possible de revenir dessus. En revanche, tant que la proposition est active, il est toujours possible de reculer ou d'avancer la date de fin :

    ![Démarrer une nouvelle décision](./media/framavox_modifier-fin.png){ loading=lazy : style="width:300px" }

Une fois la proposition prête, il suffit de cliquer sur "Créer une proposition".

### Comment créer un groupe ou un sous-groupe

Déjà pourquoi créer des groupes ou des sous-groupe ? Dans notre fonctionnement, il est possible qu'un groupe (ouvert ou fermé) souhaite pouvoir utiliser loomio pour son propre fonctionnement. Pour ne pas encombrer la team, il est plus simple de créer un autre flux de discussion.

S'offrent alors deux choix :

- Créer un autre groupe
- Créer un sous-groupe

Le choix logique est de créer des sous-groupe lorsque le groupe est constitué uniquement de personnes membre de "Team alternatiba". La création d'un sous-groupe à plusieurs avantages :

- Plus facile d'y ajouter des membres du groupe "Team Alternatiba"
- Les discussions apparaissent pour les membres du sous-groupe directement avec les discussions du groupe, ce qui permet de ne suivre qu'un seul écran général plutôt que de naviguer entre plusieurs écrans

La limite arrive lorsqu'on souhaite inviter dans ce sous-groupe d'autres personnes

- Si ce groupe est un groupe ouvert d'alternatiba, cela n'est pas dérangeant. Il reste juste à déterminer notre volonté : accepter ou non que des personnes ne faisant pas parti de la team puissent voir les discussions de la team
- S'il s'agit d'un groupe de travail inter-orga, la question se pose plus car toutes les personnes de la Team peuvent voir les discussions d'un sous-groupe, donc il est préférable de créer un groupe détaché.

De manière générale :

- Les gens de "Team alternatiba" ne voient pas dans leur flux de discussion les discussions des sous-groupes dans lesquels ils ne sont pas.
- Les personnes d'un sous-groupe voient dans leur flux de discussion "Team alternatiba" les discussions des sous-groupes dans lesquels ils sont
- Toute personne de "Team alternatiba" voit les sous groupes existant et peut demander à les rejoindre.
- Lorsqu'on voit un groupe (ou sous-groupe), si celui-ci est secret, on ne voit que le groupe. Si celui-ci est fermé, on peut, en cliquant sur le groupe (ou sous-groupe) voir toutes les discussions. En revanche on ne peut ni participer à la discussion, ni participer à une prise de décision.

#### Créer un sous-groupe

Seul.e.s les coordinateurs et coordinatrices peuvent créer un sous-groupe. Il suffit de définir le nom et la description du sous-groupe ainsi que son statut de confidentialité :

- **Ouvert :** n'importe qui peut trouver ce sous-groupe et demander à le rejoindre. Seuls les membres peuvent voir qui appartient au groupe. Toutes les discussions sont publiques.
- **Fermé :** n'importe qui peut trouver ce sous-groupe et demander à le rejoindre. Seuls les membres peuvent voir qui appartient au groupe. Les discussions peuvent être publiques ou privées.
- **Secret :** seules les personnes invitées peuvent trouver ce sous-groupe, voir qui sont les membres et lire les discussion

Il faut donc se rapprocher des coordinateurs et coordinatrices loomio lorsqu'on souhaite créer un sous-groupe. A priori, tout cela se fera en réunion Team \!

### Comment inviter des personnes ou sous-groupes

![Inviter des personnes](./media/framavox_inviter-personnes.png){ align=left loading=lazy : .w-400 .big }

**Inviter des personnes** :

1. Écran principal
2. Cliquer sur "Inviter des personnes"
3. Puis...
    1. SOIT rentrer les adresses e-mail puis cliquer sur "Fait"
    2. SOIT copier et partager le lien aux personnes voulues (elles recevront un e-mail comme dans [la 1ère étape](#1ere-etape-sinscrire-en-3-clics))

**Inviter des sous-groupes** :

1. Écran principal
2. Cliquer sur "Inviter des personnes" puis sur "Ajouter des membres"
3. Sélectionner les personnes dans la liste de tous les membres de "Team Alternatiba" qui ne sont pas déjà dans le groupe

Et finir en cliquant sur "Fait" !

![Inviter des personnes](./media/framavox_inviter-personnes.png){ loading=lazy : .w-400 .small }

---

Tu es perdu⋅e ? Tu peux écrire à [informatique@alternatiba.eu](mailto:informatique@alternatiba.eu) ou sur le canal [⁉️ Questions et annonces](https://chat.alternatiba.eu/coordination-du-mouvement/channels/town-square). On est là pour t'aider ! :slight_smile:

<style>
.small {
  margin: auto;
  display: block;
}
.w-400 {
  width: 400px;
}
.w-200 {
  width: 200px;
}
/* Show on big screens / hide on small screens */
@media (max-width: 800px) {
  .w-400.big {
    display: none;
  }
}
@media (max-width: 600px) {
  .w-200.big {
    display: none;
  }
}
/* Show on small screens / hide on big screens */
@media (min-width: 800px) {
  .w-400.small {
    display: none;
  }
}
@media (min-width: 600px) {
  .w-200.small {
    display: none;
  }
}
</style>
