---
title: Framavox / Loomio
author: Alternatiba / ANV-COP21
date: Janvier 2025
theme: beige
icon: material/poll
---

[Framavox](https://framavox.org) est un service en ligne de prise de décision proposé par l'association [Framasoft](https://framasoft.org).

Le service repose sur le logiciel libre [Loomio](https://www.loomio.com/).

Loomio est un outil en ligne, simple, facile à utiliser, qui permet des prises de décision collaborative. Loomio permet d’engager des discussions en y invitant les personnes concernées. Au moment opportun, Loomio permet de prendre des décisions et de transformer les délibérations en actions dans le monde réel.

**Voici [le tutoriel utilisateurice ici](./user.md) !**

Ainsi que [la documentation officielle là](https://docs.framasoft.org/fr/loomio/).