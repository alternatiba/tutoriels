---
title: Zoom - utilisateurice
author: Alternatiba / ANV-COP21
date: Janvier 2025
theme: beige
---

## ⚙️ Utilisation de Zoom

### Installation

Il y a deux solutions pour utiliser Zoom :

- **application bureau** : [télécharger (gratuit)](https://zoom.us/download) en avance et l'installer sur son ordinateur avant la réunion (**recommandé**)
- **application web** : se connecter directement avec un navigateur web *via* le lien d'invitation sans installer le logiciel

Si vous avez régulièrement des réunions Zoom, autant installer le logiciel \!

### Connexion

Que vous utilisiez le client web ou l'application bureau, nous vous invitons à **vous connecter en avance** afin de pouvoir régler tout problème technique et d'arriver à l'heure à la réunion \! Pour vous connecter, il suffit de cliquer sur le lien d'invitation de la réunion. Si vous avez installé l'application, elle s'ouvrira automatiquement.

Si une salle d'attente a été mise en place, vous tomberez sur ce message :

!!! quote ""

    **Veuillez patienter, l'hôte de la réunion vous laissera bientôt entrer.**

Il vous suffit d'attendre que l'animateur⋅ice de la réunion vous bascule dans la salle de réunion.

Une fois connecté·e, pensez à faire 3 choses dans l'interface de Zoom :

1. **Désactiver votre micro**
2. **Désactiver votre caméra**
3. **Ouvrir la fenêtre de conversation** (chat)

Vous pouvez faire ces 3 manipulations en appuyant simplement sur 3 boutons, visibles en bas de l'interface de Zoom :

![Interface Zoom](./media/zoom_interface.jpeg){ loading=lazy : style="width:800px"}

### Liens utiles

- [Planning visio](https://docs.google.com/spreadsheets/d/1q5-NvCgfL8Wmid6xQwPTX7kfSLLCKG7c0Ckk77Z1P2I/edit#gid=1885582118)
- [Paramètres du compte Zoom](https://zoom.us/account/setting)

## 📅 Réservation de créneau

### Réserver un créneau Zoom Alternatiba

L'abonnement Zoom d'Alternatiba nous permet de prendre en charge jusqu'à 100 participant·es. Pour pouvoir diffuser plus largement des tables rondes virtuelles par exemple, il existe la possibilité de diffuser en live sur Facebook et Youtube. Il est possible d'enregistrer les visios.

!!! info "Info"

    Alternatiba dispose d'un abonnement à Zoom depuis le 24/03/2020.

1. Vérifier dans l'onglet « [Planning visio](https://docs.google.com/spreadsheets/d/1q5-NvCgfL8Wmid6xQwPTX7kfSLLCKG7c0Ckk77Z1P2I/edit#gid=1885582118) » du tableur Team que le créneau souhaité est libre

2. Se connecter sur le compte Zoom Alternatiba à cette [adresse](https://us02web.zoom.us/signin#/login) (les identifiants sont partagés via [Bitwarden](../bitwarden/index.md))

3. Cliquer sur « Réunions » : vous accédez à la liste des créneaux déjà réservés

    ![Liste des réunions Zoom](./media/zoom_reunions.png){ loading=lazy : style="width:600px"}

4. Cliquer sur « Planifier une réunion »

    ![Planifier une réunion](./media/zoom_planifier-reunion.png){ loading=lazy : style="width:600px"}

5. Choisir les paramètres de la réunion :

    - **Sujet**, **Quand**, **Durée** (⚠️ les dates et horaires sont au **format américain**, vérifie bien que c’est OK !)

    - Dans **Sécurité**, décocher**« Salle d'attente »** et **« Code secret »** pour une salle accessible uniquement par lien (laisser les autres paramètres tels quels)

        ![Paramètres d'une réunion](./media/zoom_parametres-reunion.png){ loading=lazy : style="width:600px"}

6. Enregistrer : vous tombez sur la page récap de votre réunion sur laquelle vous pouvez copier le lien de la réunion

7. Ajouter le lien dans l'onglet « [Planning visio](https://docs.google.com/spreadsheets/d/1q5-NvCgfL8Wmid6xQwPTX7kfSLLCKG7c0Ckk77Z1P2I/edit#gid=1885582118) » du tableur Team

## ⭐ Informations pratiques

### Host, co-host, délégation de pouvoirs

Seul un·e admin *(Host)* a le pouvoir de **démarrer une réunion** et pendant une réunion de **couper le micro ou la caméra d'une personne** ou d'**enregistrer la session**. Mais ce pouvoir peut être délégué en début de réunion par un·e des admins à condition qu'iel soit connecté·e à la réunion ou alors en utilisant vous-même la clé pour être animateur⋅ice en début de réunion.

Si l'un⋅e des admins sera un⋅e participant⋅e à la réunion, iel pourra vous déléguer le rôle d'animation. Sinon :

**Animateur Zoom Alternatiba** :

- Demander à l'admin de cocher le paramétrage "Ouvrir l'accès à la réunion avant l'arrivée de l'animateur" de la session
- Rejoindre la réunion avant qu'elle ne démarre
- Aller dans la liste des participants > "Prendre le rôle d'animateur" (en bas)
- Renseigner le code admin Alternatiba (à retrouver sur Bitwarden, voir ci-dessous)

!!! info "Code admin sur Bitwarden"

    Tu peux retrouver les codes admin Zoom sur [Bitwarden](../bitwarden/index.md) dans la collection **"Commun EGQ - Team"** > dossier **"Visio"** > élément **"Zoom"** > champs **"Code admin"**. Il faut que tu aies accès à la collection pour voir. Tu peux faire une demande d'accès à [informatique@alternatiba.eu](mailto:informatique@alternatiba.eu). 

### Enregistrement

L'enregistrement peut se faire de 2 manières :

- **sur le cloud de Zoom** : si vous choisissez d'enregistrer sur le cloud, à la fin de la formation, prévenez l'admin qui se chargera d'uploader la vidéo sur Youtube afin de ne pas surcharger le cloud zoom
- **sur l'ordinateur de la personne qui enregistre** : donc soit sur l'ordinateur de l'admin ou celui de la personne à qui le pouvoir d'enregistrer a été délégué

!!! tip "Pour éviter d'oublier d'enregistrer"

    Si vous avez peur d'oublier d'enregistrer, vous pouvez demander à un·e admin de cocher l'option **"Enregistrement automatique de la réunion"** :

    - S'iel choisit l'option **"Sur ordinateur"**, il faudra que cet admin reste tout le temps de la réunion

    - S'iel choisit **"Sur le cloud"**, la réunion y sera enregistrée qu'il soit présent ou pas
        - Si l'option **"Ouvrir l'accès à la réunion avant l'arrivée de l'animateur"** a aussi été cochée, l'enregistrement démarrera de manière automatique à l'arrivée de la première personne participante dans la réunion

### Connexion par téléphone

Vous pouvez demander aux admins de permettre aux gens ou non de se connecter à la réunion par téléphone et le cas échéant leur fournir un numéro de téléphone pour se connecter (attention à préciser France dans les options).

### Contrôle de l'accès à la réunion

Vous avez 2 possibilités de contrôle (à demander à l'admin) :

- **Mettre un mot de passe à la réunion** : les participant·es devront le donner avant de pouvoir participer à la réunion
- **Cocher l'option "Activer la salle d'attente"** : ainsi, une fois que l'admin vous aura délégué le pouvoir, vous pourrez accueillir les participant·es dans une salle d'attente avant de les intégrer ou non dans la salle de réunion
    - Par exemple, cela peut-être utile si une 1ère partie d'une réunion n'est pas ouverte à tout le monde mais la 2ème oui
    - Vous pouvez pendant la réunion activer ou désactiver la fonction salle d'attente
    - Vous pouvez renvoyer quelqu'un qui est dans la réunion dans la salle d'attente

### Informations pratiques à destination des organisateurices

Le jour J pour lancer la réunion, ré-ouvrir l'application Zoom et cliquer sur le bouton "commencer" pour lancer la réunion à l'heure souhaitée :

![Commencer une réunion](./media/zoom_commencer-reunion.png){ loading=lazy : style="width:600px"}

Dès l'ouverture de la session, renommer le nom qui s'affiche par défaut pour vous dans la liste des participant-e-s :

![Se renommer au début d'une réunion](./media/zoom_renommer.png){ loading=lazy : style="width:300px"}

### Donner les pouvoirs d'animation et d'enregistrement pendant une réunion

- Cliquer sur le menu à côté de la personne à qui vous voulez donner les pouvoirs puis :
    - Soit **seulement permettre l'enregistrement**
    - Soit **nommer cette personne animateur·ice** (elle pourra aussi couper les micros ou les caméras)

    ![Enregistrer une réunion](./media/zoom_enregistrer.png){ loading=lazy : style="width:600px"}

### Live Facebook

1. Pour permettre la diffusion en live sur Facebook, rendez-vous en amont de la réunion sur  [l'espace web Zoom](https://us02web.zoom.us/signin#/login) :
    - Cliquez sur Gestion de compte \> [Paramètres du compte](https://zoom.us/account/setting)
    - Cliquez sur le bouton à droite "Permet la rediffusion en direct des réunions" sous "En réunion (avancé)"
2. Au moment de la réunion, vous devez être présent·e pendant toute la durée de la diffusion et disposer des accès aux comptes sur lesquels se fera la diffusion.
3. Ouvez Zoom et rejoignez la réunion
4. Sélectionnez "En direct sur Facebook"

    ![En direct sur Facebook](./media/zoom_direct-facebook.png){ loading=lazy : style="width:600px"}

5. Zoom vous ouvre une fenêtre Facebook : sélectionnez "Partagez sur une page que vous gérez" puis la bonne page

    ![Diffusez en direct sur Facebook](./media/zoom_publier-video-direct.png){ loading=lazy : style="width:400px"}

6. Écrivez votre publication et partagez \!

    ![En direct](./media/zoom_en-direct.png){ loading=lazy : style="width:600px"}

---

Tu es perdu⋅e ? Tu peux écrire à [informatique@alternatiba.eu](mailto:informatique@alternatiba.eu) ou sur le canal [⁉️ Questions et annonces](https://chat.alternatiba.eu/coordination-du-mouvement/channels/town-square). On est là pour t'aider ! :slight_smile:
