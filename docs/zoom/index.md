---
title: Zoom
author: Alternatiba / ANV-COP21
date: Janvier 2025
theme: beige
icon: material/video
---

À la différence de [Mumble](../mumble/index.md), Zoom permet de partager sa webcam et/ou son écran avec les autres utilisateur⋅ices (comme [BigBlueButton](../bbb/index.md)).

Il peut également servir à des réunions inter-orga pour lesquelles on ne voudrait pas utiliser le serveur Mumble interne (et donc ne pas partager les codes d'accès).

Zoom permet également de faire des sous-groupes, de partager un tableau blanc, d'enregistrer la réunion et de faire une diffusion en direct.

Retrouve le [tuto utilisateurice ici](./user.md) !
