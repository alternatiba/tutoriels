---
title: Bitwarden - administrateurices
author: Alternatiba / ANV-COP21
date: Janvier 2025
theme: beige
---

!!! info "Cible de ce tuto"

    Ce tuto est destiné aux administrateurices de Bitwarden, pour les utilisateurices voir [ce tuto](./user.md).

[Bitwarden](https://bitwarden.com) est un **gestionnaire de mots de passe**, c'est-à-dire un outil qui permet de partager facilement et de manière sécurisée des mots de passe au sein d'une équipe. En se connectant à Bitwarden avec son **mot de passe maître**, on accède à son coffre qui contient des identifiants.

Alternatiba / ANV-COP21 utilise l'instance Bitwarden hébergée par le [CHATONS](https://chatons.org) Tedomum.

**L'url de l'instance est : [vault.tedomum.net](vault.tedomum.net)**

## 📨 Inviter une nouvelle personne à Bitwarden

!!! warning "Invitation pour Bitwarden"

    Il faut d’abord que l’administrateurice invite la nouvelle personne. Elle ne doit pas tenter de créer un compte avant d’être invitée.

---

La procédure en résumé :

1. **L'admin invite l'utilisateurice**
2. **L'utilisateurice clique sur le lien d'invitation par mail**
3. **L'utilisateurice crée son compte**
4. **L'utilisateurice se connecte**
5. **L'admin confirme son compte**
6. **Et voilà, la personne a rejoint l’organisation et a accès aux mots de passe 🎉**

---

La procédure en détail :

1. **L'admin invite l'utilisateurice**

    - Se connecter à [https://vault.tedomum.net](https://vault.tedomum.net) avec son compte personnel

    - Dans le menu à gauche, cliquer sur "Admin Console" en bas

        ![Admin Console](./media/admin_admin-console.png){ loading=lazy : style="width:200px"}

    - En haut dans le nouveau menu de gauche, sélectionner l’organisation à laquelle on souhaite inviter une nouvelle personne (ici "Alternatiba / ANVCOP21" était déjà sélectionné par défaut comme c'est la seule)

        ![Collections](./media/admin_collections.png){ loading=lazy : style="width:900px"}

    - Cliquer sur l’onglet "Membres" dans le menu de gauche : la liste des membres apparaît

        ![Membres de l'organisation](./media/admin_membres.png){ loading=lazy : style="width:900px"}

    - Cliquer sur le bouton "+ Inviter un membre" en haut à droite

        ![Inviter un membre : rôle](./media/admin_inviter-membre-role.png){ loading=lazy : style="width:900px"}

    - Indiquer l’adresse mail et cocher le rôle "Utilisateur"

    - Cliquer sur l'onglet "Collections" en haut

        ![Inviter un membre : collections](./media/admin_inviter-membre-collections.png){ loading=lazy : style="width:900px"}

    - Sélectionner une permission et une collection (ou sélectionner plusieurs collections à la fois) puis enregistrer

        !!! info "Permissions"

            Voici toutes les permissions :

            - **"Peut voir"** → la personne peut voir et utiliser mais pas modifier
            - "Peut voir, sauf les mots de passe" → pour éviter que la personne puisse diffuser les mots de passe
            - **"Peut modifier"** → la personne peut voir, utiliser et modifier
            - "Peut modifier, sauf les mots de passe" → pour éviter que la personne puisse modifier les mots de passe
            - **"Peut gérer"** → la personne peut également ajouter ou supprimer des identifiants dans la collection
            
            On recommande "Peut voir", "Peut modifier" ou "Peut gérer" pour plus de simplicité

    - L’utilisateurice est maintenant "Invité" : iel doit maintenant confirmer l’invitation, créer son compte et se connecter

        ![Utilisateurice invitée](./media/admin_guest.png){ loading=lazy : style="width:900px"}

2. **L'utilisateurice clique sur le lien d'invitation par mail**

3. **L'utilisateurice crée son compte**

4. **L'utilisateurice se connecte**

5. **L'admin confirme son compte**

    - Sur [https://vault.tedomum.net](https://vault.tedomum.net) l’utilisateurice est maintenant en "Confirmation nécessaire"

        ![Confirmation nécessaire](./media/admin_confirmation-necessaire.png){ loading=lazy : style="width:900px"}

    - Cliquer sur le menu "..." puis sur "Confirmer"

        ![Menu confirmer](./media/admin_confirmer-menu.png){ loading=lazy : style="width:900px"}

    - Dans la popup qui apparaît cliquer sur "Confirmer" (pas besoin de vérifier la phrase d'empreinte)

        ![Popup confirmer](./media/admin_confirmer-popup.png){ loading=lazy : style="width:900px"}

6. **Et voilà, la personne a rejoint l’organisation et a accès aux mots de passe 🎉**

## 🔧 Gérer les accès des utilisateurices

Donner accès à une collection :

1. S’assurer que la personne a bien créé son compte en suivant [ce tuto](./user.md)

2. Se connecter à [https://vault.tedomum.net](https://vault.tedomum.net) avec son compte personnel

3. Dans le menu à gauche, cliquer sur "Admin Console" en bas

    ![Admin Console](./media/admin_admin-console.png){ loading=lazy : style="width:200px"}

4. Dans son coffre, cliquer sur le menu "..." de la collection à laquelle on veut donner les droits puis cliquer sur "Modifier l'accès"

    ![Accès d'une collection](./media/admin_modifier-acces-menu.png){ loading=lazy : style="width:900px"}

5. Dans la lise déroulante "Sélectionner les membres", sélectionner les membres à qui donner les droits (sauf besoin particulier, laisser en "Peut voir" uniquement)

    ![Membres ayant accès à une collection](./media/admin_modifier-acces-popup.png){ loading=lazy : style="width:900px"}

6. Cliquer sur "Enregistrer"

## 🔧 Gérer les collections

*À venir...*

---

Tu es perdu⋅e ? Fais signe à [informatique@alternatiba.eu](mailto:informatique@alternatiba.eu) si besoin ! On est là pour t'aider ! 🤓
