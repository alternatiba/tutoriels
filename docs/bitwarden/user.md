---
title: Bitwarden - utilisateurices
author: Alternatiba / ANV-COP21
date: Janvier 2025
theme: beige
---

!!! info "Cible de ce tuto"

    Ce tuto est destiné aux utilisateurices de Bitwarden, pour les administrateurices voir [ce tuto](./admin.md).

[Bitwarden](https://bitwarden.com) est un **gestionnaire de mots de passe**, c'est-à-dire un outil qui permet de partager facilement et de manière sécurisée des mots de passe au sein d'une équipe. En se connectant à Bitwarden avec son **mot de passe maître**, on accède à son coffre qui contient des identifiants.

Alternatiba / ANV-COP21 utilise l'instance Bitwarden hébergée par le [CHATONS](https://chatons.org) Tedomum.

**L'url de l'instance est : [vault.tedomum.net](vault.tedomum.net)**

!!! question "Bitwarden et Vaultwarden"

    Un peu partout, tu verras parfois "Bitwarden" et parfois "Vaultwarden". Tu peux considérer que c'est pareil. Si tu veux des détails et comprendre la différence, tu peux en parler avec la commission informatique ! 😉

## ✏️ Créer son compte Bitwarden

!!! warning "Invitation pour Bitwarden"

    Il faut se faire inviter pour rejoindre Bitwarden. Attends de recevoir une **invitation par mail** avant de créer un compte.

---

La procédure en résumé :

1. **L'admin t'invite**
2. **Tu cliques sur le lien d'invitation par mail**
3. **Tu crées ton compte**
4. **Tu te connectes**
5. **L'admin confirme ton compte**
6. **Ça y est, ton coffre à mots de passe fonctionne 🎉**

---

La procédure en détail :

1. **L'admin t'invite**
    - Demande à une personne administratrice de t'inviter : donne-lui ton adresse mail pour qu'elle t'envoie l'invitation
    - Voir la liste des admins dans [le tableau Team](https://docs.google.com/spreadsheets/d/1q5-NvCgfL8Wmid6xQwPTX7kfSLLCKG7c0Ckk77Z1P2I/edit#gid=846288416), onglet "Outils"
2. **Tu cliques sur le lien d'invitation par mail**
    - Tu devrais recevoir "You have been invited to join Alternatiba organization" → clique sur "Join now"
3. **Tu crées ton compte**
    - Choisis "Créer un compte" et entre les informations nécessaires (mail, nom, mot de passe)

        ![Créer un compte](./media/user_create-account.png){ loading=lazy : style="width:400px"}

    - Nous t'invitons à choisir un mot de passe maître solide que tu n'utilises sur aucun compte par ailleurs.
        - Plutôt qu'un mot de passe, choisis plutôt une [phrase de passe](https://xkcd.com/936/) !
        - **⚠️ Tu dois absolument te souvenir de ton mot de passe maître. Il est impossible de le récupérer !**
        - Tu peux utiliser la fonction "indice de mot de passe" si tu as peur d'oublier ton mot de passe maître
    - Tu tombes ensuite sur la page d'accueil [https://vault.tedomum.net](https://vault.tedomum.net) avec un petit message qui confirme la création de ton compte

        ![Compte créé](./media/user_account-created.png){ loading=lazy : style="width:300px"}

4. **Tu te connectes**
    - Maintenant connecte toi, tu devrais voir un nouveau message qui te dit que l'invitation a été acceptée

        ![Invitation confirmée](./media/user_invitation-accepted.png){ loading=lazy : style="width:300px"}

5. **L'admin confirme ton compte**
    - Écris un message à la personne qui t'a invitée pour lui demander "confirme mon compte stp"
        - En attendant, tu peux [installer l'extension Bitwarden](#configurer-lextension-dans-son-navigateur) sur ton navigateur (ça te sera utile !)
        - Tu peux aussi vérifier ton adresse mail (le petit encart orange en haut à droite)

        ![Vérifier l'adresse e-mail](./media/user_verify-address.png){ loading=lazy : style="width:300px"}

6. **Ça y est, ton coffre à mots de passe fonctionne 🎉**

## 🔧 Configurer l'extension dans son navigateur

L'extension "Bitwarden" te permet de te connecter très facilement aux différents comptes qui sont stockés dans ton coffre. Il y a cependant une petite configuration initiale à faire.

1. Installe l'extension **"Bitwarden"** pour ton navigateur :
    - [ici pour Firefox](https://addons.mozilla.org/fr/firefox/addon/bitwarden-password-manager/)
    - [ici pour Google Chrome](https://chrome.google.com/webstore/detail/bitwarden-free-password-m/nngceckbapebfimnlniiiahkandclblb)
    - [ici pour Safari](https://apps.apple.com/us/app/bitwarden/id1352778147) (attention, il faut nécessairement installer l'application de bureau pour Mac, [détails ici](https://bitwarden.com/help/install-safari-app-extension/))

    !!! tip "Pour plus d'infos"

        Si tu n'es pas à l'aise avec l'utilisation d'extensions dans ton navigateur, prends le temps de lire les tutos spécifiques à ton navigateur pour comprendre le principe : [ici pour Firefox](https://support.mozilla.org/fr/kb/trouver-installer-modules-firefox#w_comment-trouver-et-installer-les-modules-complementaires) et [ici pour Google Chrome](https://support.google.com/chrome_webstore/answer/2664769?hl=fr)

2. Connecte toi à l'extension
      - Une fois installée, clique sur l'icône en forme de bouclier qui est apparue en haut à droite du navigateur
      - Avant de te connecter, tu dois indiquer à l'extension le serveur Bitwarden auquel se connecter
        - Clique sur la flèche à côté de [bitwarden.com](bitwarden.com)
        - Choisis **"Auto-hébergé"** dans le menu déroulant
        - Indique l'URL du serveur : **[https://vault.tedomum.net](https://vault.tedomum.net)**
        - Et n'oublie pas d'enregistrer !

            ![Se connecter à un serveur auto-hébergé](./media/user_auto-heberge.gif){ loading=lazy : style="width:400px"}

      - Maintenant, tu peux te connecter 😊

## 🔐 Utiliser Bitwarden au quotidien

L'extension Bitwarden de ton navigateur va automatiquement détecter s'il y a un identifiant stocké dans ton coffre quand tu navigues en ligne. S'il y a plusieurs identifiants qui correspondent au site visité, l'extension les propose tous, tu peux choisir.

1. Connecte-toi dans l'extension (déverrouiller son coffre)

    !!! question "Quand déverrouiller son coffre ?"

        Tu dois déverrouiller ton coffre à chaque fois que tu ouvres ton navigateur. Tu vas voir que ça devient un réflexe de te connecter une seule fois en début de journée.

2. Va sur la page où tu veux te connecter (par exemple Wordpress, la boîte mail, etc.)

    !!! info "Bien choisir la page"

        Attention à bien aller sur la page où apparaissent les champs nom d’utilisateur / mot de passe, sinon ca ne marche pas (par exemple pour sendinblue, c’est ici : [https://app.sendinblue.com/account/login](https://app.sendinblue.com/account/login)).

3. Clique sur l'icône de l'extension, sélectionne l'identifiant que tu souhaites, et hop !

    ![Remplissage automatique à la connection](./media/user_autofill.gif){ loading=lazy : style="width:600px"}

    !!! tip "Remplissage automatique"

        Astuce pour le remplissage des champs de connexion : au lieu de cliquer sur l'icône, puis sur l'identifiant, tu peux utiliser le raccourci clavier **Ctrl+Maj+L**. Ou encore configurer le remplissage automatique dans les paramètres de l'extension.

## 🤯 FAQ / problèmes fréquents

### Bitwarden dit que je n'ai pas d'identifiants enregistrés pour un site, alors que je devrais en avoir

1. Est-ce que tu as bien [configuré l'extension dans ton navigateur](#configurer-lextension-dans-son-navigateur) ?
    - Pour l'installer, tu as besoin de chercher l'extension dans le "store" de ton navigateur Internet - pour en savoir plus, regarde [ici pour Google Chrome](https://chrome.google.com/webstore/detail/bitwarden-free-password-m/nngceckbapebfimnlniiiahkandclblb) et [ici pour Firefox](https://addons.mozilla.org/fr/firefox/addon/bitwarden-password-manager/)
    - Vérifie que tu as bien fait l'étape d'entrer l'URL du serveur dans l'extension
2. Est-ce que tu es bien sur la **page de connexion** du site auquel tu souhaites accéder ?
    - Est-ce que tu vois affiché un champ où tu peux entrer l'adresse de messagerie et le mot de passe ?
    - Exemples : [https://canva.com/login](https://canva.com/login) et [https://account-app.brevo.com/account/login](https://account-app.brevo.com/account/login)
3. Tu peux aussi essayer de [synchroniser ton coffre](#un-mot-de-passe-a-ete-mis-a-jour-mais-ca-ne-fonctionne-pas-chez-moi)
4. Si rien de tout ça ne marche, tu peux contacter la commission informatique en disant : *"J'ai essayé ce qui est écrit dans le tuto juré craché !"*

### J'ai installé l'application Bitwarden sur mon ordi, mais ça ne marche pas

C'est normal, car il faut installer une extension navigateur (mini-appli à l'intérieur de ton navigateur), pas une appli ordi.

[C'est là que ça se passe](#configurer-lextension-dans-son-navigateur) !

### Un mot de passe a été mis à jour mais ça ne fonctionne pas chez moi

C'est probablement dû au fait que ton extension n'est pas synchronisée avec le coffre fort...

Si c'est ton cas il te suffit de **lancer manuellement la synchronisation** : 

1. Ouvre l'extension
2. Va dans l'onglet "Paramètres" en bas à droite
3. Choisis "Coffre" et clique sur le bouton "Synchroniser le coffre maintenant"

    ![Synchroniser maintenant](./media/user_sync-now.gif){ loading=lazy : style="width:400px"}

4. Hop ! Le mot de passe est apparu !

### Je n'arrive pas à me connecter à l'extension

1. Vérifie que l'URL du serveur est la bonne, c'est ce qui se trouve dans le champ "URL du serveur" quand tu vas dans les paramètres de l'extension (cf. [cette section]((#configurer-lextension-dans-son-navigateur)))
2. Vérifie que le login / mot de passe est bon
3. Essaie de te connecter directement sur [https://vault.tedomum.net](https://vault.tedomum.net)
4. Si ça marche sur le site web mais pas sur l'extension, désinstalle et ré-installe l'extension

### J'ai oublié mon mot de passe maître

Aïe aïe aïe ! **Il n'est pas possible de récupérer son mot de passe maître.** Peut-être que tu as indiqué un indice pour te souvenir de ton mot de passe maître ? Si oui, clique sur "Obtenir l'indice du mot de passe principal" en-dessous du champ de mot de passe. Sinon, cherche dans ta mémoire, dans tes carnets...

![Indice de mot de passe maître](./media/user_password-hint.png){ loading=lazy : style="width:400px"}

Si tu ne te souviens vraiment plus de ton mot de passe maître, tu as malheureusement perdu tous les mots de passe de ton coffre !

- Ouf ! Les mots de passe d'Alternatiba / ANV-COP21 sont sains et saufs, tu pourras de nouveau y avoir accès
- Aïe ! Tes mots de passe perso sont bel et bien disparus dans les méandres d'Internet... J'espère que tu as fait une sauvegarde !

Dans ce cas, tu es obligé·e de supprimer ton compte et de le re-créer à nouveau :

1. Rendez-vous sur [https://vault.tedomum.net/#/recover-delete](https://vault.tedomum.net/#/recover-delete)
2. Entre l'adresse de messagerie associée à ton compte
3. Tu reçois un courriel avec le fameux bouton "Delete Your Account"
4. Si tu es sûr⋅e de toi, clique à nouveau sur "Supprimer le compte" : **attention, c'est un bouton de non-retour** !

    ![Supprimer son compte](./media/user_delete-account.png){ loading=lazy : style="width:400px"}

5. Pouf ! [Retour à la case départ](#creer-son-compte-bitwarden) sans toucher 20 000 francs !

### J'ai l'erreur "Failed to fetch" quand j'essaie de me connecter

Il faut vider le cache ou supprimer les cookies... On a essayé une fois et ça a marché !

### Ajouter un élément dans mon coffre perso

Lors de la création de ses accès à l'organisation Alternatiba / ANV-COP21, chaque compte reçoit également un **coffre personnel** sur cette même instance. Les coffres persos sont indépendants du coffre de l'organisation (même la commission informatique ne peut les voir !). Ces coffres persos peuvent donc tout à fait vous servir de **gestionnaire de mots de passe personnel car les deux espaces sont séparés**.

Vous pouvez gérer votre compte perso comme bon vous semble en y ajoutant les éléments que vous souhaitez ainsi que des dossiers pour tout bien rangé. Lorsque vous quitterez le mouvement et qu'on supprimera vos accès à l'organisation Alternatiba / ANV-COP21 de Bitwarden, vous garderez votre coffre perso !

Il y a deux façons d'ajouter un élément dans son coffre perso :

- ⚙️ **Automatiquement**
    - Lorsque tu crées un compte sur un site internet, l'extension Bitwarden peut te proposer (parfois l'extension ne détecte pas la création d'un nouveau compte, il faut donc le faire manuellement) d'ajouter les identifiants du compte que tu viens de créer dans ton coffre

        ![Enregistrer un mot de passe automatiquement](./media/user_save-password.png){ loading=lazy : style="width:600px"}

    - Dans ce cas, tu peux cliquer sur Enregistrer et un nouvel élément est créé dans ton coffre perso
- ✋ **Manuellement**
    - **soit en passant par l'extension** :
        - clique sur le bouton "+" en haut à droite de la fenêtre de l'extension

            ![Enregistrer un mot de passe manuellement](./media/user_new-password.png){ loading=lazy : style="width:400px"}

        - entre les infos dans les champs correspondants
        - le champ "Site web (URI)" correspond à l'adresse à laquelle l'extension va s'activer pour cet élément (choisir l'url de login du site)
    - **soit en passant par l'interface de [vault.tedomum.net](https://vault.tedomum.net)** :
        - connecte toi à [vault.tedomum.net](https://vault.tedomum.net)
        - clique sur Nouveau > Élément
        - remplis les champs

!!! tip "Enregistrer des notes"

    Tu n'es pas obligé·e de rentrer des identifiants ! Tu peux juste te servir d'un élément pour stocker du texte dans le champ Notes (un mot de passe uniquement, une clé de chiffrement, des infos importantes, etc.).

### Ajouter un élément dans le coffre Alternatiba

Cela peut se produire si par exemple tu as créé un compte sur un nouvel outil avec une adresse générique comme [contact@alternatiba.eu](mailto:contact@alternatiba.eu) ou [comext@alternatiba.eu](mailto:comext@alternatiba.eu). 

- 📍 **Dès la création du compte**
    - Dans ce cas, une fois que tu as validé les identifiants, ton extension Bitwarden peut te proposer de stocker les identifiants dans ton coffre.
    - Par défaut, les identifiants sont stockés dans ton coffre perso mais tu peux choisir un autre coffre : 
        - clique sur le bouton Modifier de la fenêtre qui apparaît

            ![Modifier un mot de passe automatiquement](./media/user_update-password.png){ loading=lazy : style="width:600px"}

        - dans la nouvelle fenêtre qui s'ouvre, scrolle tout en bas pour trouver le champ "À qui appartient cet élément ?"
        - dans la liste déroulante, choisis Alternatiba / ANV-COP21
        - la liste des collections apparaît en-dessous et tu peux choisir dans laquelle mettre ton élément

            ![Propriété d'un mot de passe](./media/user_password-owner.png){ loading=lazy : style="width:400px"}

- ⏱️ **Plus tard**
    - **soit en passant par l'extension** : 
        - cherche un élément dans ton coffre via le menu rechercher de l'extension
        - clique sur l'icône modifier (tu retombes sur le même menu que le point précédent)
    - **soit en passant par l'interface de [vault.tedomum.net](https://vault.tedomum.net)** : 
        - clique sur les trois petits points à droite d'un élément dans la liste
        - puis clique sur "Déplacer vers l'organisation"

            ![Déplacer un mot de passe](./media/user_move-password.png){ loading=lazy : style="width:300px"}

!!! info "Droits de modification"

    **Seules les personnes qui ont les droits de modification sur les collections peuvent ajouter des éléments.** Il est donc possible que vous ne voyiez pas apparaître la liste des collections ou que la collection à laquelle vous souhaitez ajouter l'élément n'apparaisse pas. Dans ce cas, c'est probablement que vous n'avez pas les bons droits : **contactez l'équipe informatique** !

### La gestion des mots de passe : un peu de théorie et quelques bonnes pratiques

!!! warning "À lire jusqu'au bout"

    Si vous lisez cette partie, il est important de la lire jusqu'au bout pour comprendre le raisonnement en entier ! Ne pas s'arrêter au milieu.

Contrairement à ce qu'on l'entend parfois (et ce que laissent supposer les recommandations des interfaces de création de compte de type « au moins un chiffre et un caractère spécial », etc.), la sécurité d'un mot réside essentiellement dans sa longueur et son unicité (voir [cette section](#au-fait-cest-quoi-un-bon-mot-de-passe)) et pas forcément dans le nombre de caractères spéciaux ou de chiffres qu'il contient.

Par exemple, ce n'est *pas beaucoup plus dur* d'attaquer « r3nAr$ » que « renard » alors que c'est *beaucoup plus dur* d'attaquer « renard fraise » (alors que celui-ci est peu voire pas plus dur à retenir). Voici quelques exemples de temps de déchiffrement de quelques mots de passe d'après le site [https://www.security.org/how-secure-is-my-password/](https://www.security.org/how-secure-is-my-password/).

| Mot de passe                                       | Temps pour le déchiffrer                  | Commentaire                                                                                                             |
| -------------------------------------------------- | ----------------------------------------- | ----------------------------------------------------------------------------------------------------------------------- |
| renard                                             | 7 millisecondes                           |                                                                                                                         |
| r3n4r$!                                            | 22 secondes                               |                                                                                                                         |
| renard fraise                                      | 2000 ans                                  | ← le gap de déchiffrement est énorme par rapport à la complexité du mdp                                                 |
| correct horse battery staple                       | 15 octillion années (LOL)                 | ← assez facile à retenir mais pourtant hyper sûr **(on appelle ça une phrase de passe)**                                |
| #TatqXMAvLAPjc!aB*446MYHLepu5Y@smeMuE3dAdSr4XDCd6T | 16 quattuorvigintillion années (MÉGA LOL) | ← 50 caractères complètements aléatoires : impossible à retenir mais hyper sûr et très facile à générer automatiquement |

Ainsi, dans l'idéal, il faut retenir des mots de passe de type « banane éléphant torchon pokemon » pour le site de la CAF, « ordinateur mangue avion macron » pour le site des impôts, etc. **Ce qui est clairement ingérable... si l'on n'utilise pas de gestionnaire de mot de passe.** En effet, un des intérêts majeurs d'un gestionnaire de mot de passe est qu'il stocke les mots de passe à notre place. Pas besoin de s'en souvenir. 

Un début de bonne pratique est donc :

- **une phrase de passe (que l'on retient) pour son compte Bitwarden**
- **des mots de passe de 20 ou 30 caractères aléatoires stockés dans Bitwarden pour tous les autres comptes**

Beaucoup de gestionnaires de mot de passe, dont Bitwarden, permettent de générer des mots de passe complètement (pseudo-)aléatoires très facilement !

Mais, ça veut donc dire que je n'ai qu'un seul mot de passe à retenir ? Et bien, **malheureusement, non** ! Cette bonne pratique a en effet un inconvénient : **votre coffre Bitwarden perso contient des informations que vous ne connaissez pas** (les mots de passe impossibles à retenir) **et que vous ne pouvez pas retrouver si les serveurs sur lesquels ils sont stockés brûlent** par exemple.

Il est donc très important d'en faire des copies régulières, **en exportant votre coffre** (voir [partie suivante](#exporter-son-coffre)) et les stocker correctement. Bien évidemment, l'idée est de ne pas stocker ça chez Google ou de le laisser traîner n'importe où sur son PC. Il faut en **assurer la sécurité** !

Une bonne pratique est donc de stocker son coffre sur différents supports. Une bonne pratique est au moins 2, idéalement 3, **espaces chiffrés** pour se prémunir contre la destruction physique de votre coffre :

- Le premier espace est bien sûr le serveur [vault.tedomum.net](https://vault.tedomum.net). Vous pouvez y accéder avec une phrase de passe.
- Un second espace possible est votre compte protonmail. Vous pouvez mettre le coffre exporté dans un brouillon. Il sera stocké sur sur les serveurs chiffrés de protonmail.
- Un troisième espace possible est un disque chiffré. Si votre ordinateur est chiffré ou si vous avez une clé USB chiffrée, vous pouvez stocker votre coffre dessus.

Vous avez donc compris ! Une bonne pratique complète est donc d'utiliser : 

1. **une phrase de passe (que l'on retient) pour son compte Bitwarden**
2. **des mots de passe de 20 ou 30 caractères aléatoires générés par Bitwarden pour tous les autres mots de passe**
3. **une phrase de passe (que l'on retient) pour son compte protonmail et y stocker l'export de son coffre**
4. **une phrase de passe (que l'on retient) pour chiffrer son ordinateur ou une clé USB et y stocker l'export de son coffre**

Si cela vous rassure, il est tout à fait possible de générer des phrases de passe (et non pas des aléas) pour certains comptes afin de les retenir (en plus de les mettre dans votre coffre), en gardant en tête que : 

- les phrases de passe doivent être indépendantes les unes des autres autant que possible
- les phrases de passe doivent contenir au moins 3 voire 4 mots

### Exporter son coffre

Pour exporter son coffre :

- Se connecter sur [vault.tedomum.net](https://vault.tedomum.net)
- Aller dans Outils > Exporter le coffre 
- Deux options : 
    - 🔓 **Votre PC n'est pas chiffré**
        - Choisir le format .json (Encrypted)
            - ⚠️ C'est un élément sensible, il est conseillé de ne pas le télécharger en clair si votre ordinateur n'est pas chiffré !
        - Deux options :
            - **Restreint à ce compte** :  le json est chiffré avec la clé de chiffrement de votre compte et ne pourra être réutilisé qu'en l'important sur ce même compte.
            - **Protégé par mot de passe** : chiffré par un nouveau mot de passe (encore et toujours une phrase de passe !) afin de pouvoir l'utiliser sur d'autres comptes.
    - 🔒 **Votre PC est chiffré**
        - Vous pouvez l'exporter en clair pour le stocker sur protonmail ou un autre disque chiffré (mais autant le stocker de manière chiffrée).
        - ⚠️ Attention à ne pas le stocker ou le diffuser sur des canaux non chiffrés.

### Au fait, c'est quoi un "bon" mot de passe ?

Un "bon" mot de passe est :

- **spécifique** : utilisé uniquement pour ce site web
- **long** : 10-20 caractères, ou plus...
- **mémorisable** : il est écrit nulle part ailleurs que dans ta mémoire
- **unique** : si possible, il comporte des mots qui n'existent pas dans le dictionnaire (mots inventés, nom propre, symboles, initiales...)
- **drôle** : pour s'en souvenir plus facilement et sourire quand tu l'écris ☺️

On recommande plutôt d'utiliser une phrase de passe, comme expliqué dans [cette BD](https://xkcd.com/936) :

![xkcd#936](./media/user_xkcd-936.png){ loading=lazy : style="width:600px"}

Une autre méthode consiste également à prendre la première lettre de chaque mots d'une phrase.

Exemple : **Mcavdlg,c'evd!** → "Mon chat a vomi dans le garage, c'était vraiment dégueulasse !"

Pour aller plus loin, voici [un article intéressant indiquant les recommandations du NIST](https://grenoble.ninja/le-nist-recommande-de-nouvelles-regles-pour-la-securite-des-mots-de-passe) (National Institute of Standards and Technology) en matières de mots de passe (pour une fois, c'est bien la taille qui compte).

---

Tu es perdu⋅e ? Fais signe à [informatique@alternatiba.eu](mailto:informatique@alternatiba.eu) si besoin ! On est là pour t'aider ! 🤓 
