---
title: Bitwarden
author: Alternatiba / ANV-COP21
date: Janvier 2025
theme: beige
icon: simple/bitwarden
---

[Bitwarden](https://bitwarden.com) est un **gestionnaire de mots de passe**, c'est-à-dire un outil qui permet de partager facilement et de manière sécurisée des mots de passe au sein d'une équipe. En se connectant à Bitwarden avec son **mot de passe maître**, on accède à son coffre qui contient des identifiants.

Alternatiba / ANV-COP21 utilise l'instance Bitwarden hébergée par le [CHATONS](https://chatons.org) Tedomum.

**L'url de l'instance est : [vault.tedomum.net](vault.tedomum.net)**

---

Utilisateurice ? [C'est par ici !](./user.md)

Administrateurice ? [C'est par là !](./admin.md)

---

Tu es perdu⋅e ? Fais signe à [informatique@alternatiba.eu](mailto:informatique@alternatiba.eu) si besoin ! On est là pour t'aider ! 🤓 
