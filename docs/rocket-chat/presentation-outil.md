---
title: Outil de communication interne
author: Alternatiba
date: Avril 2018
theme: beige
---

## Rappel des objectifs

- permettre un échange entre les groupes Alternatiba/ANV-COP21/militant.e.s sur des sujets spécifiques
- faciliter la communication entre l'équipe d'animation globale et les groupes Alternatiba/ANV-COP21/militant.e.s
- permettre à tout notre réseau, y compris les personnes ne faisant pas partie d'un groupe Alternatiba/ANV-COP1 de suivre les actualités
- créer un réseau social interne où chaque membre du mouvement sait qu'il y retrouve sa communauté, peut accéder à toutes les informations dont il y a besoin et interagir avec les autres membres de la communauté
- recruter de nouveaux militant.e.s, bénévoles, activistes sur les différents projets et événements que nous organisons grâce à un canal de communication direct

## Fonctionnalités de Mattermost

----

### Discussion en ligne

Envoyer des messages

-   sur les salons publics
-   dans des groupes privés
-   de personne à personne

### Fonctionnalités pratiques

-   Regroupement de canaux en
-   Partage de fichiers et d'images
-   Recherche dans l'historique des messages
-   Création et modération des canaux de discussion
-   Regroupement de canaux en catégories, que l'on peut rendre silencieuses ou mettre en favori
-   Applications mobiles pour Android et Iphone

## Logiciel libre, hébergement alternatif

Mattermost est un logiciel libre, que nous hébergeons chez Cloud Girofle, membre du collectif CHATONS (Collectif des Hébergeurs Alternatifs Transparents Ouverts Neutres et Solidaires).

* respect de notre vie privée
* dans les valeurs de Alternatiba
* gouvernance horizontale

## Le chat d'Alternatiba / ANV-COP21

### Les salons à venir

* general: accueil et discussions générales
* fil-infos: actus des réseaux sociaux et du site

Où demander de l'aide ? **aide_mattermost**
Où discuter ? **coin_militant** et **coin_detente**

### Salons thématiques

* alternatives_territoriales
* tour_alternatiba_2024
