---
title: Utiliser Rocket.Chat au quotidien
author: Alternatiba / ANV-COP21
date: Juillet 2022
theme: beige
---

!!! warning "Rocket.Chat est mort, vive Mattermost !"
    Depuis février 2024, nous avons changé d'outil de communication interne et utilisons désormais Mattermost.
    
    👉 **RDV sur [ce tuto](/mattermost/) pour découvrir et prendre en main Mattermost.**

    L'instance Rocket.Chat d'Alternatiba / ANV-COP21 reste accessible en lecture seule pendant un temps à l'adresse [https://chat-archives.alternatiba.eu](https://chat-archives.alternatiba.eu).

## ✏️ Rédaction des messages

### Comment poster un message ?

Sur ordinateur et sur smartphone : en bas de l’écran une ligne permet d’écrire ton message.

Pour rédiger des messages avec de la mise en forme (gras, liens, titres), tu peux suivre [ce tutoriel](/rocket-chat/mise-en-forme).

### Comment lire les messages d’un canal ?

**Sur ordinateur** : A gauche de l’écran, la liste des canaux est visible. Les canaux sur lesquels il y a des nouveaux messages apparaissent en gras avec le nombre de messages non-lus. Le fait de cliquer sur un canal fait s’afficher son contenu dans la fenêtre centrale.

**Sur application mobile** : clique sur le nom d’un canal pour voir les messages et fait “retour” pour revenir à la liste.

### Comment réagir et répondre à un message ?

Pour réagir à un message, passe ta souris sur le message et clique sur l’icône “emoji” en haut à droite.

Sur smartphone, appuie longtemps sur le message pour voir l’icône apparaître.

## 📲 Installer l’application

Tu peux choisir d’utiliser Rocket.Chat en te rendant sur [https://chat.alternatiba.eu](https://chat.alternatiba.eu){target=_blank} dans un navigateur Internet, mais nous te conseillons d’installer l’application Rocket.Chat. Une application est disponible :

- sur ordinateur (Desktop app)
- sur iOS
- sur Android
    

Télécharge l’application sur [https://rocket.chat/install](https://rocket.chat/install){target=_blank} ou sur [AppleStore](https://www.apple.com/fr/store){target=_blank} / [PlayStore](https://play.google.com/store/games){target=_blank}, puis au lancement de l’application, entre les informations :

- URL du serveur :  https://chat.alternatiba.eu
- Adresse mail ou nom d’utilisateur : l’adresse choisie lors de l’inscription
- Mot de passe : le mot de passe choisi lors de l’inscription



## 🔔 Notifications

!!! warning "Attention"
	**Il n'y a pas de notification sur le téléphone. C'est normal.**

Si tu ne te connectes pas souvent et que tu veux suivre les discussions d'un canal spécifique, tu peux activer les **notifications par mail** pour ce canal :

1. Dans le menu en haut à droite du canal, clique sur "Préférence pour les notifications"

	![Rocket.Chat](media/rc-clic-preferences-notifications.png){ loading=lazy .md-img-border }

2. Dans la catégorie "E-mail", choisis comment tu souhaites être notifiée :

	- Tous les messages
	- Mentions (uniquement quand tu es mentionné⋅e dans un message)
	- Rien

	![Rocket.Chat](media/rc-config-notifications-mail.png){ loading=lazy .md-img-border }

3. N'oublie pas d'enregistrer !



## 🔑 Chiffrer les communications - E2E Password

Vous allez découvrir ce magnifique **bandeau bleu** en haut de l'interface de Rocket.Chat. C'est ce qui permet de renseigner ta clef de chiffrement. E2E signifie “end to end”, expression anglaise pour chiffrement “de bout en bout”. Ce protocole permet de sécuriser les communications.

Rocket.Chat crée automatiquement un mot de passe E2E pour chaque utilisateurice. Dès que tu en as défini un, pense bien à le noter car il te sera demandé régulièrement.

**En cas d’oubli du mot de passe E2E suivre les étapes suivantes** : 
mon compte > sécurité > chiffrement de bout en bout > réinitialiser la clé de chiffrement.



## 😀 Ajouter des membres

Clique sur le canal (ou la discussion) dans lequel tu souhaites ajouter une personne puis :

**Sur ordinateur :**

1.  Clique en haut à droite sur les petits bonhommes :
    
    ![Rocket.Chat](media/rc-bouton-membres.png){ loading=lazy .md-img-border }


2.  Si tu es propriétaires du canal, alors en bas à droite apparaît :

	![Rocket.Chat](media/rc-bouton-ajouter-membre.png){ loading=lazy .md-img-border }


3.  Tape le nom de la ou des personnes puis clique sur le bouton "Ajouter"

	![Rocket.Chat](media/rc-choix-nouveau-utilisateur.png){ loading=lazy .md-img-border }


**Sur l'application mobile :**

1.  Clique en haut sur le nom du canal

    Si tu as le droit d’ajouter des personnes, alors l'option apparaît :

	![Rocket.Chat](media/rc-bouton-ajouter-membre-mobile.png){ loading=lazy .md-img-border }

2.  Tape le nom de la ou des personnes puis clique sur "suivant" en haut à droite de l'écran.


### Lien d'invitation

Il est possible de créer un lien qui permettra aux personnes qui le suivent de rejoindre un canal. Pour créer ce lien, il faut être **propriétaire** du canal en question.

!!! warning "Attention"
	Si tu crées un nouveau lien, l'ancien sera révoqué.


#### Sur ordinateur :

1. Clique en haut à droite sur les petits bonhommes :
    
    ![Rocket.Chat](media/rc-bouton-membres.png){ loading=lazy .md-img-border }
    
2. Si tu es propriétaires du canal, alors en bas à droite apparaît :

	![Rocket.Chat](media/rc-bouton-lien-invitation.png){ loading=lazy .md-img-border }

3. Tu peux configurer le lien d'invitation : durée d'expiration, nombre maximal d'utilisations.

4. Copie le lien qui apparait et diffuse-le.

5. Si tu veux changer le lien, clique sur "modifier l'invitation" et choisis le temps de validité du nouveau lien et son nombre d'utilisations.

	![Rocket.Chat](media/rc-lien-modifier-invitation.png){ loading=lazy .md-img-border }



#### Sur l'application mobile :

1. Clique en haut sur le nom du canal
    
2. Si tu as le droit d’ajouter des personnes, alors l'option apparaît !

    **PHOTO**
    
3. Tu peux configurer le lien d'invitation : durée d'expiration, nombre maximal d'utilisations.

4. Copie le lien qui apparait et diffuse-le.

5. Si tu veux changer le lien, clique sur "modifier l'invitation" et choisis le temps de validité du nouveau lien et son nombre d'utilisations.
    

### Gérer les rôles

Il existe différents rôles sur Rocket.Chat. Si vous n'en n'avez pas, vous êtes un⋅e utilisateurice "classique". Ces rôles permettent d'avoir des autorisations supplémentaires afin de gérer les canaux de discussion.

- **Moderateur⋅ice** : permet d'avoir l'accès en écriture dans un canal qui est en lecture seule pour les utilisateurices classiques. Les propriétaires du canal peuvent attribuer ce rôle.

- **Propriétaire** : permet de configurer le canal (nom, description, message épinglé...), ajouter des nouvelles personnes, créer un lien d'invitation, supprimer le canal. La personne qui crée un canal de discussion devient automatiquement son propriétaire.

!!! tip "Conseil"
    Quand tu crées un canal, veille à **attribuer le rôle de propriétaire à quelques personnes** pour qu'elles puissent configurer le canal et inviter d'autres personnes.

- **Admin** : permet de configurer Rocket.Chat et donne de nombreux droits d'administration. Seulement quelques membres de l'équipe d'animation ont ce rôle. Tu peux contacter l'équipe d'administration par courriel : <a href="mailto:admin_rc@alternatiba.eu" target="_blank">admin_rc@alternatiba.eu</a>.


## 💬 Canal, fil, discussion

!!! question "Quelle différence entre un canal ? un fil ? une discussion ?"

    * Un **canal** est l'élément "de base" dans lequel on peut discuter, se répondre, interagir... N'importe qui peut créer un canal

    Un canal est représenté par un croisillon **#**. Il peut être public ou privé.

    On peut facilement faire référence à un canal en tapant son nom `#nom-du-canal` dans un message :

    ![Rocket.Chat](media/rc-reference-canal.png){ loading=lazy .md-img-border }

    Le nom du canal apparaît également dans l'URL sur navigateur après `/channel/` pour un canal public ou `/group/` pour un canal privé.


    * Un **fil** est créé quand une personne répond à un message. Cela permet d'échanger sur un sujet particulier (répondre à une question par exemple) sans que les messages ne s'entrecroisent dans le canal principal.

    ![Rocket.Chat](media/rc-bouton-repondre-fil.png){ loading=lazy .md-img-border }

    Un **fil** est représenté par **une bulle**.

    ### Discussion

    * Une **discussion** est représentée par **deux bulles**.


### Créer une discussion

Une **discussion** se créé à partir d'un canal parent. Il sera possible de rejoindre cette discussion à partir du canal en question mais aussi à partir de la liste de l'ensemble de tes canaux.

#### Sur ordinateur :

1. Sur le canal parent, à droite du champ pour écrire, clique sur le "+" puis sélectionne "discussion".

	![Rocket.Chat](media/rc-creer-discussion.gif){ loading=lazy .md-img-border }

2. Remplis les différentes informations nécessaires : le nom de la discussion, des membres (tu peux aussi les ajouter pas la suite), et un message qui s'affichera dans la nouvelle discussion.


??? question "Qui a accès à la discussion ?"
    Toutes les personnes qui sont membres du canal parent ont la possibilité de rejoindre la discussion.

    En revanche, les personnes ajoutées à la discussion au moment de la création ne peuvent pas rejoindre le canal parent si elles n'en font pas partie.


#### Sur l'application mobile :

1. Sur la page principale, clique sur l'icône du crayon dans un carré en haut à droite, puis sélectionne "créer une discussion".

    **PHOTO**

2. Remplis les différentes informations nécessaires : le canal parent, le nom de la discussion, des membres (tu peux aussi les ajouter pas la suite), et un message qui s'affichera dans la nouvelle discussion.

### Accéder à une discussion

Tu peux voir la liste des discussions qui sont reliées à un canal en cliquant sur l'icône des deux bulles entrelacées dans le menu en haut à droite :

![Rocket.Chat](media/rc-bouton-discussions.png){ loading=lazy .md-img-border }

!!! warning "Attention"
    Il s'agit bien de l'icône avec les **deux** bulles (discussion) , et non l'icône avec **une seule** bulle (fil).

Lorsque tu te rends sur une discussion pour la première fois, tu peux voir les messages qui y sont postés. Mais pour participer aux échanges et recevoir les notifications, il faut le rejoindre en cliquant sur le bouton bleu "Rejoindre".

![Rocket.Chat](media/rc-bouton-rejoindre.png){ loading=lazy .md-img-border }






## 🌙 Mode sombre

Pour reposer les yeux et économiser de l'énergie, tu peux passer Rocket.Chat en mode sombre. 

PHOTO

**Sur ordinateur :**

Clique sur le soleil ou la lune dans le menu en haut à gauche.

**Sur Smartphone :**

Clique sur le menu en haut à gauche, puis sur "paramètres". Va dans la catégorie "Thème" et sélectionne le mode qui te convient.
