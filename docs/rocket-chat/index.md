---
title: Rocket.Chat (obsolète)
author: Alternatiba / ANV-COP21
date: Juillet 2022
theme: beige
icon: fontawesome/brands/rocketchat
---

!!! warning "Rocket.Chat est mort, vive Mattermost !"
    Depuis février 2024, nous avons changé d'outil de communication interne et utilisons désormais Mattermost.
    
    👉 **RDV sur [ce tuto](/mattermost/) pour découvrir et prendre en main Mattermost.**

    L'instance Rocket.Chat d'Alternatiba / ANV-COP21 reste accessible en lecture seule pendant un temps à l'adresse [https://chat-archives.alternatiba.eu](https://chat-archives.alternatiba.eu).


## 🤔 Rocket.Chat, c’est quoi ?

![Rocket.Chat](media/rocketchat.jpg){ align=left }

Rocket.Chat est un logiciel de messagerie instantanée qui est **l'outil de communication interne du mouvement Alternatiba / ANV-COP21 depuis 2018**. Cet espace de discussion est en premier lieu un moyen de maintenir et de développer le lien entre tous les membres du réseau. Son existence offre la possibilité d'échanger sur nos pratiques, de transmettre des informations et des documents, de travailler sur des projets communs, par le biais de salons de discussion dédiés à des sujets variés.

Rocket.Chat est accessible sur le web, et sur application pour ordinateur et téléphone intelligent.


??? question "Une alternative comme on les aime"

    Rocket.Chat est une alternative comme on les aime : un outil à taille plus humaine, géré par des gens que l'on connaît ([IndieHosters](https://indiehosters.net/)), qui vivent pas trop loin de chez nous et qui sont animé⋅es par des valeurs de solidarité et par l'espoir d'un monde meilleur. C'est aussi un gage de confiance : on sait qui stocke nos messages, et ce n'est pas une entreprise gigantesque, impersonnelle et animée par des multimilliardaires.

    Rocket.Chat est un logiciel libre développé par une grande communauté. En constante évolution, son développement est guidé par les retours apportés par les utilisateur⋅ices, ce qui en fait un outil super adapté !

??? question "L'information accessible à toustes"

    Tout⋅e utilisateur⋅ice de Rocket.Chat a le même statut et donc le même accès à l'information, contrairement aux messages électroniques où les référent⋅es ont besoin de transmettre l'info aux membres de leurs groupes locaux.

    De plus en plus de groupes locaux l'utilisent comme outil de communication interne, ce qui permet d'être plus facilement en lien avec les autres groupes et avec l'équipe d'animation globale.

## Les tutoriels Rocket.Chat

* [Mes premiers pas sur Rocket.Chat](./premiers-pas)
* [Utiliser Rocket.Chat au quotidien](./au-quotidien)
* [Mettre en forme ses messages sur Rocket.Chat](./mise-en-forme)