---
title: Mes premiers pas sur Rocket.Chat
author: Alternatiba / ANV-COP21
date: Juillet 2022
theme: beige
---

!!! warning "Rocket.Chat est mort, vive Mattermost !"
    Depuis février 2024, nous avons changé d'outil de communication interne et utilisons désormais Mattermost.
    
    👉 **RDV sur [ce tuto](/mattermost/) pour découvrir et prendre en main Mattermost.**

    L'instance Rocket.Chat d'Alternatiba / ANV-COP21 reste accessible en lecture seule pendant un temps à l'adresse [https://chat-archives.alternatiba.eu](https://chat-archives.alternatiba.eu).


## ✍️ S'inscrire sur Rocket.Chat

1. Dans un navigateur Internet, rends-toi sur [https://chat.alternatiba.eu](https://chat.alternatiba.eu){target=_blank}

2. Clique sur "Créer un nouveau compte"

    ![](media/rc-enregistrer-un-nouveau-compte.png){ loading=lazy .md-img-border }

3. Entre tes informations puis valide


    !!! question "Quel nom ?"

        Pour le nom, écris ton prénom + le nom de ton territoire entre parenthèses. Exemple : **Raymond (Grenoble)**

        Cela permet d'identifier facilement d'où viennent les personnes.

    ![](media/rc-informations-nouveau-compte.png){ loading=lazy .md-img-border }


4. Clique sur le lien de confirmation reçu par courriel (vérifie les spams !)

5. Choisis un nom d’utilisateurice et valide.
    
    Contrairement au nom d'affichage, le nom d'utilisateur n'a pas vocation à être modifié régulièrement.


    !!! question "Quel nom d'utilisateur ?"

        Exemples de nom d'utilisateur : **prénom.nom**, **prénom.ville**

    ![](media/rc-nom-utilisateur.png){ loading=lazy .md-img-border }

Et voilà ! Tu es désormais inscrit·e et connecté·e !



## 💬 Rejoindre les canaux de discussion

En arrivant pour la première fois, tu es automatiquement ajouté⋅e aux canaux les plus importants. Tu peux découvrir tous les autres espaces de discussion :

1.  En haut à gauche, clique sur le globe ![](media/rc-globe.png) : une barre de recherche des canaux s’affiche.

    ![](media/rc-clic-globe.png){ loading=lazy .md-img-border }


2.  Sélectionne l'onglet "Canaux" pour voir la liste des canaux publics. Pour découvrir une sélection des canaux les plus intéressants, tu peux aussi consulter la [cartographie des fils publics](https://docs.google.com/drawings/d/17b8Z2_gM5Wb5JhBCzcMIJQ1I5OdKoCuSJu6DY5apWiE){target=_blank}.

    ![](media/rc-clic-canaux.png){ loading=lazy .md-img-border }


3.  Clique sur le canal de ton choix, puis sur "Rejoindre" en bas à droite.

    ![](media/rc-clic-rejoindre.gif){ loading=lazy .md-img-border }

Et voilà ! Tu viens de faire tes premiers pas sur Rocket.Chat !



## 💪 Pour aller plus loin

- nous t’invitons à lire [la charte d’utilisation de Rocket.Chat](https://docs.google.com/document/d/1VP-YGjHHWRIO_wRvOiBSzI2uZ6EuUruiJkVKJ50YpWs/edit?usp=sharing){target=_blank} qui fixe quelques règles permettant un bon usage de l’outil

- tu peux continuer la lecture des autres tutos de cette page afin de t’approprier l’outil


## 🤯 J'ai besoin d'aide !

Tu peux retrouver toutes les informations générales sur Rocket.Chat en cliquant sur la petite maison en haut à gauche.

![](media/rc-clic-maison.png){ loading=lazy .md-img-border }

Si tu es perdu·e ou que tu rencontres des difficultés, n’hésite pas à envoyer un message sur le canal [#🚀Aide Rocket.Chat](https://chat.alternatiba.eu/channel/aide-rocket-chat){ target=_blank } ou par courriel à [admin_rc@alternatiba.eu](mailto:admin_rc@alternatiba.eu).

