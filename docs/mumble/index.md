---
title: Mumble
author: Alternatiba / ANV-COP21
date: Novembre 2024
theme: beige
icon: simple/mumble
---

![Mumble](media/mumble-logo.png){ align=left }

[**Mumble**](https://mumble.info/) est un logiciel libre permettant d'organiser des conférences audio.

Concrètement, il permet à plusieurs personnes de vivre des réunions à distance en n'utilisant que le **son** et le **chat textuel**, sans la visio. Le serveur Mumble est organisé avec différents **salons** permettant de vivre plusieurs réunions en parallèle.

**Pour bien commencer [c'est par ici](./user)** ! 👈

![Fenêtre de Mumble](media/mumble-fenetre.png){ loading=lazy }

Autres tutoriels : [http://revenudebase.info/mumble](http://revenudebase.info/mumble) et [Vidéo - Installer et configurer Mumble sur Windows](https://www.youtube.com/watch?v=QR0BRlbAP_o)

---

Tu es perdu⋅e ? Tu peux écrire à [informatique@alternatiba.eu](mailto:informatique@alternatiba.eu) ou sur le canal [⁉️ Questions et annonces](https://chat.alternatiba.eu/coordination-du-mouvement/channels/town-square). On est là pour t'aider ! :slight_smile:
