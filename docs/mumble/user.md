---
title: Utiliser Mumble
author: Alternatiba / ANV-COP21
date: Novembre 2024
theme: beige
---

![Mumble](media/mumble-logo.png){ align=left }

[**Mumble**](https://mumble.info/) est un logiciel libre permettant d'organiser des conférences audio.

Concrètement, il permet à plusieurs personnes de vivre des réunions à distance en n'utilisant que le **son** et le **chat textuel**, sans la visio. Le serveur Mumble est organisé avec différents **salons** permettant de vivre plusieurs réunions en parallèle.

![Fenêtre de Mumble](media/mumble-fenetre.png){ loading=lazy }

Autres tutoriels : [http://revenudebase.info/mumble](http://revenudebase.info/mumble) et [Vidéo - Installer et configurer Mumble sur Windows](https://www.youtube.com/watch?v=QR0BRlbAP_o)

## 📥 Télécharger Mumble

Va sur [https://mumble.info](https://mumble.info) et télécharge le client Mumble qui correspond à ton système d'exploitation.

!!! tip "Application mobile"
    Tu peux aussi télécharger l'application pour smartphone, pratique pour faire des réunions dans le train !

    🤖 Android : application sur [F-Droid](https://f-droid.org/packages/se.lublin.mumla/), sur [Google Play](https://play.google.com/store/apps/details?id=se.lublin.mumla)<br/>
    🍏 iOS : application sur [Apple store](https://apps.apple.com/us/app/mumble/id443472808)

!!! info "Linux"

    Sur Linux, la version Flatpak qui est proposée dans la logithèque ou la version `apk` sont bien.

## 🪄 Installation

<!--
Pour réinstaller et repartir à zéro sour Linux en installant via apt :
sudo apt purge mumble
rm -rf ~/.config/Mumble/ ~/.local/share/Mumble/ ~/Documents/MumbleAutomaticCertificateBackup.p12
sudo apt install mumble
-->

Ouvre le fichier d'installation. Au premier lancement, un parcours de configuration t'es proposé, voici quelques recommandations :

1. Assistant audio (tu peux le retrouver plus tard dans Configurer > Assistant audio) :

    - À la sélection des périphériques (2ème écran) : décoche la case "Activer l'audio positionnel" (normalement décochée par défaut)
    - À la détection de l'activité vocale (4ème écran), choisis :
        - soit "Amplitude d'entrée" (recommandé si pas à l'aise avec l'ordi) : il faudra cliquer sur le micro pour ouvrir/couper ton micro
        - soit "Appuyer pour parler" + configurer la touche qui sera utilisée pour parler : il faudra appuyer dessus pour parler
    - À la qualité et notifications (5ème écran) : désactive la synthèse vocale (utiliser des sons à la place)
    - Au dernier écran : tu peux décocher la case "Soumettre des statistiques anonymes au projet Mumble"

2. Gestion de certificats : garde la création de certificat automatique

3. Partage de ton adresse pour accéder aux serveurs Mumble publics : tu peux choisir "Non"

## 🌐 Ajouter le serveur d'Alternatiba

Clique sur "Ajouter nouveau..." et entre la configuration :

!!! quote ""

    Adresse : **audio.alternatiba.eu**

    Port : **64738**

    Nom d'utilisateur : **Prenom(Ville)** (sans espace)

    Nom : **Alternatiba** (ou autre chose, c'est le nom d'affichage du serveur)

Puis connecte-toi ! 😺

!!! success "Message d'alerte certificat"

    À la première connexion, tu as un message d'alerte concernant le certificat du serveur. Tu peux accepter de faire confiance au certificat.

## ⚙️ Configurations recommandées

Puis, voilà quelques configurations recommandées à faire à ta première connexion :

- Engrenage bleu > Entrée audio > Divers > décocher **Indiquer si muet**
- Engrenage bleu > Messages > dans la liste à la ligne "Message texte" > décocher **Chemin**

## 🎧 Utiliser Mumble

Double-clique sur le salon que tu veux rejoindre dans la liste à droite.

Pense à bien te muter (icône "micro" en haut) quand une autre personne parle ! 😉

!!! info "Signes écrits pour l'utilisation du chat"

    | Signe  | Sens                                                |
    | ------ | --------------------------------------------------- |
    | **++** | je suis d'accord                                    |
    | **--** | je ne suis pas d'accord                             |
    | **+-** | je suis plus ou moins d'accord, à affiner/compléter |
    | **\*** | je demande la parole                                |
    | **>>** | déjà dit, on se répète, il faut avancer             |
    | **//** | le son coupe, je n'entends rien ou pas bien         |
    | **%**  | le son est bon, je t'entends bien                   |

Voilà !

## 🚨 À l'aide !

### Je veux changer de pseudo

Pour changer de pseudo après l'installation tu peux :

- SOIT cliquer sur "Éditer" au lieu de "Connexion" quand tu ouvres Mumble
- SOIT quand tu es connecté·e aller dans Serveur > Se connecter > Éditer puis modifier le pseudo

### J'entends des voix

Si une voix répète tout ce qui est écrit dans le chat, désactiver la synthèse vocale :<br/>
Engrenage bleu > Messages > Synthèse vocale > désactiver

### J'entends des bips réguliers

Si tu entends un bip sonore à intervalles réguliers (toutes les 4 secondes) :<br/>
Engrenage bleu > Entrée audio > Divers > décocher **Indiquer si muet**

### J'entends des "TOUNG" à chaque message

Si tu veux désactiver les "TOUNG" quand un message est envoyé :<br/>
Engrenage bleu > Messages > dans la liste à la ligne "Message texte" > décocher **Chemin** 

### Je n'entends rien quand un message est envoyé

Si tu veux activer les "TOUNG" quand un message est envoyé :<br/>
Engrenage bleu > Messages > dans la liste, cocher **Chemin** à la ligne "Message texte"

### Je veux que mon micro ne s'active que quand j'appuie sur une touche

Il faut configurer un raccourci "Appuyer pour parler" (ou "Push to talk").

Sur Linux / Windows :

- Engrenage bleu > Réglages / Paramètres
- *Si nécessaire, cocher la case Avancé en bas à droite*
- Onglet "Entrée audio" (à gauche) > Section "Transmission"
- Sélectionner "Appuyer-pour-parler"
- Onglet "Raccourci"
- Ajouter un raccourci pour "Appuyer-pour-parler" (on recommande la touche "pause" si elle est présente)

Sur Mac :

- Mumble > Préférences > Raccourcis > Ajouter (une nouvelle ligne s'ajoute sur le tableau)
- Cliquer sur "Non assigné" dans la colonne "Fonction"
- Choisir "Cliquer pour parler"
- Cliquer sur la même ligne au niveau de la colonne "Raccourci"
- Choisir la touche pour parler (on recommande la touche "Commande" cmd)

### Mumble ne s'ouvre pas après l'installation

Sur Windows, il est possible que Mumble ne parvienne pas à se lancer : la fenêtre s'ouvre mais se ferme immédiatement.

C'est probablement dû au fait qu'il faut installer une librairie C++ :

1. Aller sur [https://www.mumble.info/downloads](https://www.mumble.info/downloads)
2. Installer "Windows Microsoft Visual C++ 2022 Redistributable" en cliquant sur le lien "x64 download"
3. Réessayer d'ouvrir Mumble, ça devrait fonctionner à nouveau !

### Message d'erreur à l'installation sur Mac

Pour les Mac, il est possible d'avoir un message d'erreur  au premier lancement, comme quoi le dévelopeur ne peut pas être vérifié.

Voici comment le régler :

1. Télécharger le fichier .dmg

2. Glisser l'icône Mumble dans le dossier Applications

3. Lancer l'application Mumble : erreur !

    ![Mumble cannot be opened](media/mumble_cannot-be-opened.png){ loading=lazy : style="width:300px"}

4. Il faut autoriser macOS à exécuter Mumble :<br/>Menu Pomme > System Settings > Privacy and security (dans la barre de gauche) > Security<br/>On voit "Mumble was blocked"

    ![Mumble was blocked](media/mumble_was-blocked.png){ loading=lazy : style="width:500px"}

5. Cliquer sur "Open Anyway"

6. Entrer son mot de passe Mac

7. Lancer à nouveau Mumble

8. À nouveau message d'alerte → cliquer sur "Open"

    ![macOS cannot verify the developer of "Mumble"](media/mumble_verify.png){ loading=lazy : style="width:300px"}

C'est tout bon !

---

Tu es perdu⋅e ? Tu peux écrire à [informatique@alternatiba.eu](mailto:informatique@alternatiba.eu) ou sur le canal [⁉️ Questions et annonces](https://chat.alternatiba.eu/coordination-du-mouvement/channels/town-square). On est là pour t'aider ! :slight_smile:
