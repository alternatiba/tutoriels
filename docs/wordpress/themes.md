---
title: Thèmes à tester
summary: Tests des thèmes les plus légers
author: Numahell
license: CC0
date: Décembre 2020
---

# Thèmes à tester

https://athemes.com/collections/best-free-lightweight-wordpress-themes/

## Zakra

* **+** léger
* **+** paramétrages très nombreux

## Astra

* **+** paramétrages nombreux et pertinents
* **+** léger
* **+** paramétrage fil d'ariane
* **+** paramétrage fin d'une page ou d'un article

## Airi

* **+** trés épuré
* **-** quasiment pas de paramétrages

## Generated Press

* **-** pas mal sans plus
* **+** paramétrages pas trop nombreux et pertinents
* **+** vraiment léger

## Ocean WP

* **+** paramétrages nombreux
* **-** pas le plus léger

## Page builder framework

* **-** paramétrages pas forcément pertinents
* **-** pas de réglages de couleurs

## Twenty one

* **+** accessibilité
* **-** peu de paramétrages
* **-** pas de barre latérale