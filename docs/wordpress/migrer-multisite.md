---
title: Migrer un site wordpress depuis un multisite
summary: 
author: Numahell
license: CC0
date: Décembre 2020
---

## Méthode de migration export / import `xml`

Nous proposons cette méthode de migration des données d'un site de groupe local : elle utilise la fonctionnalité d'import / export `xml` de Wordpress.

Du fait du multisite, l’export `sql` d'un seul site n'a pas toutes les tables, et contien des metadonnées liées aux trop nombreux plugins. Il est donc plus compliqué de l'importer tel quel dans une nouvelle base de données.

Par contre, cette méthode ne permet pas de récupérer la configuration des widget, des menus et du thème.

## Étapes pour migrer votre contenu

### Nettoyez votre site

1. Nettoyez vos commentaires en spam (inutile de les récupérer :))
2. Supprimez les contenus que vous ne souhaitez pas conserver
3. Supprimez les comptes que vous ne souhaitez pas conserver

### Exportez vos contenus

Exportez vos contenus depuis le site alternatiba.eu/votregroupe

1. Allez dans la section [Outils > Exporter](https://alternatiba.eu/toulouse/wp-admin/export.php)
2. Sélectionnez **Tout le contenu**, et cliquez sur **Télécharger le fichier d'export**

Un fichier au format `xml` est sauvegardé sur votre disque. Si sa taille est supérieure à 2Mo, vous aurez une étape de plus à faire sur votre serveur.

### Préparez votre nouveau site

1. Installez Wordpress (doc officielle)
2. Lancez l'installation `/wp-admin/install.php`
3. Installez le thème souhaité
4. Installez les plugins que vous utilisez sur votre site actuel et qui ont créé des types de contenus spécifiques, et les activer
        Par exemple, pour Alternatiba Toulouse nous utilisons les plugins [Event Manager](https://fr.wordpress.org/plugins/events-manager/) et [Contact form 7](https://fr.wordpress.org/plugins/contact-form-7/)
5. Si votre fichier est trop volumineux, configurez `php` pour accepter de téléverser des fichiers jusqu'à 16Mo par exemple (cf encadré)

### Importer les contenus

1. Allez dans la section **Outils > Importer**
1. À la ligne **Wordpress**, cliquer sur **Installer maintenant** pour installer le plugin
1. Cliquez sur **Lancez l'outil d'importation**

Vous arrivez sur la page d'importation depuis Wordpress

1. Cliquez sur **Parcourir** et sélectionnez le fichier `xml` précédemment récupéré
1. Cliquez sur **Téléverser et importer le fichier**

Vous arrivez sur une page permettant de configurer l'import

1. Assignez les auteur⋅ices à leurs comptes existant sur le site (s'ils existent)
1. Cochez la case **Téléverser et importer les fichiers joints**
1. Cliquez sur **Envoyer**

Patientez quelques minutes, surtout si vous avez beaucoup de contenus !

### Vérifiez vos contenus

Vous devez avoir au moins autant de page et d'article que sur votre site sur alternatiba.eu

TODO: captures d'écran

## Résolutions de problèmes

### Le fichier est trop gros

Une limite de taille est fixée pour téléverser des fichiers, vous pouvez la modifier.

Pour cela, vous aurez besoin d'avoir accès au serveur : augmentez la taille maximale acceptée dans un fichier `/etc/php/conf.d/updload.ini`

```ini
memory_limit = 64M
upload_max_filesize = 64M
post_max_size = 64M
max_execution_time = 600
```

### Erreur mysql

Vous rencontrez une erreur php en lançant votre site. Si la version de php est supérieure à 7.3 et que vous utilisez `mysql`, vous devrez ajouter cette ligne dans votre configuration php : `extension = mysqli`

TODO: à vérifier avec MariaDB