---
title: Bienvenue
description: Tuto Alternatiba / ANV-COP21
author: Alternatiba / ANV-COP21
date: août 2023
màj: novembre 2024
theme: beige
icon: material/home
---

# Bienvenue ! :wave:

Tu trouveras ici quelques tutoriels concernant des outils numériques utilisés au sein d'Alternatiba / ANV-COP21 :

- :fontawesome-solid-users: Outils de réunion
    - :material-video: [Zoom](/zoom), pour faire des réunions en audio/vidéo
    - :simple-bigbluebutton: [BigBlueButton](/bbb), pour faire des réunions en audio et vidéo
    - :simple-mumble: [Mumble](/mumble), pour faire des réunions en audio
- :fontawesome-solid-comments: Outils de communication
    - :simple-mattermost: [Mattermost](/mattermost), messagerie du mouvement
    - :material-palette: [Aktivisda](/aktivisda), pour la création de visuels
    - :material-email: [Mail](./mail), pour gérer son adresse mail ou sa liste Alternatiba
- :material-database: Outils de données
    - :material-database: [Baserow](/baserow), outil de base de données
    - :fontawesome-solid-map-pin: [Transiscope](/transiscope), carte des alternatives
    - :fontawesome-solid-calendar-day: [NOÉ](/noe), pour organiser des événements
- :fontawesome-solid-toolbox: Autres outils
    - :simple-bitwarden: [Bitwarden](/bitwarden), gestionnaire de mots de passe
    - :simple-googledrive: [Google Drive](/google-drive), pour partager des documents
    - :material-poll: [Framavox](/framavox), outil de prise de décision

:toolbox: Retrouve également tous les outils et liens utiles dans **[la caisse à outils du mouvement](https://outils.alternatiba.eu)** !

[![Image title](assets/logo-alternatiba.png#only-light){ width="200" loading=lazy }](https://alternatiba.eu)
[![Image title](assets/logo-alternatiba-blanc.png#only-dark){ width="200" loading=lazy}](https://alternatiba.eu)
[![Image title](assets/logo-anv.png#only-light){ width="200" loading=lazy}](https://anv-cop21.org)
[![Image title](assets/logo-anv-blanc.png#only-dark){ width="200" loading=lazy}](https://anv-cop21.org)

---

Fait avec :heart: par [Alternatiba](https://alternatiba.eu) et [ANV-COP21](https://anv-cop21.org) sous [licence CC BY-SA](https://creativecommons.org/licenses/by-sa/3.0/fr/)

Une question ? Une suggestion ? Écris à [informatique@alternatiba.eu](mailto:informatique@alternatiba.eu)
